# Global imports
import flask_restplus
from flask_restplus import reqparse

# Local imports
from data_pm.web.api import backend


ns = flask_restplus.Namespace('environment')


parser = reqparse.RequestParser()
parser.add_argument('hostname', type=str, required=True)
parser.add_argument('hash_code', type=str, required=True)


@ns.route('/')
class Index(flask_restplus.Resource):
    def post(self):
        """
        Update last used date for a given environment.
        """

        args = parser.parse_args(strict=True)

        hostname = args['hostname']
        hash_code = args['hash_code']

        backend.update_environment(hostname, hash_code)

    def delete(self):
        """
        Delete an environment from worker
        """

        args = parser.parse_args(strict=True)

        hostname = args['hostname']
        hash_code = args['hash_code']
