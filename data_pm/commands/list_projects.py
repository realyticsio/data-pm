#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Return a list of projects.

"""

# Global imports
from __future__ import print_function

import sys

# Local imports
from data_pm.core import project_mngr
from data_pm.tools.cli_handler import BaseCommand


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def handle(self, namespace):
        project_names = project_mngr.get_project_names()
        print('\n'.join(sorted(project_names)))
