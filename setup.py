import setuptools

DISTNAME = 'data_pm'
DESCRIPTION = "Data Project Manager"
LONG_DESCRIPTION = "A project manager to process data"
URL = "https://bitbucket.org/realyticsio/data-pm"
LICENSE = "Apache License, Version 2.0"
VERSION = "4.4.16"

PYTHON_REQUIREMENTS = [
    'unidecode>=0.04.18',
    # Used by pm debug when launching study
    'ipykernel==5.1.0', 'jupyter-console==6.0.0', 'IPython==7.4.0',
    'traitlets < 5.0.0',  # The 5.0.0 drop the python 3.6 support, preventing correct installation of data-pm
    'requests>=2.22.0',
    # When creating environment for datalab
    'requirements-parser==0.2.0', 'gitpython==2.1.11', 'gitdb2==2.0.6',
    # Study start and end can be send to slack in some case (token might not be up-to-date)
    'slackclient==1.3.1',
    # To handle study start and monitoring
    'redis==3.2.1', 'celery==4.3.0', 'flower>=0.9.3',
    'kombu==4.5.0', 'vine <= 1.3.0',  # Dependencies of celery and flower
    'networkx==2.3.0',  # Study dependencies graph building
    # Study dashboard
    'flask==1.0.2', 'flask_restplus==0.12.1',
    'psutil',  # To handle locks with process IDs
    # Useful to the build process
    'sphinx==1.3.1',
]


PACKAGE_DATA = {
    'data_pm.web.dashboard': [
        'static/dashboard_app/*.jpg',
        'static/dashboard_app/*.js',
        'static/css/*.css',
        'templates/dashboard_app/*.html',
        'templates/dashboard_app/*/*.html',
        'templates/static/*.html'
    ],
    'data_pm_docs': [
        'build/html/*.html',
        'build/html/*.js',
        'build/html/*/*'
    ],
    'data_pm': [
        'templates/*'
    ]
}

DATA_FILES = [
    ('/etc/bash_completion.d', ['scripts/pm_completion']),  # Make a command to install this file
    # ('/usr/local/etc/data-pm.d', ['data_pm/settings/global_settings.yml'])
]

PACKAGES = [
    'data_pm',
    'data_pm.commands',
    'data_pm.configuration',
    'data_pm.core',
    'data_pm.core.drivers',
    'data_pm.core.study_components',
    'data_pm.core.factory',
    'data_pm.core.dependencies',
    'data_pm.deployment',
    'data_pm.web',
    'data_pm.web.api',
    'data_pm.web.api.views',
    'data_pm.web.dashboard',
    'data_pm.web.dashboard.tools',
    'data_pm.tools',
    'data_pm_docs'
]

params = {
    'name': DISTNAME,
    'version': VERSION,
    'description': LONG_DESCRIPTION,
    'license': LICENSE,
    'packages': PACKAGES,
    'package_data': PACKAGE_DATA,
    'data_files': DATA_FILES,
    'scripts': ['scripts/pm'],
    'install_requires': PYTHON_REQUIREMENTS,
    'url': URL,
    'keywords': 'data project manager python',
    'zip_safe': False,
}

if __name__ == '__main__':
    setuptools.setup(**params)
