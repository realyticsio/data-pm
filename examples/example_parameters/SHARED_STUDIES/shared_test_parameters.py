# encoding: utf-8

"""
description: |-
  Study that loads multiple parameters and test their values

inputs: []

outputs: []

parameters:
- name: string_explicit
  default: holé!
  dtype: str

- name: string_implicit
  default: holé!

- name: float_explicit
  default: 2.0
  dtype: float

- name: float_implicit
  default: 2.4

- name: bool_explicit
  default: true
  dtype: bool

- name: bool_implicit
  default: false

- name: integer_explicit
  default: 2
  dtype: int

- name: integer_implicit
  default: 89

"""

import data_pm


class Study(data_pm.BaseStudy):
    def run(self):
        from builtins import str as new_str

        assert isinstance(self.p.string_explicit, (str, new_str)), '{} != {}'.format(type(self.p.string_explicit), new_str)
        assert isinstance(self.p.string_implicit, (str, new_str))

        assert isinstance(self.p.integer_explicit, int)
        assert isinstance(self.p.integer_implicit, int)

        assert isinstance(self.p.float_explicit, float), '{} != {}'.format(type(self.p.float_explicit), float)
        assert isinstance(self.p.float_implicit, float)

        assert isinstance(self.p.bool_explicit, bool)
        assert isinstance(self.p.bool_implicit, bool)

        for parameter_name, parameter in sorted(self.parameters.items()):
            self.logger.info(u'{}: {}'.format(parameter_name, parameter.value))
