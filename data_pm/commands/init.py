#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Initialize a new workspace
"""

# Global imports
from __future__ import print_function

import os
import sys

# Local imports
from data_pm import settings
from data_pm.tools.cli_handler import BaseCommand
from data_pm.tools import git_helper


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('workspace', help='Name of the workspace (directory name)')

    def handle(self, namespace):
        init(namespace.workspace)


def init(workspace_name):
    if workspace_name is None:
        print('Please specify a name for your workspace')
        return

    workspace_path = os.path.abspath(workspace_name)

    if not ask_confirmation(workspace_path):
        return

    os.mkdir(workspace_path)
    os.chdir(workspace_path)

    write_file('__init__.py')

    os.makedirs(settings['data_pm.projects.dir'])
    os.makedirs(settings['data_pm.shared.dir'])

    # Create STUDIES_DIR directories and __init__.py files
    os.makedirs(settings['data_pm.studies.dir'])
    for dirpath, dirnames, filename in os.walk(os.path.split(settings['data_pm.studies.dir'])[0]):
        write_file(os.path.join(dirpath, '__init__.py'))

    # Create SHARED_STUDIES_DIR directories and __init__.py files
    os.makedirs(settings['data_pm.shared_studies.dir'])
    for dirpath, dirnames, filename in os.walk(os.path.split(settings['data_pm.shared_studies.dir'])[0]):
        write_file(os.path.join(dirpath, '__init__.py'))

    write_file('parameters.yml')
    write_file('requirements.txt', 'data-pm\n')

    write_file('branches.yml', 'master:\n  parent:\n  recompute_from: []\n')

    os.mkdir('settings')
    write_file(os.path.join('settings', 'settings.yml'))

    print('\nWorkspace created!')
    print('Move to "{}" to start using the pm command'.format(workspace_path))


def write_file(file_name, txt=None):
    with open(file_name, 'w') as fp:
        if txt is not None:
            fp.write(txt)


def ask_confirmation(workspace_path):
    print('You are going to create a workspace in "{}"'.format(workspace_path))
    sys.stdout.write('Proceed? [y/N]')
    try:
        confirm_text = sys.stdin.readline().strip()
    except KeyboardInterrupt:
        print('\nCancelled')
        return False

    if confirm_text != 'y' and confirm_text != 'Y':
        print('\nCancelled')
        return False

    return True
