"""
description: |-
  Counts the number of N-grams

inputs:
- alias: doc
  name: doc.txt
  parent: generate_data

outputs:
- alias: counts
  name: counts.csv
  driver: file
  description: |-
    csv file containing the number of each ngrams
    Example:
      ngram,count
      abc|def,3
      ghi|def,10

parameters:
- name: n
  default: '2'
  dtype: str


"""

import collections
import pandas as pd

import data_pm


class Study(data_pm.BaseStudy):
    def run(self):
        # Parse the input parameter
        n = int(self.p.n)

        # Parse input file
        with data_pm.file.read(self.i.doc) as reader:
            txt = reader.read()

        d_ngrams = collections.Counter()

        # Count trigrams
        words = tokenize(txt)
        for i in range(len(words) - n):
            key = '|'.join(words[i:i + n])
            d_ngrams[key] += 1

        # Write counts to output file
        pd.Series(d_ngrams).to_csv(self.o.counts)


def tokenize(txt):
    """
    Tokenize a text

    Use nltk in a real case

    :param str txt:
    :rtype: list[str]
    """
    words = txt.replace('\n', ' ').split(' ')

    return words
