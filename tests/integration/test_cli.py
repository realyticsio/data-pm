# encoding: utf-8

# Global imports
import unittest
import subprocess
import os
import shutil
import glob

# Local imports
WORKSPACE_DIR = '/tmp/wk'


class TestCLI(unittest.TestCase):
    def setUp(self):
        if os.path.exists(WORKSPACE_DIR):
            shutil.rmtree(WORKSPACE_DIR)

        self.pm_exec = os.path.abspath('./scripts/pm')

        # Initialize the workspace
        run_command([self.pm_exec, 'init', WORKSPACE_DIR], input=b'y')
        copy_settings(WORKSPACE_DIR)

    def test_basic(self):
        # Copy examples_studies
        copy_studies('example_basic', WORKSPACE_DIR)

        # Create a project "johnny"
        run_command([self.pm_exec, 'create_project', 'johnny'], cwd=WORKSPACE_DIR, input=b'y')

        # Launch the study "generate_data" for project "johnny"
        run_command([self.pm_exec, 'launchv3', 'johnny', 'generate_data'], cwd=WORKSPACE_DIR)

        # Launch the study "count_ngrams" for project "johnny"
        run_command([self.pm_exec, 'launchv3', 'johnny', 'count_ngrams'], cwd=WORKSPACE_DIR)

    def test_basic_s3(self):
        # Copy examples_studies
        copy_studies('example_basic_s3', WORKSPACE_DIR)

        # Create a project "johnny"
        run_command([self.pm_exec, 'create_project', 'johnny'], cwd=WORKSPACE_DIR, input=b'y')

        # Launch the study "generate_data" for project "johnny"
        run_command([self.pm_exec, 'launchv3', 'johnny', 'generate_data'], cwd=WORKSPACE_DIR)

        # Launch the study "count_ngrams" for project "johnny"
        run_command([self.pm_exec, 'launchv3', 'johnny', 'count_ngrams'], cwd=WORKSPACE_DIR)

    def test_parameters(self):
        # Copy examples_studies
        copy_studies('example_parameters', WORKSPACE_DIR)

        run_command([self.pm_exec, 'launchv3', 'shared_test_parameters'], cwd=WORKSPACE_DIR)

        cmd = [self.pm_exec, 'launchv3', 'shared_test_parameters', '-p',
               u'string_explicit=héhé',
               u'string_implicit=héhé',
               u'unicode_explicit=héhé',
               u'unicode_implicit=héhé',
               'integer_explicit=-2',
               'integer_implicit=-2',
        ]
        run_command(cmd, cwd=WORKSPACE_DIR)

    def test_shared(self):
        # Copy examples_studies
        copy_studies('example_shared', WORKSPACE_DIR)

        # Create projects
        run_command([self.pm_exec, 'create_project', 'johnny'], cwd=WORKSPACE_DIR, input=b'y')
        run_command([self.pm_exec, 'create_project', 'johlly'], cwd=WORKSPACE_DIR, input=b'y')
        run_command([self.pm_exec, 'create_project', 'johffrey'], cwd=WORKSPACE_DIR, input=b'y')

        # Launch the study "generate_data" for project "johnny"
        run_command([self.pm_exec, 'launchv3', 'johnny', 'generate_data'], cwd=WORKSPACE_DIR)

        # Launch the shared study "shared_merge_ngrams"
        run_command([self.pm_exec, 'launchv3', 'shared_merge_ngrams'], cwd=WORKSPACE_DIR)

    def test_shared_2(self):
        # Copy examples_studies
        copy_studies('example_shared_2', WORKSPACE_DIR)

        # Create projects
        run_command([self.pm_exec, 'create_project', 'johnny'], cwd=WORKSPACE_DIR, input=b'y')
        run_command([self.pm_exec, 'create_project', 'jorge'], cwd=WORKSPACE_DIR, input=b'y')
        run_command([self.pm_exec, 'create_project', 'joon'], cwd=WORKSPACE_DIR, input=b'y')

        # Launch the shared study "shared_merge_common_words"
        run_command([self.pm_exec, 'launchv3', 'shared_merge_common_words'], cwd=WORKSPACE_DIR)

    def tearDown(self):
        #if os.path.exists(WORKSPACE_DIR):
        #    shutil.rmtree(WORKSPACE_DIR)
        pass


def run_command(cmd, cwd=None, input=None):

    sub = subprocess.Popen(cmd, cwd=cwd,
                           stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE,
                           env={'PYTHONPATH': os.getcwd() + ':' + os.environ.get('PYTHONPATH'),
                                'PATH': os.environ.get('PATH')})
    stdout, stderr = sub.communicate(input)

    if sub.returncode != 0:
        raise RuntimeError('Command "{}" failed:\n{}'.format(' '.join(cmd), stderr))


def copy_studies(example_name, workspace):
    """
    Copy studies from an example to the workspace

    :param example_name:
    :param workspace:
    :return:
    """

    example_dir = os.path.join('examples', example_name)

    paths = glob.glob(os.path.join(example_dir, '*', '*.py'))

    for path in paths:
        src_path = path
        dest_path = os.path.join(workspace, 'DEV', os.path.relpath(path, example_dir))
        shutil.copy(src_path, dest_path)


def copy_settings(workspace):
    """
    Copy settings from the current directory to the target workspace

    :param workspace:
    """

    for filename in os.listdir('settings'):
        shutil.copy(os.path.join('settings', filename),
                    os.path.join(workspace, 'settings', filename))


