from data_pm import settings


def generate_dir(driver, project_name=None):
    """
    Return the path to the outputs of studies (may be log or data)
    """
    if project_name is None:
        return settings['data_pm.shared.dir']
    else:
        return driver.join(settings['data_pm.projects.dir'], project_name)


def path_to_logs(driver, branch_name, study_name, project_name=None):
    """
    Return the path to the log output of a study
    """
    logs_location = settings.expand('data_pm.logs.dir')
    directory = generate_dir(driver, project_name)
    return driver.join(logs_location, directory, 'LOG', branch_name, study_name)


def path_to_views(driver, branch_name, study_name, project_name=None):
    """
    Return the path to the html view of a study
    """
    views_location = settings.expand('data_pm.views.dir')
    directory = generate_dir(driver, project_name)
    return driver.join(views_location, directory, 'VIEW', branch_name, study_name)


def path_to_project(driver, project_name):
    assert project_name is not None
    data_location = settings.expand('data_pm.driver.{}.workspace'.format(driver.name))
    directory = generate_dir(driver, project_name)
    return driver.join(data_location, directory)


def path_to_branch(driver, branch_name, project_name=None):
    """
    Return the path to the project's branch for a given driver
    """
    data_location = settings.expand('data_pm.driver.{}.workspace'.format(driver.name))
    directory = generate_dir(driver, project_name)
    return driver.join(data_location, directory, 'DATA', branch_name)


def path_to_study(driver, branch_name, study_name=None, project_name=None):
    """
    Return the path to a study
    """
    branch_path = path_to_branch(driver, branch_name, project_name)
    return driver.join(branch_path, study_name)


def path_to_output(driver, branch_name, study_name, output_name, project_name=None):
    """
    Return the path to a specific output of a study
    """
    study_path = path_to_study(driver, branch_name, study_name, project_name)
    return driver.join(study_path, output_name)
