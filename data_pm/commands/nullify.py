#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Set the "last update" value to None for a given study.

Be careful : all downstream studies will expire.
"""

# Global imports
from __future__ import print_function
import sys

# Local imports
from data_pm import settings, client
from data_pm.core import study_mngr, project_mngr, paths
from data_pm.core.drivers import get_driver
from data_pm.tools.cli_handler import BaseCommand


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def __init__(self, parsers, parser):
        super(Command, self).__init__(parsers, parser)
        self.all_regular_study_names = study_mngr.get_study_names()
        self.all_shared_study_names = study_mngr.get_shared_study_names()
        self.all_project_names = project_mngr.get_project_names()

    def add_arguments(self, parser):
        parser.add_argument('projects_or_studies', nargs='+', metavar='PROJECT OR STUDY',
                            choices=self.all_regular_study_names + self.all_shared_study_names + self.all_project_names)
        parser.add_argument('--branch', type=str, required=True,
                            help='Delete data from this branch')

    def handle(self, namespace):
        # Filter inputs
        regular_study_names = set(namespace.projects_or_studies) & set(self.all_regular_study_names)
        shared_study_names = set(namespace.projects_or_studies) & set(self.all_shared_study_names)
        project_names = set(namespace.projects_or_studies) & set(self.all_project_names)

        if regular_study_names and shared_study_names:
            raise ValueError('Do not mix shared and regular studies, purge cancelled.')
        elif shared_study_names and project_names:
            raise ValueError('Do not mix shared studies and projects, purge cancelled.')

        study_names = shared_study_names or regular_study_names
        studies_to_nullify = []

        for study_name in study_names:
            if project_names:
                for project_name in project_names:
                    studies_to_nullify.append((study_name, project_name))
            else:
                studies_to_nullify.append((study_name, None))

        branch = namespace.branch
        size = len(studies_to_nullify)

        print(
            '@@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@\n'
            'WARNING: You are going to nullify {} studies '
            'on branch \033[41m\033[1m\033[7m{}\033[0m (here is the first ones):\n'
            '\n  {}\n\n'
            '@@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@\n'
            .format(size, branch, '\n  '.join([str(x) for x in studies_to_nullify][:20]))
        )

        confirm_or_exit()

        # Nullify studies
        for study, project in studies_to_nullify:
            print('[nullify] {}'.format((study, project)))
            client.post_last_update_nullify(dict(
                branch=branch,
                study=study,
                project=project
            ))


def confirm_or_exit():
    try:
        sys.stdout.write('\nPlease confirm by writing "yesremoveallthis[ENTER]": ')
        confirm_text = sys.stdin.readline().strip()

        if confirm_text != 'yesremoveallthis':
            exit_('Purge cancelled.')

    except KeyboardInterrupt:
        exit_('Purge cancelled.')


def exit_(msg):
    print(msg)
    sys.exit(0)
