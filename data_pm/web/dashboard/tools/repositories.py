# Global imports
import os
import time
import git
import psutil
import flask
import traceback
from werkzeug.utils import secure_filename
from git.exc import InvalidGitRepositoryError, NoSuchPathError

# Local imports
from data_pm import settings
from data_pm.tools import git_helper
from data_pm.deployment.snapshots import copy
from data_pm.core.study_components import study_control


# Wrapper for API functions
def switch_branch(func):
    def wrapper(*args, **kwargs):
        branch = secure_filename(kwargs['branch'])  # never trust user input

        try:
            with RepositoryManager(branch):
                study_control.cache.clear()
                result = func(*args, **kwargs)
        except Exception as e:
            ctx = {'messages': [traceback.format_exc()] + list(e.args)}
            return flask.render_template('dashboard_app/error.html', **ctx)

        return result

    return wrapper


class RepositoryManager(object):
    def __init__(self, branch):
        """
        Configure a safe working directory for a given branch

        Clones the branch from the current working directory and exports it to another location.
        Then move to this other location and act as if it were the current working directory.
        This allows to read studies and other files from a specific git branch.

        :param str branch: Branch name
        """
        self.branch = branch
        self.repo_api = settings.local('data_pm.repo.api')
        self.repo_master = os.path.join(self.repo_api, 'master')
        self.repo_path = os.path.join(self.repo_api, branch)
        self.lock = None
        self.init_path = None

    def get_repo(self):
        return git.Repo(self.repo_path)

    def create_repository(self):
        try:
            return self.get_repo()
        except NoSuchPathError:
            url = settings['data_pm.repo.url']
            return git_helper.clone(
                url=url,
                path=self.repo_path,
                branch=self.branch,
                single_branch=True
            )
            # git_helper.remote_update(self.repo_master)
            # repo = git.Repo(self.repo_master)
            # if not git_helper.is_remote(repo, self.branch):
            #     raise RuntimeError('You tried to access branch "{}". This branch is not in remote branches.'
            #                        .format(self.branch))
            # copy(self.repo_master, self.repo_path)
            # self.get_repo().git.checkout(self.branch)
            # self.get_repo().git.pull()
        except InvalidGitRepositoryError:
            raise RuntimeError('API repository of branch "{}" is corrupted. Delete it.'.format(self.branch))

    def pull(self):
        self.get_repo().git.pull()

    def __enter__(self):
        self.lock = GlobalLock()
        self.lock.lock()

        try:
            if os.path.exists(self.repo_path):
                active_branch = self.get_repo().active_branch.name
                if self.branch != active_branch:
                    raise RuntimeError('Active branch of repo:"{}" is "{}".'
                                       .format(self.branch, active_branch))
            else:
                self.create_repository()
        except (RuntimeError, IOError):
            self.lock.release()
            raise

        self.init_path = os.getcwd()
        os.chdir(self.repo_path)

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.chdir(self.init_path)
        self.lock.release()


class GlobalLock(object):
    def __init__(self):
        """
        Create a global lock, even across different processes

        """
        self.lock_path = os.path.join(settings.local('data_pm.local.path'), 'api-update-repo.lock')
        self.previous_directory = None

    def lock(self):
        """
        Set a global lock
        """
        self.wait()

        with open(self.lock_path, 'w') as fd:
            fd.write(str(os.getpid()))

    def wait(self):
        """
        Wait till lock is released
        """
        while True:
            # If the lock file does not exist, stop waiting
            if not os.path.exists(self.lock_path):
                break

            # If the lock is about a dead process, stop waiting
            with open(self.lock_path, 'r') as fd:
                pid = int(fd.read())

                if int(pid) not in psutil.pids():
                    break

            # Else, wait a bit before trying again
            time.sleep(0.2)

    def release(self):
        if os.path.exists(self.lock_path):
            os.remove(self.lock_path)
