"""
description: |-
  Merge the counts of N-grams over all projects

inputs:
- alias: counts
  name: counts.csv
  parent: count_ngrams

outputs:
- alias: counts
  name: counts.csv
  driver: file

parameters: []


"""

import pandas as pd

import data_pm


class Study(data_pm.BaseStudy):
    def run(self):
        s_counts = pd.Series(dtype=int)

        # Merge counts
        for input_name, input in self.inputs.items():
            if input_name.startswith('counts__'):
                s = pd.Series.from_csv(input.path)

                s_counts = s_counts.add(s, fill_value=0)

        # Write result
        s_counts.to_csv(self.o.counts)
