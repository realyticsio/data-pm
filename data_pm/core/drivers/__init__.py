# Global imports
import importlib
import sys

# Local imports
from data_pm import settings


class DriverModules(object):
    def __init__(self):
        """
        Dict like class containing drivers modules

        Loads driver modules if necessary only
        """

        self.driver_modules = {}

    def __getitem__(self, driver_name):
        if driver_name not in self.driver_modules:
            paths = [
                'data_pm.core.drivers.{}_driver'.format(driver_name),
                'drivers.{}_driver'.format(driver_name)
            ]
            for path in paths:
                try:
                    self.driver_modules[driver_name] = importlib.import_module(path)
                    break
                except Exception:
                    continue
            else:
                raise RuntimeError('Could not load driver "{}"'.format(driver_name))

        return self.driver_modules[driver_name]


driver_modules = DriverModules()


def get_driver(driver_name, _settings=None):
    """
    Return an instantiated driver

    :param driver_name: Example: "file", "hdfs", "s3"
    :param _settings
    :rtype: data_pm.core.drivers.base_driver.BaseDriver
    """
    if _settings is not None:
        return driver_modules[driver_name].Driver(_settings=_settings)

    return driver_modules[driver_name].Driver.get_default()


def redirect(driver_name):
    if need_redirection(driver_name):
        driver_name = settings['data_pm.redirection.driver']
    return get_driver(driver_name)


def need_redirection(driver_name):
    if driver_name != 'file':
        return False

    if settings['data_pm.redirection.driver'] == 'file':
        return False
    else:
        return True


class DriverShortcut(object):
    def __init__(self, driver_name):
        """
        Shortcut to a driver.

        The underlying driver is only instantiated if necessary.

        Example use:
            hdfs = DriverShortcut('hdfs')
            hdfs.listdir()

        :param str driver_name: Example: "file", "hdfs", "s3"
        """
        self._driver_name = driver_name
        self._drivers = {}

    def __getattribute__(self, item):
        driver_name = object.__getattribute__(self, '_driver_name')
        drivers = object.__getattribute__(self, '_drivers')

        _class = driver_modules[driver_name].Driver

        def func(*args, **kwargs):
            types = (str, unicode) if sys.version_info <= (2,) else (str,)

            str_args = [x for x in list(args) + list(kwargs.values()) if isinstance(x, types)]
            parsed_args = map(_class.parse_config, str_args)

            config = next((x for x in parsed_args if x), {})

            key = tuple(config.values())

            if key not in drivers:
                drivers[key] = get_driver(driver_name, _settings=config or None)

            attr = object.__getattribute__(drivers[key], item)
            return attr(*args, **kwargs)

        if item in {'_drivers', '_driver_name', '_class'}:
            return object.__getattribute__(self, item)

        if len(drivers) == 0:
            drivers[()] = get_driver(driver_name)

        # Called on default Driver
        attr = object.__getattribute__(drivers[()], item)

        if item.startswith('__'):  # Avoid returning func when item=__class__
            return attr

        return func if callable(attr) else attr
