#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Launch studies for a set of clients
"""
# Global imports
from __future__ import print_function
import sys
import json
from requests.exceptions import ConnectionError

# Local imports
from data_pm import client
from data_pm.core.analysis_tool import AnalysisTool
from data_pm.core.factory import get_factory
from data_pm.tools.cli_handler import BaseCommand


__maintainer__ = "Nicolas Richard"


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('--projects', dest='projects', nargs='*', default=[])
        parser.add_argument('--studies', dest='studies', nargs='+')
        parser.add_argument('--branch', dest='branch', type=str,
                            help='Branch to run the code from')
        parser.add_argument('--branches-in', dest='branches_in', type=str, default='',
                            help='pre-computed branches for inputs')
        parser.add_argument('-p', '--params', dest='params', nargs='*', type=str,
                            help='Parameters in the form myparam=myvalue')
        parser.add_argument('--min-depth-force', type=int, default=0,
                            help='Force compute studies deeper than this value. '
                                 'By default, only force compute the provided studies.')
        parser.add_argument('--max-depth-check', type=int, default=1000,
                            help='Do not check the status of studies deeper than this value. '
                                 'By default, check as deep as possible (1000).')
        parser.add_argument('--ignore-parent', action='store_true',
                            help='Do not check the status of studies of parent branch.')
        parser.add_argument('--unlock', action='store_true',
                            help='Do not check if study is already running')
        parser.add_argument('--slack', dest='slack', type=str, default=None,
                            help='Send notifications to the given slack channel')
        parser.add_argument('--origin', dest='origin', type=str, default=None,
                            help='The user which issues this task')
        parser.add_argument('--task-id', dest='task_id', type=str, required=True,
                            help='Task id (if launched by a worker)')

    def handle(self, namespace):
        # Push the stdout and stderr streams to the API
        redirect_output_streams(namespace.task_id)

        if namespace.branches_in:
            assert len(namespace.studies) == 1
            study_ctrl = get_factory().create_study_control(
                study_name=namespace.studies[0],
                project_name=namespace.projects[0] if namespace.projects else None
            )
            # Clear parameter "branches_in"
            branches_in = namespace.branches_in.strip('"').replace('\\', '')

            study_ctrl.set_branches_in(json.loads(branches_in))
            study_ctrl.set_branch_out(namespace.branch)
            AnalysisTool(namespace).try_launch(study_ctrl)
        else:
            AnalysisTool(namespace).launch_studies()


def redirect_output_streams(task_id):
    """
    Configure stdout and stderr so that their content is pushed to the API

    :param str task_id:
    """
    sys.stdout = DoubleStream(sys.stdout, lambda msg: client.post_stream_stdout(task_id, msg))
    sys.stderr = DoubleStream(sys.stderr, lambda msg: client.post_stream_stderr(task_id, msg))


class DoubleStream(object):
    def __init__(self, original_fd, callback):
        """
        Interface that can replace sys.stdout and sys.stderr

        Sends content to the API and keep writing to the underlying file

        :param file original_fd: Example: sys.stdout
        :param func callback:
        """
        self.original_fd = original_fd
        self.callback = callback

    def write(self, line):
        # Write to the original file handler
        self.original_fd.write(line)

        try:
            # Push the message to the API
            self.callback(line)
        except ConnectionError as e:
            self.original_fd.write('[Warning] Failed to write stream to API: {}\n'.format(e.args))

    def flush(self):
        self.original_fd.flush()
