# Global imports
from __future__ import print_function

import os
import flask
import codecs
import pickle
import flask_restplus
from werkzeug.utils import secure_filename
from flask_restplus.fields import String

# Local imports
import data_pm
from data_pm import log, settings
from data_pm.core import paths
from data_pm.core.study_components import study_log
from data_pm.web.api import backend


ns = flask_restplus.Namespace('log')

message_meta_mdl = ns.model('MessageMeta', {
    'branch': String(required=True),
    'project': String(required=False),
    'study': String(required=True),
})

message_mdl = ns.inherit('Message', message_meta_mdl, {
    'message': String(required=True),
})


@ns.route('/')
class Index(flask_restplus.Resource):
    @ns.expect(message_mdl)
    def post(self):
        """
        Append a message to a file
        """

        args = flask.request.json

        p = args.get('project')
        if not p or p == 'None':
            args['project'] = None
        else:
            args['project'] = secure_filename(p)

        branch = secure_filename(args['branch'])
        study = secure_filename(args['study'])
        project = args['project']
        message = args['message']

        d_meta = study_log.parse_meta_log(message)

        if d_meta.get('keyword') == 'start_date':
            do_rollover = True
        else:
            do_rollover = False

        # Write message on filesystem
        dir_path = paths.path_to_logs(data_pm.file, branch, study, project)
        file_path = data_pm.file.join(dir_path, settings['data_pm.log.filename'])
        log_on_file_system(file_path, message, do_rollover)

        # Update status in database
        if d_meta.get('keyword') == 'datetime':
            backend.update_last_update(
                branch=branch,
                study=study,
                project=project,
                timestamp=d_meta['value']
            )
        elif len(d_meta) > 0:
            # todo: improve conditions
            backend.update_study_log(
                branch=branch,
                study=study,
                project=project,
                column=d_meta['keyword'],
                value=d_meta['value']
            )

        return {'message': 'ok'}

    @ns.expect(message_meta_mdl, validate=True)
    def get(self):
        """
        Get a full log from a file
        """
        args = flask.request.json

        branch = secure_filename(args['branch'])
        study = secure_filename(args['study'])
        project = secure_filename(args['project']) if args['project'] else None

        dir_path = paths.path_to_logs(data_pm.file, branch, study, project)
        file_path = data_pm.file.join(dir_path, settings['data_pm.log.filename'])

        try:
            reader = study_log.StudyLogReader(file_path)
        except OSError:
            reader = None

        return {'StudyLogReader': pickle.dumps(reader)}


def log_on_file_system(path, message, do_rollover):
    """
    Write log message on local filesystem:
        PROJECTS/.../LOG/...
        SHARED/LOG/...

    Create directory if not exist
    """

    dir_path = os.path.dirname(path)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    # Rollover if needed
    log.create_handler('rotating_file', path=path, rollover=do_rollover)

    if not message.endswith('\n'):
        message += '\n'

    with open(path, 'a') as fd:
        fd.write(message)


###################################################


@ns.route('/stream/<stream_name>/<task_id>')
class Stream(flask_restplus.Resource):
    def post(self, stream_name, task_id):
        task_id = secure_filename(task_id)
        stream_name = secure_filename(stream_name)

        dir_path = data_pm.file.join(data_pm.settings['data_pm.driver.file.tmpdir'], 'streams', task_id)
        data_pm.file.makedirs(dir_path)

        with open(data_pm.file.join(dir_path, stream_name), 'a+') as fd:
            fd.write(flask.request.data.decode('utf-8'))

        return {'message': 'ok'}

    def get(self, stream_name, task_id):
        task_id = secure_filename(task_id)
        stream_name = secure_filename(stream_name)

        args = flask.request.json
        pos = args.get('pos')

        dir_path = data_pm.file.join(data_pm.settings['data_pm.driver.file.tmpdir'], 'streams', task_id)
        data_pm.file.makedirs(dir_path)

        if not data_pm.file.exists(data_pm.file.join(dir_path, stream_name)):
            return flask.Response('', mimetype='text/plain')

        with codecs.open(data_pm.file.join(dir_path, stream_name), encoding='utf-8') as fd:
            fd.seek(pos)
            data = fd.read(16*1024)
            return flask.Response(data, mimetype='text/plain')

    def delete(self, stream_name, task_id):
        dir_path = data_pm.file.join(data_pm.settings['data_pm.driver.file.tmpdir'], 'streams', task_id)
        stream_path = data_pm.file.join(dir_path, stream_name)

        if data_pm.file.exists(stream_path):
            data_pm.file.remove(stream_path)

        if data_pm.file.exists(dir_path) and len(data_pm.file.listdir(dir_path)) == 0:
            data_pm.file.remove(dir_path, recursive=True)

        return {'message': 'ok'}
