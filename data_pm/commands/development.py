#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Start development services (API and celery worker)
"""

# Global imports
from __future__ import print_function
import subprocess
import sys
import os

# Local imports
from data_pm.tools.cli_handler import BaseCommand


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        service_subparsers = parser.add_subparsers(dest="service", help="Service to start")

        server_parser = service_subparsers.add_parser('server')
        server_parser.add_argument('--allow-branch-switch',
                                   help='Allows branch switch. See data_pm.web.dashboard.tools.repositories',
                                   action='store_true')

        service_subparsers.add_parser('worker')

    def handle(self, namespace):
        if namespace.service == 'server':
            start_server(namespace.allow_branch_switch)
        elif namespace.service == 'worker':
            start_worker()


def start_server(allow_branch_switch):
    """
    Start a flask debug server to act as a management API

    :param bool allow_branch_switch:
    """

    # Add current path to PYTHONPATH
    if os.getcwd() not in sys.path:
        sys.path.insert(0, os.getcwd())

    if not allow_branch_switch:
        # Monkey patch to disable the switch branch feature
        from data_pm.web.dashboard.tools import repositories

        def donothingwrapper(func):
            return func

        repositories.switch_branch = donothingwrapper
        print('Do not use switch the branch switch button on the dashboard')

    from data_pm.web.dashboard import main
    application = main.app

    application.run(host='0.0.0.0', port=5000, debug=True, threaded=True)
    # application.run(host='0.0.0.0', port=5000, use_debugger=False)  # to use line profiler


def start_worker():
    subprocess.call(['celery', 'worker', '-A', 'data_pm.deployment.tasks', '-E', '-Q', 'default'])

