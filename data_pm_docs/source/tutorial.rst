========
Tutorial
========


Initialize the workspace
************************
The workspace is the main directory that stores the code of studies and potentially project data.
If you are starting from scratch, you need to create a new workspace::

      pm init my_workspace

This will create the directory ``my_workspace`` in your current path and several sub-folders::

    my_workspace
    ├── DEV
    │   ├── SHARED_STUDIES   # Repository of shared studies
    │   └── STUDIES          # Repository of studies
    ├── PROJECTS             # Contains the data that resides on the local filesystem, segmented by project name
    └── settings             # Configuration that may overwrite the default configuration of data_pm


Create a project
****************
A *project* is an instance of coherent data, an isolated context of analysis.
It should be considered as a marketing project rather than an IT project.
For example, a *project* may be a B2B client: we want to process the data of each client with the same code to deliver them
the same experience, but we do not want data to be mixed between clients.
In fact, data_pm provides ways of mixing data between *projects*, so the main point of *projects* is to clearly identify
the entity to which the data belong to.

The most common case is to launch a *study* for a *project*.
It means the input data of this *study* for this *project* will be processed to create the output data of this *study* for
this *project*. You may need to launch several studies for a bunch of *projects*: then each study will be launched for
each *project*.

Create a project by running::

    pm create_project supersite_com

This will simply create a folder under the path ``PROJECTS/trackedWebsite1``.
From now on, this project exists and will be recognized when using ``pm`` commands.


Create a study
**************
A *study* can just be seen as a black box that gets input data and yields output data.
It is generally a good idea to make a *study* process a well defined elementary task, such as "compute sessions", or
"upload time series to S3".

In concrete terms, a *study* is a python module (basically a .py file) located in ``DEV/STUDIES``.
For example, a valid path for a *study* would be ``DEV/STUDIES/my_study.py``, and the *study* name would be ``my_study``.
You can organize studies into packages, for example, ``DEV/STUDIES/events/my_study.py`` is fine,
and the *study* name would be ``events.my_study``.

This file must contains definitions that characterize the *study*: the inputs, the outputs, the parameters
and the code that will be run to transform the inputs into outputs.

To show the connections between studies, we are going to create two studies:
- The first study ``sessions_downloading`` will act as if we were downloading of file filled with events.
- The second study ``timeseries_computing`` will count the number of events by date.
Your directory tree will look like this::

    DEV
    ├── __init__.py
    └── STUDIES
        ├── __init__.py
        ├── events_computing.py
        └── timeseries_computing.py

Fill ``DEV/STUDIES/events_computing.py`` with this code::

    import data_pm

    class Study(data_pm.BaseStudy):
        inputs = {
        }

        outputs = {
            'downloaded_events': data_pm.Output('events.csv')
        }

        parameters = {
        }

        def run(self):
            # Let's suppose that this string was not hardcoded but downloaded from some place that is not part of data_pm
            data = '''dde,2015-10-25
    afv,2015-10-22
    tso,2015-10-25
    aop,2015-10-21
    pmo,2015-10-25
    mla,2015-10-21
    '''

            with open(self.o.downloaded_events, 'w') as fp:
                fp.write(data)


The class ``Study`` can not be named otherwise and must inherit from
:py:class:`data_pm.BaseStudy <data_pm.core.study.BaseStudy>`.

The class documentation explains in details how to fill ``inputs``, ``outputs``, ``parameters`` and ``run``.
Here, we don't use the ``inputs`` dictionary at all because this study really is
the entry point of data into the workspace.
The ``outputs`` dictionary contains a single entry: the key ``downloaded_events`` is just an alias that can be used in the
code, while the parameter ``'events.csv'`` filled in the Output class is the name of the output and determines the path
data will be stored.

The ``run`` method must take care of writing the outputs to their destination,
which is why we write to ``self.o.downloaded_events``.
If a study does not create/update its output, a warning message will be logged.

Now, fill the second study ``DEV/STUDIES/timeseries_computing.py`` with this code::

    import pandas as pd
    import data_pm

    class Study(data_pm.BaseStudy):
        inputs = {
            'downloaded_events': data_pm.Input('events_downloading', 'events')
        }

        outputs = {
            'timeseries': data_pm.Output('timeseries.csv')
        }

        parameters = {
        }

        def run(self):
            # Open and parse the input file
            df = pd.read_csv(self.i.downloaded_events, names=['user_id', 'date'])
            self.logger.info('Loaded {} events'.format(len(df)))

            timeseries = df.groupby('date').count()
            self.logger.info('Created a timeseries with {} dates'.format(len(timeseries)))

            timeseries.to_csv(self.o.timeseries)


You can now launch your second study::

    pm launch timeseries_computing supersite_com



Launch studies
**************

**A study needs all its inputs to exist and be up-to-date to run**. When you launch a study, data-pm will look for
its parent studies, will try to launch them if it is necessary, and so on, until studies with no inputs are found.

    pm launch supersite_com timeseries_computing
    # It will run events_computing, then timeseries_computing, because events_computing's output do not exist

    pm launch supersite_com timeseries_computing
    # It will run timeseries_computing only, because events_computing has no parent and will always be up-to-date

    pm launch supersite_com events_computing
    # It will run events_computing only, because events_computing has no parents

    pm launch supersite_com timeseries_computing
    # It will run events_computing, then timeseries_computing, because events_computing was updated,
    # so timeseries_computing was not up-to-date anymore

Multiple projects and multiple studies can be launched with the same command, meaning that all the provided studies
(and their dependencies) will be computed for all the provided projects.

**Submit process**

.. image:: _static/pm-submit.png

**Data streams**

.. image:: _static/pm-submit-streams.png


**Data-pm instances**

==============  ==================  =================================  ===============
Location        Application         Working directory                  Python packages
==============  ==================  =================================  ===============
Your machine    pm submit           Your workspace                     Your environment
data-pm server  apache2+mod_wsgi    Configured in apache2 site         Global packages
data-pm worker  celery worker       Configured by celery start script  Global packages
data-pm worker  pm launchforworker  <data_pm.environments.dir>/<hash>  virtualenv
==============  ==================  =================================  ===============


Parameters
**********

Parameters allows running studies with small variations.

They are particularly useful when it comes to tuning some algorithms variables, or to easily define project based configurations.
They are directly defined in the studies.

Their values may be defined and overwritten from the following sources:

- From the code, as defaut values in :py:class:`Parameter <data_pm.core.parameter.Parameter>`
- From the file ``parameters.yml``
- From the command line, for example when using :doc:`pm launch <commands>`


To use a parameter in a study, you must first define it in the `parameters` dict of the `Study` class.
Then you can get the value either from the parameter or the shortcut::

    import data_pm

    class Study(data_pm.BaseStudy):
        inputs = {}
        outputs = {}
        parameters = {
            'my_param': data_pm.Parameter('default_value')
        }

        def run(self):
            # These two lines will print "default_value"
            print self.parameters['my_param'].value
            print self.p.my_param



Example `parameters.yml`. Structure: project->study->param->value::

    project_a:
      my_study:
        start: '2014-01-01'
        end: '2015-01-01'

    project_b:
      my_study:
        start: '2012-01-01'
        end: '2015-01-01'

      my_other_study:
        another_param: '09876'
