# Global imports
import subprocess
import data_pm
import shutil

# Local imports
from data_pm.core.drivers.base_driver import FileStatus, BaseDriver


class Binding(object):
    """
    This class reprensents a pair of paths between the source and the destination filesystems.

    When an operation is done on the destination, call the method `expire` to prevent from
    reading the obselete destination status if it exists.
    """

    def __init__(self, src=None, dst=None, src_status=None, dst_status=None):
        assert src_status or dst_status, 'A binding points to at least one existing file'
        assert src or src_status, 'A source must be specified'
        assert dst or dst_status, 'A destination must be specified'

        if src_status is not None:
            self.relative_path = src_status.relative_path
        else:
            self.relative_path = dst_status.relative_path

        self.src = src or src_status.absolute_path
        self.dst = dst or dst_status.absolute_path

        self._src_entry = Entry(self.src, file_status=src_status)
        self._dst_entry = Entry(self.dst, file_status=dst_status)

        self._dst_status_expired = False

    @property
    def src_entry(self):
        return self._src_entry

    @property
    def dst_entry(self):
        if self._dst_status_expired is True:
            raise ValueError('The destination status is now obselete for path "{}"'
                             .format(self.dst))
        return self._dst_entry

    def src_exists(self):
        return self.src_entry.exists()

    def dst_exists(self):
        return self.dst_entry.exists()

    def expire(self):
        """
        The destination status expires if an IO operation is done on it
        """
        if self._dst_status_expired is True:
            raise ValueError('The destination status has already expired')

        self._dst_status_expired = True

    def __repr__(self):
        return '<Binding(\'{}\'\n  {}\n  {})>\n\n' \
            .format(self.relative_path, self._src_entry, self._dst_entry)


class Entry(object):
    """
    This class is a wrapper for the class FileStatus
    """

    def __init__(self, absolute_path, file_status=None):
        """
        :param str absolute_path:
        :param FileStatus? file_status:
        """
        self.absolute_path = absolute_path
        self._file_status = file_status

        if file_status is not None \
                and not file_status.is_directory \
                and absolute_path.endswith('/'):
            raise ValueError('"{}" is not a directory but ends with a slash'.format(absolute_path))

    def exists(self):
        return self._file_status is not None

    def __getattribute__(self, item):
        if item in {'absolute_path', 'exists', '_file_status'} or item.startswith('__'):
            return object.__getattribute__(self, item)

        if self._file_status is None:
            raise RuntimeError('You can not access "{}" if entry does not exist'.format(item))

        return object.__getattribute__(self._file_status, item)

    def __repr__(self):
        return '<Entry({}, exists={})>'.format(self.absolute_path, self.exists())


def get_function(src_driver, dst_driver):
    """
    Convenient way to pick a transfer function between two filesystems

    :param BaseDriver src_driver:
    :param BaseDriver dst_driver:
    :return: func
    """

    connections = {
        ('hdfs', 's3'): hdfs_to_s3,
        ('s3', 'hdfs'): s3_to_hdfs,
        ('file', 'hdfs'): local_to_hdfs,
        ('hdfs', 'file'): hdfs_to_local,
        ('file', 'file'): local_to_local,
        ('file', 's3'): local_to_s3,
        ('s3', 'file'): s3_to_local,
    }

    try:
        return connections[(src_driver.name, dst_driver.name)]
    except KeyError:
        raise KeyError('There is not function to transfer data from "{}" to "{}"'
                       .format(src_driver.name, dst_driver.name))


def check_need_sync(binding, method):
    """
    Check whether a sync is needed

    :param Binding binding:
    :param str method: "always": always write to destination,
                       "date": write to destination if the source files are more recent,
                       "size": write to destination if the file size differs
    :rtype: bool
    """

    if not binding.src_exists():
        raise ValueError('Source path "{}" does not exist')

    if method not in {'always', 'date', 'size'}:
        raise ValueError('Incorrect value for method: "{}"'.format(method))

    if method == 'always':
        return True

    if method == 'date':
        if not binding.dst_exists() or binding.src_entry.mtime > binding.dst_entry.mtime:
            return True

    if method == 'size':
        if not binding.dst_exists() or binding.src_entry.size != binding.dst_entry.size:
            return True

    return False


def generate_bindings(src, src_driver, dst, dst_driver):
    """
    Generates differences and matches between source and destination

    :param str src:
    :param BaseDriver src_driver:
    :param str dst:
    :param BaseDriver dst_driver:
    :return: iterator of (Entry, Entry)
    """

    if not src_driver.exists(src):
        raise ValueError('Source path "{}" does not exist'.format(src))

    if dst_driver.exists(dst):
        if src_driver.isdir(src) and dst_driver.isfile(dst):
            raise RuntimeError('Cannot overwrite non-directory with a directory')
        if src_driver.isfile(src) and dst_driver.isdir(dst):
            raise RuntimeError('Cannot overwrite directory with a non-directory')

    # List existing source files
    d_src = {
        file_status.relative_path: file_status
        for file_status in src_driver.list_statuses(src, recursive=True)
    }

    # List existing destination files
    d_dst = {
        file_status.relative_path: file_status
        for file_status in dst_driver.list_statuses(dst, recursive=True)
    } if dst_driver.exists(dst) else {}

    # Generating pairs from source
    for src_status in d_src.values():
        if src_status.relative_path in d_dst:
            yield Binding(src_status=src_status, dst_status=d_dst[src_status.relative_path])
        else:
            dst_path = dst_driver.join(dst, src_status.relative_path) if src_status.relative_path else dst
            yield Binding(src_status=src_status, dst=dst_path)

    # Generating pairs from destination
    for dst_status in d_dst.values():
        if dst_status.relative_path not in d_src:
            src_path = dst_driver.join(src, dst_status.relative_path) if dst_status.relative_path else src
            yield Binding(src=src_path, dst_status=dst_status)


def synchronize(src, src_driver, dst, dst_driver, method='always',
                preserve_mtimes=False, delete_after=False, progress_callback=None):
    """
    Synchronizes files/directories between ``src`` and ``dst``

    The behavior of this function is similar to the behavior of the `rsync` command
    (although there are far fewer options)

    However, we can highlight two differences:
        - If src and dst point to directories:
            `rsync -r src dst` moves src plus its content in dst
            `rsync -r src/ dst` moves only src content in dst
            synchronize(src, ..., dst, ...) always moves only src content in dst, slash doesn't count

        - If src points to a file and dst to a directory:
            `rsync src dst` moves the file src under dst
            synchronize(src, ..., dst, ...) raises an exception

    :param str src:
    :param BaseDriver src_driver:
    :param str dst:
    :param BaseDriver dst_driver:
    :param str method: the condition used to transfer a file
    :param bool preserve_mtimes: sets modification times on transfered files as in source
    :param bool delete_after: deletes extraneous files from the receiving side
    :param progress_callback: this function is called with the progress of the synchronization
    """

    src = src_driver.abspath(src)
    dst = dst_driver.abspath(dst)

    # Pick function for transfer
    transfer_func = get_function(src_driver, dst_driver)

    # Generate bindings
    # raises Exception
    bindings = list(generate_bindings(src, src_driver, dst, dst_driver))

    # Filter pairs based on synchronization method
    bindings_to_sync = [b for b in bindings if b.src_exists() and check_need_sync(b, method)]

    total_size = sum([float(b.src_entry.size) for b in bindings_to_sync]) if progress_callback is not None else 1.

    # Transfer files
    for binding in bindings_to_sync:
        transfer_func(binding.src, binding.dst)
        binding.expire()

        if progress_callback is not None and total_size > 0.:
            progress_callback(binding.src_entry.size / total_size)

    # Set mtimes in destination as in source
    if preserve_mtimes is True:
        for binding in bindings_to_sync:
            if src_driver.isfile(binding.src):
                # We don't want to synchronise timestamp of directory to handle s3 (that doesn't have directories).
                dst_driver.setmtime(binding.dst, binding.src_entry.mtime)

    # Delete destination files/directories that do not exist in source
    if delete_after is True:
        destinations_to_delete = [b.dst for b in bindings if not b.src_exists()]
        for path in destinations_to_delete:
            # Call to exists() is not optimal
            if dst_driver.exists(path):
                dst_driver.remove(path, recursive=True)


def hdfs_to_s3(src, dst):
    """
    Synchronize a file or a folder from HDFS to S3

    Copies a file with the best method according to the file size.

    :param str src:
    :param str dst:
    """
    if not dst.startswith('s3://'):
        raise ValueError('Destination URL "{}" does not start with a scheme (s3://)'.format(dst))

    if data_pm.hdfs.isfile(src):
        if data_pm.hdfs.getsize(src) > 64e6*10:
            _hdfs_to_s3_distributed(src, dst)
        else:
            _hdfs_to_s3_local(src, dst)
    # No need to copy the directory, s3 doesn't handle them.


def _hdfs_to_s3_distributed(src, dst):
    """
    Copies a file with "hadoop distcp" within a subprocess.
    Performs well with large files.

    :param str src: Source path
    :param str dst: Destination path
    """

    dst = dst.replace('s3://', 's3a://')

    cmd = ['/opt/hadoop/bin/hadoop', 'distcp', src, dst]

    subprocess.check_output(cmd, stderr=subprocess.PIPE)


def _hdfs_to_s3_local(src, dst):
    """
    Copies a file by reading from hdfs and writing to S3 locally.
    Performs well with small files.

    :param str src: Source path
    :param str dst: Destination path
    """

    with data_pm.hdfs.read(src) as fd_in:
        with data_pm.s3.write(dst) as fd_out:
            while True:
                bulk = fd_in.read(1024 * 1024)
                if len(bulk) == 0:
                    break
                fd_out.write(bulk)


def s3_to_hdfs(src, dst):
    """
    Copies a file with the best method according to the file size.

    :param str src:
    :param str dst:
    """

    if not src.startswith('s3://'):
        raise ValueError('Source URL "{}" does not start with a scheme (s3://)'.format(src))

    if data_pm.s3.getsize(src) > 64e6*10:
        _s3_to_hdfs_distributed(src, dst)
    else:
        _s3_to_hdfs_local(src, dst)


def _s3_to_hdfs_distributed(src, dst):
    """
    Copies a file with "hadoop distcp" within a subprocess.
    Performs well with large files.

    :param str src: Source path
    :param str dst: Destination path
    """

    src = src.replace('s3://', 's3a://')

    cmd = ['/opt/hadoop/bin/hadoop', 'distcp', src, dst]

    subprocess.check_output(cmd, stderr=subprocess.PIPE)


def _s3_to_hdfs_local(src, dst):
    """
    Copies a file by reading from S3 and writing to HDFS locally.
    Performs well with small files.

    :param str src: Source path
    :param str dst: Destination path
    """

    with data_pm.s3.read(src) as fd_in:
        with data_pm.hdfs.write(dst) as fd_out:
            while True:
                bulk = fd_in.read(1024 * 1024)
                if len(bulk) == 0:
                    break
                fd_out.write(bulk)


def hdfs_to_local(src, dst):
    """
    Download a file from HDFS to local filesystem

    Example: hdfs_to_local('file.lzo', 'folder/file.lzo')
    Example: hdfs_to_local('folder/', '/tmp/folder/')
    """

    if data_pm.hdfs.isdir(src):
        data_pm.file.makedirs(dst)
    else:
        # data_pm.hdfs.download() fails if destination exists
        if data_pm.file.exists(dst):
            # Fails if destination is a directory, it's a protection
            data_pm.file.remove(dst)

        # Makes intermediate directories if needed.
        # This is necessary for the download unlike the upload
        data_pm.file.makedirs(data_pm.file.dirname(dst))

        data_pm.hdfs.download(src, dst)


def local_to_hdfs(src, dst):
    """
    Upload a file from local filesystem to HDFS

    Example: local_to_hdfs('file.lzo', 'folder/file.lzo')
    Example: local_to_hdfs('folder/', '/tmp/folder/')
    """

    if data_pm.file.isdir(src):
        data_pm.hdfs.makedirs(dst)
    else:
        # data_pm.hdfs.upload() fails if destination exists
        if data_pm.hdfs.exists(dst):
            # Fails if destination is a directory, it's a protection
            data_pm.hdfs.remove(dst)

        data_pm.hdfs.upload(src, dst)


def local_to_local(src, dst):
    if data_pm.file.isdir(src):
        data_pm.file.makedirs(dst)
    else:
        # Make intermediate directories if needed
        if not data_pm.file.exists(data_pm.file.dirname(dst)):
            data_pm.file.makedirs(data_pm.file.dirname(dst))

        shutil.copy(src, dst)

def s3_to_local(src, dst):
    """
    Download a file from S3 to local filesystem

    Example: s3_to_local('file.lzo', 'folder/file.lzo')
    Example: s3_to_local('folder/', '/tmp/folder/')
    """

    if not data_pm.s3.isfile(src):
        data_pm.file.makedirs(dst)
    else:
        # data_pm.s3.download() fails if destination exists
        if data_pm.file.exists(dst):
            # Fails if destination is a directory, it's a protection
            data_pm.file.remove(dst)

        # Makes intermediate directories if needed.
        # This is necessary for the download unlike the upload
        data_pm.file.makedirs(data_pm.file.dirname(dst))

        data_pm.s3.download(src, dst)


def local_to_s3(src, dst):
    """
    Upload a file from local filesystem to S3

    Example: local_to_s3('file.lzo', 'folder/file.lzo')
    Example: local_to_s3('folder/', '/tmp/folder/')
    """

    if data_pm.file.isdir(src):
        # We don't synchronise empty directory, s3 doesn't handle directory.
        pass
    else:
        # data_pm.s3.upload() fails if destination exists
        if data_pm.s3.exists(dst):
            # Fails if destination is a directory, it's a protection
            data_pm.s3.remove(dst)

        data_pm.s3.upload(src, dst)