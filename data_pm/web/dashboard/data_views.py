# Global imports
import flask
import pandas as pd
import data_pm
import shutil

# Local imports
from data_pm.core import paths
from data_pm.web.dashboard.tools import repositories
from data_pm.core.factory import get_factory


blueprint = flask.Blueprint('data', __name__)


@blueprint.route('/download/<project>/DATA/<branch>/<study>/<output>/', endpoint='download')
@blueprint.route('/download/<project>/DATA/<branch>/<study>/<output>/<path:path>', endpoint='download')
@blueprint.route('/download/SHARED_DATA/<branch>/<study>/<output>/', endpoint='download')
@blueprint.route('/download/SHARED_DATA/<branch>/<study>/<output>/<path:path>', endpoint='download')
@repositories.switch_branch
def download(study, branch, output, path='', project=None):
    s = get_factory().create_study_control(
        study_name=study,
        project_name=project,
        configure_branch=True
    )

    o = s.outputs[output]

    absolute_path = o.resource.path
    if path:
        absolute_path = o.resource.driver.join(o.resource.path, path)

    driver = o.resource.driver

    if not driver.exists(absolute_path):
        return flask.abort(404, 'File does not exist')
    elif o.resource.driver.name not in ('file', 'hdfs', 's3'):
        return flask.abort(400, 'No way to return data')
    elif o.resource.driver.isdir(absolute_path):
        return flask.abort(400, 'Can not download a directory (or please implement .zip download)')

    else:
        fd = Reader(o.resource.driver.read(absolute_path))
        return flask.send_file(fd, as_attachment=True, attachment_filename=driver.basename(absolute_path),
                               last_modified=driver.getmtime(absolute_path))


class Reader:
    """
    Emulate a file object from a context manager
    """
    def __init__(self, cm):
        self._cm = cm
        self._fd = self._cm.__enter__()

    def read(self, *args, **kwargs):
        return self._fd.read(*args, **kwargs)

    def close(self):
        self._cm.__exit__(None, None, None)


@blueprint.route('/view/<project>/DATA/<branch>/<study>/', endpoint='study_view', methods=['GET', 'POST'])
@blueprint.route('/view/SHARED_DATA/<branch>/<study>/', endpoint='study_view', methods=['GET', 'POST'])
def study_view(study, branch, project=None):
    dir_path = paths.path_to_views(
        driver=data_pm.file,
        branch_name=branch,
        study_name=study,
        project_name=project
    )

    file_path = data_pm.file.join(dir_path, 'view.html')

    if data_pm.file.exists(file_path):
        # Returns the html view file
        with open(file_path) as fd:
            return fd.read()
    else:
        study_ctrl = get_factory().create_study_control(
            study_name=study,
            project_name=project,
            configure_branch=True
        )
        l_entries = [
            {
                'url': flask.url_for('.view', project=project, branch=branch, study=study, output=output_name),
                'name': o.name,
                'mtime': o.get_mtime(),
                'size': o.driver.getsize(o.resource.path) if o.driver.exists(o.resource.path) else ''
            }
            for output_name, o in study_ctrl.outputs.items()
        ]
        l_entries = sorted(l_entries, key=lambda x: x['name'])
        return flask.render_template('dashboard_app/snippets/directory-index.html', entries=l_entries)


@blueprint.route('/view/<project>/DATA/<branch>/<study>/<output>/', endpoint='view')
@blueprint.route('/view/<project>/DATA/<branch>/<study>/<output>/<path:path>', endpoint='view')
@blueprint.route('/view/SHARED_DATA/<branch>/<study>/<output>/', endpoint='view')
@blueprint.route('/view/SHARED_DATA/<branch>/<study>/<output>/<path:path>', endpoint='view')
@repositories.switch_branch
def view(study, branch, output, path='', project=None):
    s = get_factory().create_study_control(
        study_name=study,
        project_name=project,
        configure_branch=True
    )

    o = s.outputs[output]

    absolute_path = o.resource.path
    if path:
        absolute_path = o.resource.driver.join(o.resource.path, path)

    driver = o.resource.driver

    # If the path does not exist, return 404
    if not driver.exists(absolute_path):
        return flask.abort(404, 'Path "{}" doesn\'t exist'.format(absolute_path))

    # Else if the driver does not support showing data, return 400
    elif driver.name not in ('file', 'hdfs', 's3'):
        return flask.abort(400, 'No way to get data')

    # Else if the study is configured to show data, use it
    elif not path and hasattr(s, 'view_' + output):
        o.use_tmp_dir = False  # Hack to force download hdfs->file
        o.activate()
        resp = getattr(s, 'view_' + output)(**flask.request.args)
        o.deactivate()
        return resp

    # Else if the requested path is a directory, return an index
    elif not driver.isfile(absolute_path):
        l_entries = [
            {'url': flask.url_for('.view', project=project, branch=branch, study=study, output=output,
                                  path=driver.join(path, status.relative_path)),
             'name': status.relative_path,
             'mtime': status.mtime,
             'size': status.size,
             } for status in driver.list_statuses(absolute_path)]
        l_entries = sorted(l_entries, key=lambda x: x['name'])

        return flask.render_template('dashboard_app/snippets/directory-index.html', entries=l_entries)

    # Else if the requested path is a parquet file, return a sample in a table
    elif absolute_path.endswith('.parquet') and driver.getsize(absolute_path) < 10e6:
        with DownloadableFile(driver, absolute_path) as tmp_path:
            df = pd.read_parquet(tmp_path)
        return flask.Response(table_style + df.to_html(max_rows=1000))

    # Else if the requested path is a HDF file, return a sample in a table
    elif absolute_path.endswith('.h5') and driver.getsize(absolute_path) < 10e6:
        with DownloadableFile(driver, absolute_path) as tmp_path:
            df = pd.read_hdf(tmp_path)
        if isinstance(df, pd.Series):
            df = df.to_frame()
        return flask.Response(table_style + df.to_html(max_rows=1000))

    # Else if the requested path is a HTML file, return that file
    elif absolute_path.endswith('.html') and driver.getsize(absolute_path) < 10e6:
        with DownloadableFile(driver, absolute_path) as tmp_path:
            with open(tmp_path) as fd:
                txt = fd.read()
        return flask.Response(txt)

    # Else, return the raw data
    else:
        return flask.redirect(flask.url_for('.download', project=project, study=study, branch=branch, output=output,
                                            path=path))


class DownloadableFile(object):
    """
    Download a file to a temporary location.

    Used within a with statement.
    """
    def __init__(self, driver, absolute_path):
        self.driver = driver
        self.absolute_path = absolute_path
        self.tmp_file = data_pm.file.TempFile(create=False)

    def __enter__(self):
        # Download file to a temp location
        with self.driver.read(self.absolute_path) as fd_in:
            with open(self.tmp_file.path, 'wb') as fd_out:
                shutil.copyfileobj(fd_in, fd_out)

        return self.tmp_file.path

    def __exit__(self, exc_type, exc_val, exc_tb):
        # Remove temporary file
        self.tmp_file.remove()


table_style = '''<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 6px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}

tr:hover {background-color:#f5f5f5;}
</style>
'''
