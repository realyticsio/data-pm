import flask_restplus
import flask
import json
import os
from werkzeug.utils import secure_filename

ns = flask_restplus.Namespace('debug')


'''
{'control_port': 45619,
 'hb_port': 41864,
 'iopub_port': 42526,
 'ip': '127.0.0.1',
 'kernel_name': '',
 'key': '65ead38a-3d5afc03f84ad234b6180e27',
 'shell_port': 41253,
 'signature_scheme': 'hmac-sha256',
 'stdin_port': 38834,
 'transport': 'tcp'}
'''

CONFIG_DIR = '/tmp'


@ns.route('/<task_id>')
class Index(flask_restplus.Resource):
    def post(self, task_id):
        """
        Push the connection file to attach to a IPython Kernel
        """

        task_id = secure_filename(task_id)
        filename = '{}.json'.format(task_id)
        path = os.path.join(CONFIG_DIR, filename)

        with open(path, 'w') as fd:
            fd.write(json.dumps(flask.request.json))

        return {'message': "ok"}

    def get(self, task_id):
        """
        Get the connection file to attach to a IPython Kernel
        """
        task_id = secure_filename(task_id)
        filename = '{}.json'.format(task_id)
        path = os.path.join(CONFIG_DIR, filename)

        if not os.path.exists(path):
            return {"error": "Could not find file"}, 404

        with open(path) as fd:
            content = fd.read()

        os.remove(path)

        return json.loads(content)

    def delete(self, task_id):
        """
        Delete the connection file
        """
        task_id = secure_filename(task_id)
        filename = '{}.json'.format(task_id)
        path = os.path.join(CONFIG_DIR, filename)

        if os.path.exists(path):
            os.remove(path)

        return {'message': 'ok'}

