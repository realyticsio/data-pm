# Global imports
import yaml
import networkx as nx
from networkx.exception import NetworkXNoCycle
from collections import OrderedDict

# Local imports
from data_pm import settings


class BranchConfiguration(object):

    def __init__(self, d_conf):
        """
        :param dict[srt,Any] d_conf:
        """
        self.d_conf = d_conf
        self.graph = None
        self._d_branches = OrderedDict()
        """Preserves writing order to be able to retrieve the branches hierarchy"""

    @staticmethod
    def from_file():
        try:
            with open(settings['data_pm.branches.path']) as fd:
                yaml_str = fd.read()
            d_conf = yaml.load(yaml_str, Loader=yaml.Loader)
        except yaml.YAMLError as e:
            raise e
        except Exception:
            raise Exception(
                'Failed to load file "{}": check if the file exists'
                .format(settings['data_pm.branches.path'])
            )

        return BranchConfiguration(d_conf=d_conf)

    @property
    def sources(self):
        g = self.generate_graph()
        sources = [n for n in g.nodes() if g.in_degree[n] == 0]
        return sources

    def generate_graph(self):
        if self.graph is not None:
            return self.graph

        graph = nx.DiGraph()

        for branch_name, sub_conf in self.d_conf.items():
            graph.add_node(branch_name, recompute_from=sub_conf['recompute_from'])
            if sub_conf['parent'] is not None:
                graph.add_edge(branch_name, sub_conf['parent'])

        self.graph = graph

        return graph

    def raise_if_error(self):
        g = self.generate_graph()

        if len(self.sources) != 1:
            raise ValueError('Bad configuration: 1 branch child expected, {} found'.format(len(self.sources)))

        for n in g.nodes():
            if g.out_degree[n] > 1:
                raise ValueError('Bad configuration: branch "{}" has more than one parent'.format(n))

        try:
            nx.find_cycle(g)
            raise ValueError('Bad configuration: loop detected in branches.yml')
        except NetworkXNoCycle:
            pass

    def get_parent(self, branch_name):
        self.raise_if_error()
        return self.d_conf[branch_name]['parent']

    def get_hierarchy(self):
        """
        Returns a dictionary with branch names (as keys) and their associated studies (as values)
        The "associated studies" are the one from which we recompute the branch

        :return: dict[str, list[str]] d_branches
        """
        self.raise_if_error()

        if len(self._d_branches) > 0:
            # Already loaded
            return self._d_branches

        if len(self.graph) == 1:
            # One branch, no parent
            node, data = list(self.graph.nodes.data())[0]
            self._d_branches[node] = data['recompute_from'] or []
            return self._d_branches

        # Make result, children first
        src = self.sources[0]
        for i, (a, b) in enumerate(nx.edge_dfs(self.graph, src)):
            if i == 0:
                self._d_branches[a] = self.graph.nodes[a].get('recompute_from') or []
            self._d_branches[b] = self.graph.nodes[b].get('recompute_from') or []

        return self._d_branches
