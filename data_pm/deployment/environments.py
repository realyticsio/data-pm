#!/usr/bin/python
# -*- coding: utf-8 -*-

# Global imports
import re
import os
import time
import socket
import subprocess
import hashlib
import venv
from distutils.spawn import find_executable
from requests.exceptions import HTTPError

# Local imports
import data_pm
from data_pm import settings, client
from data_pm.deployment import snapshots


class WorkingDirHelper(object):
    """
    This class has 3 use cases:
        - (pm launch)
            Making a snapshot of a git repository and using it as working dir

        - (pm submit - part 1)
            Making a snapshot of a git repository and uploading it to the API

        - (pm submit - part 2)
            Retrieving a repository from the API and using it as working dir
    """

    def __init__(self):
        self.tmp_working_dir = None
        self.python_env = None  # Virtualenv directory

    @property
    def path(self):
        if self.tmp_working_dir is not None:
            return self.tmp_working_dir.path

    def snapshot_from_directory(self, path='.'):
        """
        Make a snapshot of the directory under `path` to send to the workers

        :param str path: path of repository to read from
        """

        assert self.tmp_working_dir is None

        dir_tasks = settings.local('data_pm.repo.tasks')
        self.tmp_working_dir = data_pm.file.TempDir(dir=dir_tasks)

        try:
            snapshots.snapshot_from_directory(path, self.tmp_working_dir.path)
        except Exception:
            self.tmp_working_dir.remove()
            raise

    def snapshot_from_branch(self, branch):
        """
        Take a snapshot from a git branch to send to the workers
        """

        assert self.tmp_working_dir is None

        dir_tasks = settings.local('data_pm.repo.tasks')
        self.tmp_working_dir = data_pm.file.TempDir(dir=dir_tasks, create=False)

        try:
            snapshots.snapshot_from_branch(branch, self.tmp_working_dir.path)
        except Exception:
            self.tmp_working_dir.remove()
            raise

    def upload(self, task_id):
        """
        Compress and send the code snapshot to the API url

        :param str task_id:
        :return: number of bytes uploaded
        """

        assert self.tmp_working_dir is not None

        tmp_compressed_code = data_pm.file.TempFile(suffix='.tar.gz')

        # Compress repository with gzip
        p = subprocess.Popen(['tar', '-czf', tmp_compressed_code.path, '.'],
                             cwd=self.tmp_working_dir.path, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            raise RuntimeError('Compression failed before upload:\n{}'.format(stderr))

        # Send code
        with open(tmp_compressed_code.path, 'rb') as fd:
            r = client.put_workspace(task_id, fd)

        if not r.ok:
            raise HTTPError(
                'Got error code {} when pushing workspace code: {}'.format(r.status_code, r.content)
            )

        archive_size = data_pm.file.getsize(tmp_compressed_code.path)
        tmp_compressed_code.remove()

        return archive_size

    def download(self, task_id):
        """
        Download the git repository from the API and extract it

        Use case: (pm submit - part 2)

        :param str task_id:
        :param bool delete: if True, delete code from server
        """

        assert self.tmp_working_dir is None and task_id

        tmp_compressed_code = data_pm.file.TempFile(suffix='-{}.tar.gz'.format(task_id))

        download_archive(task_id, tmp_compressed_code.path)

        # try:
        #     download_archive(task_id, tmp_compressed_code.path)
        # finally:
        #     if delete:
        #         client.delete_workspace(task_id)

        # Extract code in working dir
        dir_tasks = settings.local('data_pm.repo.tasks')
        self.tmp_working_dir = data_pm.file.TempDir(suffix='-' + task_id, dir=dir_tasks, create=True)

        p = subprocess.Popen(['tar', '-xzf', tmp_compressed_code.path, '-C', self.tmp_working_dir.path],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            raise RuntimeError('Extraction failed for task {}:\n{}'.format(task_id, stderr))

        tmp_compressed_code.remove()

    def install(self, task_id=None):
        """
        Install a virtual environment using the requirements.txt found in the working directory
        """
        requirements_path = data_pm.file.join(self.tmp_working_dir.path, 'requirements.txt')
        if not data_pm.file.exists(requirements_path):
            return

        self.python_env = PythonEnvironment.from_requirements(requirements_path)
        self.python_env.build(task_id)

    def clear(self, task_id_to_delete=None):
        if self.tmp_working_dir is not None:
            self.tmp_working_dir.remove()
            self.tmp_working_dir = None
        
        if task_id_to_delete:
            client.delete_workspace(task_id_to_delete)

    def __del__(self):
        self.clear()


def download_archive(task_id, dst):
    """
    Download an archive located at from API

    :param str task_id:
    :param str dst: write the archive here
    """

    r = client.get_workspace(task_id)

    if r.status_code != 200:
        raise Exception(
            'Error when downloading archive for {}\n'
            'status_code={}\n'
            'reason: {}\n'.format(task_id, r.status_code, r.reason)
        )

    # Write code in temp file
    with open(dst, 'wb') as fd:
        fd.write(r.content)
        fd.flush()
        os.fsync(fd.fileno())

    try:
        # Test if archive is valid
        subprocess.check_call(['tar', '-tzf', dst])
    except subprocess.CalledProcessError:
        raise Exception(
            'The received archive is corrupted, '
            'download failed for {}'.format(task_id)
        )


class PythonEnvironment(object):
    def __init__(self, requirements_hash, path, requirements_path):
        """
        Manage a python virtualenv to run code in isolated environment.

        Prefer .from_requirements() to instantiate.

        :param path: where to create the virtualenv
        :param requirements_path: path to a requirements.txt
        """
        self.requirements_hash = requirements_hash
        self.path = path
        self.requirements_path = requirements_path

    @staticmethod
    def from_requirements(requirements_path):
        """
        Instantiate a PythonEnvironment from a requirements.txt.
        The path to the virtualenv will be determined from the hash of the requirements.txt the base environments dir.

        :param requirements_path: path to a requirements.txt
        :rtype: PythonEnvironment
        """
        # Read requirements.txt
        with open(requirements_path, mode='r', encoding='utf-8') as fd:
            txt = fd.read()

        # Parse package list
        expr = re.compile("^[^\s#]+")
        l_packages = [match.strip() for line in txt.splitlines() for match in expr.findall(line)]
        bytes_packages = '\n'.join(sorted(l_packages)).encode('utf-8')

        # Compute hash from package list
        requirements_hash = hashlib.md5(bytes_packages).hexdigest()[:8]
        env_dir = data_pm.file.join(settings.expand('data_pm.environments.dir'), requirements_hash)

        return PythonEnvironment(requirements_hash, env_dir, requirements_path)

    @property
    def python_executable(self):
        return data_pm.file.join(self.path, 'bin', 'python')

    def build(self, task_id):
        """
        Build the virtual environment by installing all the necessary packages
        """
        while data_pm.file.exists(self.path + '.lock'):
            time.sleep(1)

        if data_pm.file.exists(self.path):
            log_message = 'Using "{}"\n'.format(self.path)
            if task_id is not None:
                client.post_stream_stdout(task_id, log_message)
                client.post_environment(socket.gethostname(), self.requirements_hash)
            else:
                print(log_message, end='')
            return

        # Lock environment directory
        data_pm.file.makedirs(self.path + '.lock')

        try:
            venv.EnvBuilder(with_pip=True).create(self.path)

            log_message = 'Installing python requirements at "{}"\n'.format(self.path)
            if task_id is not None:
                client.post_stream_stdout(task_id, log_message)
            else:
                print(log_message, end='')

            with open('/dev/null', mode='w') as null, open(self.requirements_path, mode='r') as fd:
                try:
                    # Try install numpy first if it is in the requirements.
                    # This allows to install some packages afterward which depend on
                    # numpy and having the exact version we need.
                    numpy_version = [line for line in fd.read().split('\n') if 'numpy' in line][0].strip()
                    self.check_call([os.path.join(self.path, 'bin', 'pip3'), 'install', numpy_version], stdin=null, stdout=null)
                except IndexError:
                    # Numpy is not a requirement
                    pass
                self.check_call([os.path.join(self.path, 'bin', 'pip3'), 'install', '-r', self.requirements_path], stdin=null, stdout=null)

            log_message = 'Requirements installed.\n'
            if task_id is not None:
                client.post_stream_stdout(task_id, log_message)
                client.post_environment(socket.gethostname(), self.requirements_hash)
            else:
                print(log_message, end='')
        except Exception as e:
            if data_pm.file.exists(self.path):
                data_pm.file.remove(self.path, recursive=True)

            print(f'Could not install environment: {e}', end='')
            raise
        finally:
            # Unlock environment directory
            data_pm.file.remove(self.path + '.lock')

    def get_variables(self):
        """
        Build a dictionary of environment variables to go work within the virtual environment

        :rtype: dict
        """
        env = {}

        paths = os.environ.get('PATH', '').split(os.pathsep)
        paths.insert(0, data_pm.file.join(self.path, 'bin'))  # <env>/bin

        env['PATH'] = os.pathsep.join(paths)

        return env

    def check_call(self, args, **kwargs):
        """
        Run a command within the python environment.

        Will raise if the subprocess has a return code different than zero.

        :param list[str] args: Command to run
        :param kwargs: Additional parameters to pass to subprocess.check_call
        """
        p = self.Popen(args, **kwargs)

        stdout, stderr = p.communicate()

        if p.returncode != 0:
            raise RuntimeError('Error running the command: "{}":\n{}'.format(args, stderr))

    def Popen(self, args, **kwargs):
        """
        Run a command within the python environment

        :param list[str] args: Command to run
        :param kwargs: Additional parameters to pass to subprocess.check_call
        :rtype: subprocess.Popen
        """
        env = self.get_variables()  # Load the virtualenv variables
        env.update(kwargs.get('env', {}))  # Overwrite virtualenv variables with the env argument
        kwargs['env'] = env  # (re)set the env argument for the subprocess call

        kwargs['stdout'] = subprocess.PIPE
        kwargs['stderr'] = subprocess.PIPE

        # Attempt to replace the first argument with a full path
        # /bin/bash will remain /bin/bash
        # pip will become <env>/bin/pip
        args[0] = find_executable(args[0], path=env['PATH'])  # Replace "pm" by the path to the virtual env script

        env['PYTHONPATH'] = os.environ.get('PYTHONPATH', '')

        return subprocess.Popen(args, **kwargs)

