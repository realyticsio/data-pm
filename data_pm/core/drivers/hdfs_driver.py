# Global imports
import re
import time
import hdfs
from hdfs import HdfsError
import posixpath
import contextlib

# Import urlparse
try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

# Local imports
from data_pm import settings
from . import base_driver

import sys
is_old_cluster = sys.version_info < (3, 7)

_default_driver = None


class Driver(base_driver.BaseDriver):
    name = 'hdfs'

    def __init__(self, _settings):
        """
        HDFS driver
        """
        super(self.__class__, self).__init__(_settings)

        self.url = self.settings['data_pm.driver.hdfs.url']
        self.user = self.settings['data_pm.driver.hdfs.user']

        parsed_url = urlparse(self.url)
        self.hostname = parsed_url.hostname
        self.port = parsed_url.port

        url = 'http://{}:{}'.format(self.hostname, self.port)
        self.client = hdfs.InsecureClient(url, self.user)

    def __eq__(self, other):
        # HDFS drivers are the same if the URL and user are the same
        return self.url == other.url and self.user == other.user

    def __hash__(self):
        return hash((self.url, self.user))

    @staticmethod
    def get_default():
        global _default_driver
        if _default_driver is None:
            _default_driver = Driver(settings)
        return _default_driver

    def TempFile(self, prefix='tmp', suffix='', dir=None, destination=None, create=False):
        return super(self.__class__, self).TempFile(
            prefix=prefix,
            suffix=suffix,
            dir=dir if dir is not None else self.settings['data_pm.driver.hdfs.tmpdir'],
            destination=destination,
            create=create
        )

    def TempDir(self, prefix='tmp', suffix='', dir=None, destination=None, create=False):
        return super(self.__class__, self).TempDir(
            prefix=prefix,
            suffix=suffix,
            dir=dir if dir is not None else self.settings['data_pm.driver.hdfs.tmpdir'],
            destination=destination,
            create=create
        )

    def _clean_path(self, url, allow_any_host=False):
        """
        Return the path part from an url

        Raises an error when the input path contains a hostname different than this client hostname

        :param str url:
        :param bool allow_any_host: When True, don't raise if host differs
        :return: str
        """
        hostname, port, path = self.parse(url)
        if not allow_any_host and hostname is not None and hostname != self.hostname:
            raise ValueError('Provided path "{}" does not point to this client hostname "{}"'
                             .format(url, self.hostname))
        return path

    @staticmethod
    def _retry(func, *args, **kwargs):
        nb_retry = 3
        for i in range(1, nb_retry + 1):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                if i == nb_retry:
                    raise e

        raise RuntimeError("Should never happen: retry function in an invalid state")

    def abspath(self, path):
        hostname, port, path = self.parse(path)
        ret = self.client.resolve(path)
        port = port if port is not None else '9000'
        return build_hdfs_path(self.hostname, port, ret)

    def basename(self, path):
        hostname, port, path = self.parse(path)
        ret = posixpath.basename(path)
        return ret

    def dirname(self, path):
        hostname, port, path = self.parse(path)
        ret = posixpath.dirname(path)
        return build_hdfs_path(hostname, port, ret)

    def download(self, path, local_path):
        """
        Download an object from HDFS to the local filesystem

        :param path: HDFS path
        :param local_path: local path
        """
        path = self._clean_path(path)
        self.client.download(path, local_path)

    def exists(self, path):
        path = self._clean_path(path)

        return self._retry(self.client.status, path, strict=False) is not None

    def getmtime(self, path):
        path = self._clean_path(path)

        status = self._retry(self.client.status, path, strict=False)

        if status is not None:
            mtime = status['modificationTime'] / 1000.
            return mtime
        else:
            return None

    def getsize(self, path):
        path = self._clean_path(path)
        return self.client.status(path, True)['length']

    def isdir(self, path):
        if not self.exists(path):
            return False
        path = self._clean_path(path)
        return self.client.status(path)['type'] == 'DIRECTORY'

    def isfile(self, path):
        if not self.exists(path):
            return False
        path = self._clean_path(path)
        return self.client.status(path)['type'] == 'FILE'

    def join(self, arg, *args):
        hostname, port, path = self.parse(arg)
        ret = posixpath.join(path, *args)
        return build_hdfs_path(hostname, port, ret)

    def listdir(self, path):
        path = self._clean_path(path)
        return self.client.list(path, status=False)

    def list_statuses(self, path, recursive=False):
        path = self._clean_path(path)

        if not self.exists(path):
            raise HdfsError('"{}" does not exist'.format(path))

        is_directory = self.isdir(path)
        children_num = len(self.listdir(path)) if is_directory else 0

        yield base_driver.FileStatus(
            absolute_path=path,
            relative_path='',
            is_directory=is_directory,
            mtime=self.getmtime(path),
            size=0 if is_directory else self.getsize(path),
            children_num=children_num
        )

        def _list_statuses_rec(_path, _recursive):
            dirs = []
            for name, info in self.client.list(_path, status=True):
                absolute_path = self.join(_path, name)
                _is_directory = info['type'] == u'DIRECTORY'

                yield base_driver.FileStatus(
                    absolute_path=absolute_path,
                    relative_path=name,
                    is_directory=_is_directory,
                    mtime=info['modificationTime']/1000.,
                    size=0 if _is_directory else info['length'],
                    children_num=info['childrenNum']
                )

                if _is_directory:
                    dirs.append(absolute_path)

            if _recursive:
                for absolute_path in dirs:
                    for _file_status in self.list_statuses(absolute_path, recursive=True):
                        assert _file_status.absolute_path.startswith(_path)
                        _file_status.relative_path = _file_status.absolute_path[len(_path):].strip('/')
                        yield _file_status

        if is_directory:
            for file_status in _list_statuses_rec(path, recursive):
                yield file_status

    def makedirs(self, path):
        path = self._clean_path(path)
        self._retry(self.client.makedirs, path)

    def parse(self, path):
        """
        Parse an HDFS path into a triplet (hostname, port, path)

        :param str path:
        :return: A triplet (hostname, port, path)
        :rtype: (str, str str)
        """
        if path.startswith('file://'):
            raise ValueError('Expected a hdfs path, not a file path: "{}"'.format(path))

        if path.startswith('hdfs://'):
            url = path
            suburl = url[len('hdfs://'):]
            if '/' not in suburl:
                raise ValueError('No path in this hdfs url: "{}"'.format(url))
            netloc, path = suburl.split('/', 1)
            path = '/' + path
            if ':' in netloc:
                hostname, port = netloc.split(':', 1)
            else:
                hostname = netloc
                port = '9000'

            if hostname != self.hostname:
                raise ValueError('Current HDFS driver ({}) is not configured for the path "{}"'.format(self.url, url))

            return hostname, port, path

        else:
            return None, None, path

    @staticmethod
    def parse_config(url):
        pattern = r'hdfs://(?P<hostname>.+):(?P<port>[0-9]+)/user/(?P<user>(\w|-)+)/?.*'
        m = re.search(pattern, url)
        if m is not None:
            d = m.groupdict()
            # HACK: hdfs3 is used with python 3.8 on the new cluster
            # whereas hdfs2 is used with python 3.6 on the old cluster.
            # We want the port to be fixed for hadoop 2 otherwise the
            # connection with hadoop can fail.
            return {
                'data_pm.driver.hdfs.url': build_hdfs_path(d['hostname'], '50070'),
                'data_pm.driver.hdfs.user': d['user']
            } if is_old_cluster else {
                'data_pm.driver.hdfs.url': build_hdfs_path(d['hostname'], d.get('port', '50070')),
                'data_pm.driver.hdfs.user': d['user']
            }

    @contextlib.contextmanager
    def read(self, path):
        path = self._clean_path(path)
        with self.client.read(path) as reader:
            yield reader

    def remove(self, path, recursive=False):
        path = self._clean_path(path)
        self.client.delete(path, recursive=recursive)

    def rename(self, src, target):
        src_path = self._clean_path(src)
        target_path = self._clean_path(target)
        self.client.rename(src_path, target_path)

    def renames(self, src, target):
        src_path = self._clean_path(src)
        target_path = self._clean_path(target)

        parent_dir = self.abspath(self.dirname(target_path))
        if not self.exists(parent_dir):
            self.makedirs(parent_dir)
        self.client.rename(src_path, target_path)

    def setmtime(self, path, mtime=None):
        path = self._clean_path(path)
        if mtime is None:
            mtime = time.time()

        mtime = int(mtime * 1000)

        self.client.set_times(path, None, mtime)

    def upload(self, local_path, path):
        """
        Upload an object from the local filesystem to HDFS

        :param local_path: local path
        :param path: HDFS path
        """
        path = self._clean_path(path)
        self.client.upload(path, local_path)

    def walk(self, path):
        path = self._clean_path(path)
        for prefix, dirnames, filenames in self.client.walk(path):
            yield prefix, dirnames, filenames

    @contextlib.contextmanager
    def write(self, path):
        path = self._clean_path(path)

        if self.exists(path):
            if self.isdir(path):
                raise ValueError('Can not write to "{}": is a directory'.format(path))
            self.remove(path)

        with self.client.write(path) as writer:
            yield writer


def build_hdfs_path(hostname, port, path=None):
    if not hostname or not port:
        return path

    # HACK: hdfs3 is used with python 3.8 on the new cluster
    # whereas hdfs2 is used with python 3.6 on the old cluster.
    # We do not want to specify the port in the url for hadoop 3
    # as it raises an error.

    url = f'hdfs://{hostname}'

    if port and is_old_cluster:
        url += f':{port}'

    if path:
        url += f"/{path.lstrip('/')}"

    return url


def dashboard_access(path):
    hostname, port, path = Driver.get_default().parse(path)
    return 'http://{}:{}/explorer.html#{}'.format(hostname, port, path)
