# Local imports
from data_pm.core.factory import base_factory
from data_pm.core.study_components.study_control import StudyControl
from data_pm.core.branch import BranchConfiguration
from data_pm.core import study_mngr, project_mngr


class ProdFactory(base_factory.BaseFactory):

    def create_study_control(self, study_name, project_name=None, configure_branch=False):
        study_ctrl = StudyControl.from_name(study_name)

        if not study_ctrl.is_shared:
            study_ctrl.set_project_name(project_name)

        if configure_branch:
            dag = self.create_plotting_dag(study_names=[study_name])
            datanode = dag.G.nodes[study_name]['datanode']
            study_ctrl.set_branches_in(datanode.branches_in)
            study_ctrl.set_branch_out(datanode.branch_out)

        return study_ctrl

    def create_branch_configuration(self):
        return BranchConfiguration.from_file()

    def create_study(self, study_ctrl):
        study = study_mngr.load_study(study_ctrl.name)

        if not study.is_shared:
            study.set_project_name(study_ctrl.project_name)

        study.get_inputs()
        study.set_branches_in(study_ctrl.branches_in)
        study.set_branch_out(study_ctrl.branch_out)

        return study

    def get_project_names(self):
        return project_mngr.get_project_names()
