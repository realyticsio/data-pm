"""
This module contains a singleton factory object.

Useful to switch the way objects are instanciated.
You may want to do so for unit tests.
"""


_factory = None


def get_factory():
    global _factory
    if _factory is None:
        from data_pm.core.factory import prod_factory
        _factory = prod_factory.ProdFactory()
    return _factory


def set_factory(new_factory):
    if new_factory is None:
        raise RuntimeError('Failed to set factory to null')

    from data_pm.core.factory import base_factory

    if not isinstance(new_factory, base_factory.BaseFactory):
        raise RuntimeError('Failed to set factory to non-factory object')

    global _factory
    _factory = new_factory
