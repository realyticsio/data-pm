"""
description: |-
  Counts the words of this project that appear in the most frequent trigram


inputs:
- alias: doc
  name: doc.txt
  parent: generate_data
- alias: counts
  name: counts.csv
  parent: shared_merge_ngrams

outputs:
- alias: counts
  name: counts.csv
  driver: file
  description: |-
      csv file containing the number of each word
      Example:
        word,count
        abd,5
        acd,2

parameters: []


"""

import collections
import pandas as pd

import data_pm


class Study(data_pm.BaseStudy):
    def run(self):
        # Load ngram counts
        s_ngram_counts = pd.Series.from_csv(self.i.counts, header=0)

        # Get the most frequent ngram
        ngram = s_ngram_counts.argmax()

        most_common_words = set(ngram.split('|'))  # For example: {'hoo', 'joh', 'hon'}

        # Parse input file
        with data_pm.file.read(self.i.doc) as reader:
            txt = reader.read()

        d_counts = collections.Counter()

        # Count common words
        for word in tokenize(txt):
            if word in most_common_words:
                d_counts[word] += 1

        # Write counts to output file
        pd.Series(d_counts).to_csv(self.o.counts)


def tokenize(txt):
    """
    Tokenize a text

    Use nltk in a real case

    :param str txt:
    :rtype: list[str]
    """
    words = txt.replace('\n', ' ').split(' ')

    return words
