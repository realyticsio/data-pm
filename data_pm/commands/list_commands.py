#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Return a list of available subcommands.

"""

# Global imports
from __future__ import print_function
import sys

# Local imports
from data_pm.tools.cli_handler import BaseCommand


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def handle(self, namespace):
        subcommands = sorted(self.parsers.choices.keys())
        print('\n'.join(subcommands))

