*****************
Launching studies
*****************

Two modes: remote mode and local mode. Local mode should be reserved for small tests requiring few data
to be downloaded. Remote mode ensures you that the environment is clean.


Remote mode
===========

**Commands**::

    # Launch studies
    pm launch my_project1 my_project2 my_studyA my_studyB

    # Debug studies
    pm debug my_project my_study

Multiple projects and multiple studies can be launched with the same command, meaning that all the provided studies
(and their dependencies) will be computed for all the provided projects.

**Submit process**

.. image:: _static/pm-submit.png

**Data streams**

.. image:: _static/pm-submit-streams.png


**Data-pm instances**

==============  ==================  =================================  ===============
Location        Application         Working directory                  Python packages
==============  ==================  =================================  ===============
Your machine    pm submit           Your workspace                     Your environment
data-pm server  apache2+mod_wsgi    Configured in apache2 site         Global packages
data-pm worker  celery worker       Configured by celery start script  Global packages
data-pm worker  pm launchforworker  <data_pm.environments.dir>/<hash>  venv
==============  ==================  =================================  ===============


Local mode
==========

Commands::

    # Launch studies
    pm launch my_project1 my_project2 my_studyA my_studyB --local

    # Debug studies
    pm debug my_project my_study --local


**Data streams**

.. image:: _static/pm-local-streams.png


**Data-pm instances**

==============  ==================  =================================  ===============
Location        Application         Working directory                  Python packages
==============  ==================  =================================  ===============
Your machine    pm submit           Your workspace                     Your environment
data-pm server  apache2+mod_wsgi    Configured in apache2 site         Global packages
==============  ==================  =================================  ===============

