#!/usr/bin/python
# -*- coding: utf-8 -*-

# Global imports
import time
from itertools import chain

# Local imports
from data_pm.core import drivers


class Redirector(object):
    def __init__(self, study, log_writer):
        """

        :param data_pm.core.study.BaseStudy study:
        :param data_pm.core.study_log.LogWriter log_writer:
        """
        self.log_writer = log_writer
        self.logger = log_writer.logger
        self.inputs = study.inputs
        self.outputs = study.outputs

    @property
    def redirect_in_activation(self):
        # Check if study should redirect content during activation
        driver_names = set([x.driver.name for x in chain(self.inputs.values(), self.outputs.values())])
        do_redirection = any([drivers.need_redirection(name) for name in driver_names])
        return do_redirection

    @property
    def redirect_in_deactivation(self):
        # Check if study should redirect content during deactivation
        driver_names = set([x.driver.name for x in self.outputs.values()])
        do_redirection = any([drivers.need_redirection(name) for name in driver_names])
        return do_redirection

    def activate(self):
        if self.redirect_in_activation:
            self.logger.info('Redirecting inputs')
        t = time.time()

        for i in self.inputs.values():
            i.activate()
            self.logger.debug('[Redirected] {}'.format(i.resource.path))
        for o in self.outputs.values():
            o.activate()
            self.logger.debug('[Redirected] {}'.format(o.resource.path))

        duration = int(time.time() - t)

        if self.redirect_in_activation:
            self.logger.info('Activation finished after {} seconds'.format(duration))
        self.log_writer.set_activation_time(duration)

    def deactivate(self):
        if self.redirect_in_deactivation:
            self.logger.info('Redirecting outputs')

        t = time.time()

        for i in self.inputs.values():
            i.deactivate()
            self.logger.debug('[Redirected] {}'.format(i.resource.path))
        for o in self.outputs.values():
            o.deactivate()
            self.logger.debug('[Redirected] {}'.format(o.resource.path))

        duration = int(time.time() - t)

        if self.redirect_in_deactivation:
            self.logger.info('Deactivation finished after {} seconds'.format(duration))
        self.log_writer.set_deactivation_time(duration)

    def clear(self):
        self.logger.debug('Cleaning redirected IO')

        for i in self.inputs.values():
            i.clear()
        for o in self.outputs.values():
            o.clear()
