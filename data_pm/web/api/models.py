# coding: utf-8

# Global imports
import os
from datetime import timedelta, datetime
from sqlalchemy import Column, String, DateTime, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import UniqueConstraint

# Local imports
import data_pm
from data_pm.core import paths
from data_pm.core.study_components import study_log
from data_pm import settings


# Base class to inherit from to map classes and tables
Base = declarative_base()


class Environment(Base):
    """
    Store information about last use of python environments
    """
    __tablename__ = 'environment'

    id = Column(Integer, primary_key=True)
    hostname = Column(String(128), nullable=False)
    hash_code = Column(String(128), nullable=False)
    datetime = Column(DateTime)

    unique_constraint = UniqueConstraint(hostname, hash_code, datetime)

    def __repr__(self):
        date_str = self.datetime.strftime('%Y-%m-%d %H:%M:%S') if self.datetime else str(None)
        return '<Environment hostname={}, hash_code={}, datetime={}>'.format(
            self.hostname, self.hash_code, date_str
        )


class LastUpdate(Base):
    """
    Store information about last outputs update for each study
    """
    __tablename__ = 'last_update'

    id = Column(Integer, primary_key=True)
    study = Column(String(128), nullable=False)
    branch = Column(String(128), nullable=False)
    project = Column(String(128))
    datetime = Column(DateTime)

    unique_constraint = UniqueConstraint(study, branch, project)

    def __repr__(self):
        date_str = self.datetime.strftime('%Y-%m-%d %H:%M:%S') if self.datetime else str(None)
        return '<LastUpdate branch={}, study={}, project={}, datetime={}>'.format(
            self.branch, self.study, self.project, date_str
        )


class StudyLog(Base):
    """
    Represents the meta-information we can have about the execution of a study

    The constructor automatically accepts keyword names that match the columns we've mapped

    """

    __tablename__ = 'study_logs'

    id = Column(Integer, primary_key=True)

    name = Column(String(128))
    branch = Column(String(128))
    project = Column(String(128), nullable=True)

    unique_constraint = UniqueConstraint(name, branch, project)

    start_date = Column(DateTime)
    end_date = Column(DateTime)

    status = Column(String(16))
    hostname = Column(String(32))
    origin = Column(String(32))
    activation_time = Column(Integer)
    deactivation_time = Column(Integer)

    _logs = None
    _log = None

    @hybrid_property
    def is_shared(self):
        return self.project == 'None'

    @hybrid_property
    def duration(self):
        if self.start_date is None:
            return
        elif self.end_date is None:
            now = datetime.utcnow()
            now_floor = now - timedelta(microseconds=now.microsecond)
            return now_floor - self.start_date
        else:
            return self.end_date - self.start_date

    @hybrid_property
    def iduration(self):
        duration = self.duration
        if duration is not None:
            return duration.total_seconds()

    @hybrid_property
    def redirection_time(self):
        iredirection_time = self.iredirection_time
        if iredirection_time is not None:
            return timedelta(seconds=iredirection_time)

    @hybrid_property
    def iredirection_time(self):
        if self.activation_time is None and self.deactivation_time is None:
            return None
        elif self.activation_time is None:
            return self.deactivation_time
        elif self.deactivation_time is None:
            return self.activation_time
        else:
            return self.activation_time + self.deactivation_time

    def __repr__(self):
        return '<StudyLog(name={}, branch={}, project={}, is_shared={}, status={},\n' \
               '          hostname={}, origin={}, start_date={}, end_date={})>'\
            .format(self.name, self.branch, self.project, self.is_shared, self.status,
                    self.hostname, self.origin, self.start_date, self.end_date)

    @property
    def log_dir(self):
        """Path to the log directory"""
        return paths.path_to_logs(
            driver=data_pm.file,
            branch_name=self.branch,
            study_name=self.name,
            project_name=self.project
        )

    @property
    def log_path(self):
        """Path to the current log file"""
        return os.path.join(self.log_dir, settings['data_pm.log.filename'])

    @property
    def status_path(self):
        """Path to the current log file"""
        return os.path.join(self.log_dir, settings['data_pm.status.filename'])

    @property
    def logs(self):
        if self._logs is None:
            self._logs = study_log.load_logs(self.log_dir)

        return self._logs

    @property
    def last_log(self):
        """
        StudyLogReader
        """
        if self._log is None:
            if not os.path.exists(self.log_path):
                return None
            else:
                self._log = study_log.StudyLogReader(self.log_path)
        return self._log
