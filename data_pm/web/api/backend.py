# coding: utf-8

# Global imports
from datetime import datetime
import datetime as dt
from sqlalchemy.sql import func
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.orm.exc import NoResultFound

# Local imports
from data_pm import settings
from data_pm.core import study_mngr
from data_pm.web.api import models
from data_pm.web.api.models import StudyLog, LastUpdate, Environment


# Actually try to connect to the database the first time it is asked to perform a task
engine = create_engine('sqlite:///' + settings.local('data_pm.sqlite.database'),
                       connect_args={'timeout': 30})
session = scoped_session(sessionmaker(bind=engine, autocommit=False, autoflush=False))
""" This is not exactly a Session, this is a proxy.
    :type: sqlalchemy.orm.Session
"""

# Create tables if not exist
models.Base.metadata.create_all(engine)


def update_study_log(branch, study, project, column, value):
    """
    :param str branch:
    :param str study:
    :param str project: None in case of shared study
    :param str column: Column to update (example 'status')
    :param str value: new value (example 'succes')
    """

    obj = session.query(StudyLog)\
        .filter_by(name=study, branch=branch, project=project)\
        .first()

    if column == 'start_date' and obj is not None:
        # The study is starting from begining, purge old metadata if it exists
        session.delete(obj)
        session.commit()
        obj = None

    if obj is None:
        # Create a new entry
        obj = StudyLog(name=study, branch=branch, project=project)
        session.add(obj)

    # Update column
    setattr(obj, column, value)
    session.commit()


def get_study_logs(branch_name=None, study_name=None, project_name=None,
                   status=None, shared_only=False, project_only=False, today=False):
    """
    Query study statuses in database

    :param branch_name:
    :param study_name:
    :param project_name:
    :param status: can be a list or string
    :param shared_only:
    :param project_only:
    :param today: if True, returns studies started at current day
    :return:
    """
    assert not shared_only or not project_only
    assert not shared_only or not project_name

    rows = session.query(StudyLog)

    if branch_name:
        rows = rows.filter_by(branch=branch_name)

    if study_name:
        rows = rows.filter_by(name=study_name)

    if shared_only:
        rows = rows.filter_by(project=None)

    if project_name:
        rows = rows.filter_by(project=project_name)
    elif project_only:
        rows = rows.filter(StudyLog.project.isnot(None))

    if isinstance(status, list):
        rows = rows.filter(StudyLog.status.in_(status))
    elif status is not None:
        rows = rows.filter_by(status=status)

    if today:
        current_date = datetime.now().date()
        rows = rows.filter(func.date(StudyLog.start_date) == current_date)

    return sorted(rows, key=lambda s: (s.branch, s.name))


def get_filtered_study_logs(branch_name=None, study_name=None, project_name=None,
                            status=None, shared_only=False, project_only=False, today=False):
    """Read logs from database and filter non-existing studies"""

    study_names = study_mngr.get_all_study_names()

    rows = get_study_logs(
        branch_name=branch_name,
        study_name=study_name,
        project_name=project_name,
        status=status,
        shared_only=shared_only,
        project_only=project_only,
        today=today
    )

    filtered_row = [r for r in rows if r.name in study_names]

    return filtered_row


def update_last_update(branch, study, project, timestamp):
    """
    :param str branch:
    :param str study:
    :param str project: None in case of shared study
    :param datatime timestamp:
    """

    try:
        obj = session.query(LastUpdate)\
            .filter_by(study=study, branch=branch, project=project)\
            .one()
        obj.datetime = timestamp

    except NoResultFound:
        if timestamp:
            new_obj = LastUpdate(
                branch=branch,
                study=study,
                project=project,
                datetime=timestamp
            )
            session.add(new_obj)

    session.commit()


def get_last_updates(d_query):
    """
    :param dict[str, Any] d_query:

    d_query is like:
    {
        'master': {
            'study_a': {
                'project_1': None,
                'project_2': None
            },
            'shared_study_b: None
        },
        ...
    }

    This function fills in the last_updates values if present in database.

    The generated query for the example bellow is :

    LastUpdate.branch == 'master'
        AND LastUpdate.study == 'study_a'
        AND LastUpdate.project.in_(['project_1', 'project_2'])
    OR
    LastUpdate.branch == 'master'
        AND LastUpdate.study == 'shared_study_b'
        AND LastUpdate.project == None
    OR
    ...

    """
    from sqlalchemy import or_, and_

    assert len(d_query) > 0

    query_filter = None

    for branch_name, d_studies in d_query.items():
        for study_name, d_projects in d_studies.items():

            if isinstance(d_projects, dict):
                sub_filter = and_(
                    LastUpdate.branch == branch_name,
                    LastUpdate.study == study_name,
                    LastUpdate.project.in_(d_projects.keys())
                )
            else:
                sub_filter = and_(
                    LastUpdate.branch == branch_name,
                    LastUpdate.study == study_name,
                    LastUpdate.project.is_(None)
                )

            if query_filter is None:
                query_filter = sub_filter
            else:
                query_filter = or_(query_filter, sub_filter)

    last_updates = session.query(LastUpdate).filter(query_filter).all()

    for lu in last_updates:
        if lu.datetime is None:
            continue

        date_str = lu.datetime.strftime('%Y-%m-%d %H:%M:%S')

        try:
            d_query[lu.branch][lu.study][lu.project] = date_str
        except TypeError:
            # shared study
            d_query[lu.branch][lu.study] = date_str

    return d_query


def update_environment(hostname, hash_code):
    """
    Update the field 'datetime' of the environment.

    Create the entry if it does not exists.
    """

    try:
        obj = session.query(Environment)\
            .filter_by(hostname=hostname, hash_code=hash_code)\
            .one()
        obj.datetime = datetime.utcnow()
    except NoResultFound:
        obj = Environment(
            hostname=hostname,
            hash_code=hash_code,
            datetime=datetime.utcnow()
        )
        session.add(obj)

    session.commit()


def get_environments():
    return session.query(Environment).all()
