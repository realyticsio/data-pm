#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Return a list of studies.

"""

# Global imports
from __future__ import print_function

import sys

# Local imports
from data_pm.core import study_mngr
from data_pm.tools.cli_handler import BaseCommand


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def handle(self, namespace):
        study_names = study_mngr.get_all_study_names()
        print('\n'.join(sorted(study_names)))
