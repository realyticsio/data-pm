.. data-pm documentation master file, created by
   sphinx-quickstart on Fri Aug 14 14:04:24 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==============================
Data Project Manager (data-pm)
==============================

Introduction
************
Data-pm helps organizing data pipelines and running code in distributed, controlled environments.

It consists in a framework to organize and process data,
and a web based dashboard to watch the status of the processes.
The data stream process is divided into studies,
that are atomic pieces of work that get input data, and produce output data.
A study's input data is usually another study's output data, thus forming a dependency tree.

Multiple projects can be handled in the same work space:
each project follows the dependency tree determined by the studies, but the data does not mix.


Get started with the :doc:`quick start guide<quick_start>`.


Content
*******
.. toctree::
   :maxdepth: 2

   license
   quick_start
   installation
   tutorial
   advanced_usage
   commands
   settings
   launch_study


API Documentation
*****************
.. toctree::
   :maxdepth: 2

   study_api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

