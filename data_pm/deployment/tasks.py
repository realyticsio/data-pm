#!/usr/bin/python
# -*- coding: utf-8 -*-

# Global imports
from __future__ import print_function
import sys
import signal
import socket
import subprocess
from celery import Celery
from requests.exceptions import ConnectionError

# Local imports
from data_pm import settings, client
from data_pm.deployment.environments import WorkingDirHelper


ONE_WEEK = 3600 * 24 * 7

app = Celery(
    main='analysis_tool',
    broker=settings['celery.broker.url'],
    backend=settings['celery.backend.url'],
)

app.conf.task_acks_late = True
app.conf.task_default_queue = 'default'
# When enabled, allows to restart the task if the worker was lost unexpectedly.
app.conf.task_reject_on_worker_lost = True
app.conf.accept_content = ['json', 'pickle']
app.conf.broker_transport_options = {'visibility_timeout': ONE_WEEK}


def launch_studies_async(d_namespace, queue):
    job = launch_studies.si(d_namespace)
    async_result = job.apply_async(queue=queue, serializer='pickle')

    def terminate(signal, frame):
        revoke(async_result.id)
        print('SIGTERM received, celery task revoked')
        sys.exit(0)

    signal.signal(signal.SIGTERM, terminate)

    return async_result


def revoke(task_id):
    app.control.revoke(task_id, terminate=True)
    try:
        client.delete_workspace(task_id)
        client.delete_debug(task_id)
        clear_streams(task_id)
    except ConnectionError:
        pass


def clear_streams(task_id):
    try:
        client.delete_stream_stdout(task_id)
        client.delete_stream_stderr(task_id)
    except ConnectionError:
        pass


class SubmitError(Exception):
    def __init__(self, message, task_id):
        super(SubmitError, self).__init__(message)
        self.task_id = task_id

    def __str__(self):
        return '[{}] {}'.format(self.task_id[:8], self.args[0])


@app.task(bind=True)
def launch_studies(self, d_namespace):
    """Callback launched by data-pm workers"""

    task_id = self.request.id
    command = build_command(d_namespace, task_id)

    message = 'Task received by "{}"\n'.format(socket.gethostname())
    client.post_stream_stdout(task_id, message)

    wd = WorkingDirHelper()

    try:
        wd.download(task_id)
        wd.install(task_id)
        launcher = LaunchHelper(wd, command, task_id)
        launcher.run()
    finally:
        wd.clear()


class LaunchHelper(object):
    def __init__(self, working_dir, command, task_id):
        """
        :param WorkingDirHelper working_dir:
        :param list[str] command:
        """
        self.sigterm_lock = False
        self.task_id = task_id
        self.process = None
        self.command = command
        self.working_dir = working_dir

        signal.signal(signal.SIGTERM, self._sigterm_handler)

    def run(self):
        if self.working_dir.python_env:
            Popen = self.working_dir.python_env.Popen
        else:
            # FixMe: remove this part when python environments are mainstream
            Popen = subprocess.Popen

        self.process = Popen(
            args=self.command,
            cwd=self.working_dir.path,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout, stderr = self.process.communicate()

        if self.process.returncode != 0:
            raise Exception(stderr)

    def _sigterm_handler(self, signal, frame):
        if self.sigterm_lock:
            return

        self.sigterm_lock = True

        if self.process is not None:
            self.process.terminate()
            self.process.wait()

        # We want clear the local directory but not the global workspace to ensure that we can relaunch the task.
        self.working_dir.clear()
        sys.exit(1)


def build_command(d_namespace, task_id):
    d = d_namespace

    debug_mode = d.get('debug', False)

    if debug_mode:
        command = ['pm', 'debug'] + d['projects'] + d['studies']
        command += ['--task-id={}'.format(task_id)]
        if d.get('params'):
            command += ['--params'] + d['params']
        if d.get('origin', '<unkown>'):
            command += ['--origin={}'.format(d['origin'])]
    else:
        command = ['pm', 'launchforworker']
        command += ['--studies'] + d['studies']
        command += ['--branch={}'.format(d['branch'])]
        command += ['--task-id={}'.format(task_id)]
        if d.get('projects'):
            command += ['--projects'] + d['projects']
        if d.get('origin', '<unkown>'):
            command += ['--origin={}'.format(d['origin'])]
        if d.get('params'):
            command += ['--params'] + d['params']
        if d.get('min_depth_force') is not None:
            command += ['--min-depth-force={}'.format(d['min_depth_force'])]
        if d.get('max_depth_check') is not None:
            command += ['--max-depth-check={}'.format(d['max_depth_check'])]
        if d.get('ignore_parent', False):
            command += ['--ignore-parent']
        if d.get('unlock', False):
            command += ['--unlock']
        if d.get('slack'):
            command += ['--slack={}'.format(d['slack'])]
        if d.get('branches_in'):
            branches_in = d['branches_in'].replace('"', '\\"')
            command += ['--branches-in="{}"'.format(branches_in)]

    return command
