import sys
import re
import logging
import importlib
import pkgutil

from .configuration.settings import settings
from .core.drivers import DriverShortcut

try:
    from .core.client import client
    from .core.study_components.study import BaseStudy
    from .deployment.environments import PythonEnvironment
except ImportError:
    pass

logger = logging.getLogger('data-pm')

file = DriverShortcut('file')
""":type: data_pm.core.drivers.file_driver.Driver"""

hdfs = DriverShortcut('hdfs')
""":type: data_pm.core.drivers.hdfs_driver.Driver"""

s3 = DriverShortcut('s3')
""":type: data_pm.core.drivers.s3_driver.Driver"""

try:
    m = importlib.import_module('drivers')

    for importer, name, is_package in pkgutil.iter_modules(m.__path__):
        if is_package:
            continue

        match = re.search('(?P<driver_name>\w+)_driver', name)
        if match is not None:
            driver_name = match.groupdict().get('driver_name')
            setattr(sys.modules[__name__], driver_name, DriverShortcut(driver_name))

except ImportError:
    pass


# Retro-compatibility
class Bateau(object):
    def __init__(self, *args, **kwargs):
        pass


Input = Bateau
Output = Bateau
Parameter = Bateau
