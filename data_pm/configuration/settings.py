"""
Global configuration that may be overridden by user
"""

# Global imports
from __future__ import print_function
import os
import yaml
import copy
import sys
import zipfile


DEFAULT_CONF = {
    'data_pm.drivers': ['file'],

    'data_pm.log.level': 'INFO',
    'data_pm.log.format': '%(asctime)-8s | %(module)-15s | %(levelname)-8s | %(message)s',
    'data_pm.log.date.format': '%Y-%m-%d %H:%M:%S',
    'data_pm.log.filename': 'run.log',
    'data_pm.logs.dir': '~/.local/data-pm/logs',
    'data_pm.views.dir': '~/.local/data-pm/logs',
    'data_pm.status.filename': 'status.log',

    # **Relative** paths
    'data_pm.studies.dir': os.path.join('DEV', 'STUDIES'),
    'data_pm.shared_studies.dir': os.path.join('DEV', 'SHARED_STUDIES'),
    'data_pm.projects.dir': 'PROJECTS',
    'data_pm.shared.dir': 'SHARED',

    'data_pm.parameters.path': './parameters.yml',
    'data_pm.branches.path': './branches.yml',
    'data_pm.local.path': '~/.local/data-pm',

    'data_pm.driver.file.workspace': '~/.local/data-pm/data',
    'data_pm.driver.file.tmpdir': '/tmp',
    'data_pm.driver.hdfs.tmpdir': '/tmp',
    'data_pm.driver.s3.tmpdir': '/tmp',

    # Directories and files to package when uploading the code to workers
    'data_pm.package.list': ['DEV', 'settings', 'parameters.yml', 'branches.yml', 'requirements.txt'],

    # Distributed
    'data_pm.redirection.driver': 'file',
    'data_pm.api.url': 'http://localhost:80',
    'data_pm.sqlite.database': 'sqlite.db',
    'data_pm.repo.api': 'repo-api',
    'data_pm.repo.tasks': 'repo-tasks',

    # Stores python virtual environments
    'data_pm.environments.dir': '~/.local/data-pm/environments',

    # Celery
    'celery.backend.url': 'redis://localhost:6379',
    'celery.broker.url': 'redis://localhost:6379',
    'celery.queue.names': ['default'],
}
"""Default settings configuration"""


def _load(path, absent_ok=True):
    # Try load file
    conf = _load_from_file(path, absent_ok)

    # Try load file in a top level zip
    dir = path.split(os.path.sep)[0]
    if os.path.exists(dir + '.zip'):
        conf.update(_load_from_zip(dir + '.zip', path))

    return conf


def _load_from_file(path, absent_ok=True):
    """
    Load from a file

    :param path: path to the file
    :rtype: dict
    """
    if not os.path.exists(path):
        if not absent_ok:
            print('[Warning] Could not find setting file {}'.format(path), file=sys.stderr)
        return {}

    with open(path, mode='r', encoding='utf-8') as fd:
        conf = _load_from_fd(fd, path)

    return conf


def _load_from_zip(zip_path, relative_path, absent_ok=True):
    """
    Load from a .zip file

    :param zip_path: path to .zip file
    :param relative_path: relative path inside the package
    :rtype: dict
    """

    with zipfile.ZipFile(zip_path) as zf:
        try:
            zf.getinfo(relative_path)
        except KeyError:
            if not absent_ok:
                print('[Warning] Could not find setting file {}'.format(relative_path), file=sys.stderr)
            return {}

        with zf.open(relative_path, 'r') as fd:
            conf = _load_from_fd(fd, relative_path)

    return conf


def _load_from_fd(fd, path):
    """
    Load from a file descriptor

    :param file fd:
    :param str path:
    :rtype: dict
    """
    try:
        conf = yaml.load(fd, Loader=yaml.Loader)
    except IOError as e:
        raise RuntimeError('Could not load "{}": {}'.format(path, e.args))

    if conf is None:  # If the file is empty
        conf = {}

    # Update conf with the imports specified in the current file
    for import_path in conf.get('imports', []):
        conf.update(_load(import_path))

        # Deprecated: Load relatively to the current file directory
        conf.update(_load(os.path.join(os.path.dirname(path), import_path)))

    return conf


class Settings(object):
    def __init__(self):
        self.loaded = False
        self.settings = {}

    def __getitem__(self, item):
        if not self.loaded:
            settings.load()
        return self.settings[item]

    # Useful to set data_pm.running_mode
    def __setitem__(self, key, value):
        if not self.loaded:
            settings.load()
        self.settings[key] = value

    def __contains__(self, item):
        return item in self.settings

    def items(self):
        for k, v in self.settings.items():
            yield k, v

    def get(self, item, default=None):
        if not self.loaded:
            settings.load()
        return self.settings[item] if item in self.settings else default

    def expand(self, item, default=None):
        value = self.get(item)
        if not value:
            return default
        return os.path.expanduser(value)

    def local(self, item, default=None):
        value = self.get(item)
        if not value:
            return default
        elif item == 'data_pm.local.path':
            return os.path.expanduser(value)
        # value = value.replace('~', '~{}'.format(user))
        basedir = self.get('data_pm.local.path')
        return os.path.expanduser(os.path.join(basedir, value))

    def load(self):
        # Copy the default configuration
        self.settings = copy.deepcopy(DEFAULT_CONF)

        # Update conf with host settings
        data_pm_path = os.path.expanduser(self.settings['data_pm.local.path'])
        user_path = os.path.join(data_pm_path, 'settings.yml')
        self.settings.update(_load_from_file(user_path))

        # Update conf with datalab settings
        local_path = os.path.join('settings', 'settings.yml')
        self.settings.update(_load(local_path))

        self.loaded = True

        return self.settings


settings = Settings()

