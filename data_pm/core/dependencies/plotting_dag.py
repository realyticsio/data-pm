# Local imports
from data_pm.core.dependencies import base_dag


class PlottingDAG(base_dag.BaseDAG):
    """Graph for plotting, a node is like <study_name>"""

    @staticmethod
    def create(cli_studies):
        dag = PlottingDAG(cli_studies)

        for study in cli_studies:
            dag.populate(study)

        dag.spread_branches()

        return dag

    def populate(self, study):
        n_current = study.name

        datanode = base_dag.DataNode(n_current, study.is_shared)
        self.G.add_node(n_current, datanode=datanode)

        for i in study.inputs.values():
            n_parent = i.parent_study.name

            if n_parent not in self.G.nodes():
                # Explore node only if it has never been seen
                self.populate(i.parent_study)

            self.G.add_edge(n_parent, n_current)
