class Shortcut(object):
    def __init__(self, d, attr):
        self._d = d
        self._attr = attr

    def __getattr__(self, item):
        return getattr(self._d[item], self._attr)

    def __getitem__(self, item):
        return getattr(self._d[item], self._attr)

    def __dir__(self):
        return self._d.keys()

    def __str__(self):
        return '<Shortcut>' + str(self._d)

