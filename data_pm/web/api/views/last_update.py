# Global imports
import flask
import flask_restplus
from flask_restplus import reqparse

# Local imports
from data_pm.web.api import backend


ns = flask_restplus.Namespace('last_update')


parser = reqparse.RequestParser()
parser.add_argument('branch', type=str, required=True)
parser.add_argument('study', type=str, required=True)
parser.add_argument('project', type=str)


@ns.route('/nullify')
class Index(flask_restplus.Resource):
    def post(self):
        """
        Set the last update time to None.
        """

        args = parser.parse_args(strict=True)

        branch = args['branch']
        study = args['study']
        project = args['project']

        backend.update_last_update(branch, study, project, None)


@ns.route('/query')
class Index(flask_restplus.Resource):
    def post(self):
        """
        Query the last update time for a set of studies.
        """

        args = flask.request.json

        if args is None:
            return 500, 'Request content is empty'
        elif not isinstance(args, dict):
            return 500, 'Request content is malformed, it should be a dictionary'

        return backend.get_last_updates(args)
