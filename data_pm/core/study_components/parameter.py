# Global imports
from __future__ import print_function

import yaml
import os
import warnings
from builtins import str as new_str

# Local imports
from data_pm import settings
from data_pm.core.study_components import study_configuration

try:
    text_dtypes = (unicode, str, new_str)
except NameError:
    text_dtypes = (str, new_str)


class Parameter(object):
    def __init__(self, name, alias=None, default=None, dtype=None, description=None):
        """
        Define a parameter to modify the behaviour of a study.

        Note: this class has no reason to be instantiated elsewhere than in a class inheriting from a BaseStudy.

        :param str name: Parameter name
        :param str alias: Parameter alias
        :param Any default: Default value
        :param str dtype: Type of the value (str, int, float, bool)
            For bool, the strings 'y', 'yes', 't', 'true', '1' will return True
            While the strings 'n', 'no', 'f', 'false', '0' will return False
        """
        self.default = default
        self.description = description
        self.name = name
        self.alias = alias if alias is not None else name

        if dtype == 'str':
            self.type = str
        elif dtype == 'int':
            self.type = int
        elif dtype == 'float':
            self.type = float
        elif dtype == 'bool':
            self.type = bool
        elif dtype is None and self.default is not None:
            self.type = type(self.default)
        elif dtype is None:
            self.type = str
        else:
            self.type = dtype

        if self.type not in text_dtypes + (int, float, bool):
            warnings.warn('Parameter "{}" has a deprecated type "{}". Accepted types are bool, float, int, str.'
                          .format(self.name, dtype), category=UserWarning)
        else:
            self.default = parse_value(self.default, self.type)

        self._value = None
        self._value_is_loaded = False

        self.study_control = None
        """:type: data_pm.core.study_control.StudyControl"""

    @property
    def value(self):
        """
        Return the value, loading it if necessary

        :return: value
        """
        if not self._value_is_loaded:
            self.load_value()
        return self._value

    @study_configuration.require_project_set
    def load_value(self):
        val = load_parameter(self.name, self.study_control.name, self.study_control.project_name)
        self.set_value(val)

    def set_value(self, val):
        """
        Set value for this instance (will not persist)
        """
        if val is None:
            # Parameter could not be loaded, returning default value
            self._value = self.default
        else:
            # Cast and return parameter value
            try:
                self._value = parse_value(val, self.type)
            except ValueError as e:
                raise e.__class__('Unable to cast parameter {} as {}: {}'.format(self.name, self.type, e.args))
        self._value_is_loaded = True

    def configuration_changed(self):
        """
        Break cache (reset the val)
        """
        self._value = None
        self._value_is_loaded = False


def parse_value(val, dtype):
    """

    :param str val:
    :param type dtype:
    :return:
    """

    if dtype in text_dtypes:
        if not isinstance(val, new_str) and isinstance(val, str):  # Python2 str, attempt to decode it
            return val.decode('utf-8')
        elif isinstance(val, str):  # Python3 str
            return val
        else:
            return new_str(val)

    elif dtype == int:
        return int(val)

    elif dtype == float:
        return float(val)

    elif dtype == bool:
        if str(val).lower() in {'y', 'yes', 't', 'true', '1'}:
            return True
        elif str(val).lower() in {'n', 'no', 'f', 'false', '0'}:
            return False
        else:
            raise ValueError('Can not parse boolean value out of "{}"'.format(val))
    else:
        raise RuntimeError('Unknown type "{}"'.format(dtype))


def load_parameter(param_name, study_name, project_name=None):
    if os.path.exists(settings['data_pm.parameters.path']):
        with open(settings['data_pm.parameters.path'], 'r') as fp:
            doc = yaml.load(fp, Loader=yaml.Loader)

        if doc is None:
            doc = {}

        try:
            if project_name is None:
                # Probably a shared study
                return doc[study_name][param_name]
            else:
                # Not a shared study
                return doc[project_name][study_name][param_name]
        except KeyError:
            return None
    else:
        print('Parameters file not found')
        return None
