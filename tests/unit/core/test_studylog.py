from unittest import TestCase
import logging
import time

from data_pm.core.study_components import study_log
from data_pm import settings


class StudyLogTestCase(TestCase):
    def setUp(self):
        settings.load()

    def test_WriteRead(self):
        logger = logging.getLogger('my_logger')

        logger.setLevel(logging.getLevelName(settings['data_pm.log.level']))

        writer = study_log.StudyLogWriter(logger).open(branch_name='master',
                                                       project_name=None, study_name='unittest.testing')
        writer.set_start()
        writer.set_status('running')

        logger.info('a simple test')
        time.sleep(2)

        writer.set_end()
        writer.set_status('finished')
        writer.close()

        reader = study_log.StudyLogReader(tmp_file.name)
        assert 0 < reader.duration < 3
        assert any(['a simple test' in line for line in reader.lines])
        assert reader.status == 'finished'


