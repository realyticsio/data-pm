#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Launch studies for a set of clients
"""
# Global imports
from __future__ import print_function
import os
import sys
import git
import getpass

# Local imports
from data_pm.core import project_mngr
from data_pm.core.analysis_tool import AnalysisTool
from data_pm.deployment.environments import WorkingDirHelper
from data_pm.tools import git_helper
from data_pm.tools.cli_handler import BaseCommand, refactor


__maintainer__ = "Nicolas Richard"


def resolve_origin(input_origin):
    if input_origin is not None:
        origin = input_origin
    else:
        try:
            repo = git.Repo('.')
            origin = git_helper.get_value(repo, 'name') or getpass.getuser()
        except git.InvalidGitRepositoryError:
            origin = getpass.getuser()

    return origin


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('projects_or_studies', nargs='+', metavar='PROJECT OR STUDY')
        parser.add_argument('--branch', dest='branch_name', type=str, default='current')

        parser.add_argument('-p', '--params', dest='params', nargs='*', type=str,
                            help='Parameters in the form myparam=myvalue')

        parser.add_argument('--min-depth-force', type=int, default=0,
                            help='Force compute studies deeper than this value. '
                                 'By default, only force compute the provided studies.')
        parser.add_argument('--max-depth-check', type=int, default=1000,
                            help='Do not check the status of studies deeper than this value. '
                                 'By default, check as deep as possible (1000).')
        parser.add_argument('--ignore-parent', action='store_true',
                            help='Do not check the status of studies of parent branch.')

        parser.add_argument('--unlock', action='store_true',
                            help='Do not check if study is already running')

        parser.add_argument('--slack', dest='slack', type=str, default=None,
                            help='Send notifications to the given slack channel')
        parser.add_argument('--origin', type=str, default=None,
                            help='User or host that issues the task')

    def handle(self, namespace):
        repo = git_helper.get_repo('.')
        active_branch = repo.active_branch.name

        # Load projects before changing cwd
        all_projects = set(project_mngr.get_project_names())

        init_path = os.getcwd()
        working_dir = WorkingDirHelper()

        if namespace.branch_name == 'current':
            namespace.branch = active_branch
            working_dir.snapshot_from_directory('.')
        else:
            working_dir.snapshot_from_branch(namespace.branch)

        # Get user's name which issues this task
        namespace.origin = resolve_origin(namespace.origin)

        try:
            os.chdir(working_dir.path)
            refactor(namespace, all_projects)
            print('\033[93m'
                  '@@WARNING@@\n'
                  'You are running data-pm in local, redirection can take time.\n'
                  'Code is running with your python packages, not requirements.txt.\n'
                  '@@WARNING@@'
                  '\033[0m')
            print('BRANCH=\033[41m\033[1m\033[7m{}\033[0m\n'.format(namespace.branch))
            AnalysisTool(namespace).launch_studies()
        finally:
            os.chdir(init_path)
            working_dir.clear()
