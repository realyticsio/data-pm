============
Installation
============

Single node
***********

In single node, you will be able to launch studies from your machine, that will run on your machine.

::

    # Install data-pm
    sudo -E pip install git+https://anon@bitbucket.org/realyticsio/data-pm.git

    # Install redis
    sudo apt install redis-server


Cluster
*******

Data-PM can be tested on a single machine, but building a whole cluster is far more helpful.
We advise to setup one machine to serve as the data-pm server, and multiple other machines to serve as data-pm workers.


data-pm server
""""""""""""""
The API and the dashboard are flask applications, we advise to deploy them with Apache2 (mod_wsgi).
Communication with workers is managed with Celery, we advise to use Redis as both the broker and result backend.


data-pm worker
""""""""""""""
Celery workers can be launched with a single command, but we advise to use systemd service scripts.

