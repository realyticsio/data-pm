# Global imports
import unittest
import collections

# Local imports
import data_pm
from data_pm.core.drivers.base_driver import BaseDriver
from data_pm.tools import sync


class TestFiletoHdfs(unittest.TestCase):

    # def __init__(self, src_driver=data_pm.file, dst_driver=data_pm.hdfs):
    #     super(TestSync, self).__init__(methodName='runTest')
    #     self.src_driver = src_driver
    #     self.dst_driver = dst_driver

    def setUp(self):
        self.src_driver = data_pm.file
        self.dst_driver = data_pm.hdfs

        self.src_tmp = self.src_driver.TempDir(prefix='unittest-')
        self.src = self.src_tmp.path
        self.dst_tmp = self.dst_driver.TempDir(prefix='unittest-')
        # self.dst_tmp = self.dst_driver.TempDir(prefix='unittest-', destination='s3://realytics-tmp/tmp')
        self.dst = self.dst_tmp.path

    # def test_full(self):
    #     # init
    #     create_tree_structure(self.src, self.src_driver, {
    #         'dir1/subdir_a/file_alpha': 'alpha_content',
    #         'dir1/subdir_a/file_beta': 'beta_content',
    #         'dir1/subdir_b/file_mu': 'mu_content',
    #         'dir2/file_zeta': 'zeta_content,' * 100,
    #     })
    #
    #     # for command `distcp`
    #     # data_pm.hdfs.makedirs(self.dst)
    #
    #     # sync
    #     import subprocess
    #     p = subprocess.Popen(['/opt/hadoop/bin/hadoop', 'distcp', '-update', self.src + '/', self.dst + '/'])
    #     stdout, stderr = p.communicate()
    #     if p.returncode != 0:
    #         print(stderr)
    #
    #     # test
    #     errors = compare_dirs(self.src, self.src_driver, self.dst, self.dst_driver,
    #                           check_mtimes=False)
    #     self.assertListEqual(errors, [])

    def test_delete_after(self):
        # init
        create_tree_structure(self.src, self.src_driver, {
            'dir1/subdir_a/file_alpha': 'alpha_content',
            'dir1/subdir_a/file_beta': 'beta_content',
            'dir1/subdir_b/file_mu': 'mu_content',
            'dir2/file_zeta': 'zeta_content,' * 100,
        })

        create_tree_structure(self.dst, self.dst_driver, {
            'dir3/subdir_a/file_alpha': 'alpha_content',
            'dir4/file_beta': 'beta_content',
        })

        # sync
        sync.synchronize(self.src, self.src_driver, self.dst, self.dst_driver, delete_after=True)

        # test
        errors = compare_dirs(self.src, self.src_driver, self.dst, self.dst_driver,
                              check_mtimes=False)
        self.assertListEqual(errors, [])

    def test_empty_dir(self):
        # init
        self.src_driver.makedirs(self.src)

        # sync
        sync.synchronize(self.src, self.src_driver, self.dst, self.dst_driver)

        # test
        errors = compare_dirs(self.src, self.src_driver, self.dst, self.dst_driver,
                              check_mtimes=False)
        self.assertListEqual(errors, [])

    def test_empty_subdir(self):
        # init
        self.src_driver.makedirs(self.src)
        self.src_driver.makedirs(self.src_driver.join(self.src, 'subdir1'))

        # sync
        sync.synchronize(self.src, self.src_driver, self.dst, self.dst_driver)

        # test
        errors = compare_dirs(self.src, self.src_driver, self.dst, self.dst_driver,
                              check_mtimes=False)
        self.assertListEqual(errors, [])

    def test_file(self):
        # init
        with self.src_driver.write(self.src) as fd:
            fd.write('content_string')

        # sync
        sync.synchronize(self.src, self.src_driver, self.dst, self.dst_driver)

        # test
        errors = compare_dirs(self.src, self.src_driver, self.dst, self.dst_driver,
                              check_mtimes=False)
        self.assertListEqual(errors, [])

    def test_general_case(self):
        # init
        create_tree_structure(self.src, self.src_driver, {
            'dir1/subdir_a/file_alpha': 'alpha_content',
            'dir1/subdir_a/file_beta': 'beta_content',
            'dir1/subdir_b/file_mu': 'mu_content',
            'dir2/file_zeta': 'zeta_content,' * 100,
        })

        # sync
        sync.synchronize(self.src, self.src_driver, self.dst, self.dst_driver)

        # test
        errors = compare_dirs(self.src, self.src_driver, self.dst, self.dst_driver,
                              check_mtimes=False)
        self.assertListEqual(errors, [])

    def test_preserves_mtimes(self):
        # init
        create_tree_structure(self.src, self.src_driver, {
            'dir1/subdir_a/file_alpha': 'alpha_content',
            'dir1/subdir_a/file_beta': 'beta_content',
            'dir1/subdir_b/file_mu': 'mu_content',
            'dir2/file_zeta': 'zeta_content,' * 100,
        })

        # sync
        sync.synchronize(self.src, self.src_driver, self.dst, self.dst_driver, preserve_mtimes=True)

        # test
        errors = compare_dirs(self.src, self.src_driver, self.dst, self.dst_driver)
        self.assertListEqual(errors, [])

    def test_preserves_mtimes_on_empty_dir(self):
        # init
        self.src_driver.makedirs(self.src)

        # sync
        sync.synchronize(self.src, self.src_driver, self.dst, self.dst_driver, preserve_mtimes=True)

        # test
        errors = compare_dirs(self.src, self.src_driver, self.dst, self.dst_driver)
        self.assertListEqual(errors, [])

    def test_preserves_mtimes_on_empty_subdir(self):
        # init
        self.src_driver.makedirs(self.src)
        self.src_driver.makedirs(self.src_driver.join(self.src, 'subdir1'))

        # sync
        sync.synchronize(self.src, self.src_driver, self.dst, self.dst_driver, preserve_mtimes=True)

        # test
        errors = compare_dirs(self.src, self.src_driver, self.dst, self.dst_driver)
        self.assertListEqual(errors, [])

    def test_preserves_mtimes_on_file(self):
        # init
        with self.src_driver.write(self.src) as fd:
            fd.write('content_string')

        # sync
        sync.synchronize(self.src, self.src_driver, self.dst, self.dst_driver, preserve_mtimes=True)

        # test
        errors = compare_dirs(self.src, self.src_driver, self.dst, self.dst_driver)
        self.assertListEqual(errors, [])

    def tearDown(self):
        self.src_tmp.remove()
        self.dst_tmp.remove()


def compare_dirs(src, src_driver, dst, dst_driver,
                 check_source=True, check_destination=True,
                 check_mtimes=True, check_sizes=True):
    """
    Compare directory structure and content.

    :param str src:
    :param BaseDriver src_driver:
    :param str dst:
    :param BaseDriver dst_driver:
    """
    errors = []

    d_src = {file_status.relative_path: file_status
             for file_status in src_driver.list_statuses(src, recursive=True)}
    d_dst = {file_status.relative_path: file_status
             for file_status in dst_driver.list_statuses(dst, recursive=True)}

    src_paths = set(d_src.keys())
    dst_paths = set(d_dst.keys())

    if check_source:
        for path in src_paths:
            if path not in dst_paths:
                errors.append(MissingInSource(relative_path=path))

    if check_destination:
        for path in dst_paths:
            if path not in src_paths:
                errors.append(MissingInDestination(relative_path=path))

    if check_mtimes:
        for path in src_paths & dst_paths:
            if d_src[path].mtime != d_dst[path].mtime:
                errors.append(MTimeDiffer(
                    relative_path=path,
                    src_mtime=d_src[path].mtime,
                    dst_mtime=d_dst[path].mtime,
                ))

    if check_sizes:
        for path in src_paths & dst_paths:
            if d_src[path].size != d_dst[path].size:
                errors.append(MTimeDiffer(
                    relative_path=path,
                    src_mtime=d_src[path].size,
                    dst_mtime=d_dst[path].size,
                ))

    return errors


MissingInSource = collections.namedtuple('MissingInSource', ['relative_path'])
MissingInDestination = collections.namedtuple('MissingInDestination', ['relative_path'])
SizeDiffer = collections.namedtuple('SizeDiffer', ['relative_path', 'src_size', 'dst_size'])
MTimeDiffer = collections.namedtuple('MTimeDiffer', ['relative_path', 'src_mtime', 'dst_mtime'])


def create_tree_structure(dirpath, driver, tree):
    """

    :param str dirpath:
    :param BaseDriver driver:
    :param dict[str,str] tree:
    """

    # For each file
    for path, content in tree.items():
        assert path

        # Create parent directories
        driver.makedirs(driver.dirname(driver.join(dirpath, path)))

        # Write file content
        with driver.write(driver.join(dirpath, path)) as fd:
            fd.write(content)
