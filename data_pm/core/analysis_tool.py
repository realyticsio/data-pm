#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
The Analysis Tool is the main entry point to run studies.

It can be used in python code by instantiating :class:`AnalysisTool`,
"""

# Global imports
import logging
import sys
import json
from requests.exceptions import ConnectionError

# Local imports
from data_pm import log, settings, client
from data_pm.core.study_components import study_control
from data_pm.core.study_components.study import StudyError
from data_pm.core.factory import get_factory
from data_pm.deployment import tasks


class AnalysisTool:
    """
    Tool used to launch a bunch of studies for a set of projects

    It will take care of launching studies in the dependency tree to get up-to-date inputs
    """

    def __init__(self, namespace):
        if hasattr(namespace, '__dict__'):
            d_namespace = vars(namespace)
        elif isinstance(namespace, dict):
            d_namespace = namespace
        else:
            raise ValueError('Invalid type for namespace')

        d_namespace['max_depth_check'] = d_namespace.get('max_depth_check', 1000)
        d_namespace['min_depth_force'] = d_namespace.get('min_depth_force', 0)
        d_namespace['ignore_parent'] = d_namespace.get('ignore_parent', False)
        d_namespace['unlock'] = d_namespace.get('unlock', False)
        d_namespace['params'] = d_namespace.get('params', None)
        d_namespace['debug'] = d_namespace.get('debug', False)

        self.d_namespace = d_namespace

        logger = log.initialize_logger()
        settings['data_pm.slack_channel'] = d_namespace.get('slack')

        shared_mode = self.parse_studies(d_namespace['studies'])

        self.shared_mode = shared_mode
        self.parse_projects(d_namespace['projects'], shared_mode)
        self.d_params = self.parse_params(d_namespace['params']) if d_namespace['params'] else {}

        # Log information about what is going to happen
        logger.debug("Projects to process: {}".format(d_namespace['projects']))
        logger.debug("Studies to process: {}".format(d_namespace['studies']))

    @staticmethod
    def parse_projects(projects, shared_mode):
        """
        Check that all provided projects exist.

        :param list projects: List of project names
        :param bool shared_mode:
        :return: List of project names
        """
        if len(projects) == 0 and not shared_mode:
            raise ValueError('No project specified')

        if len(projects) > 0 and shared_mode:
            raise ValueError('Project names were provided whereas studies are shared')

    @staticmethod
    def parse_studies(studies):
        """
        Check that all provided studies exist.
        Also determine if studies are shared or not.

        :param list studies: List of study names
        """
        if len(studies) == 0:
            raise ValueError('No study specified')

        l_studies = map(study_control.StudyControl.from_name, studies)
        shared_mask = [s.is_shared for s in l_studies]

        if any(shared_mask):
            if all(shared_mask):
                shared_mode = True
            else:
                raise ValueError('Shared and unshared studies are mixed')
        else:
            shared_mode = False

        return shared_mode

    @staticmethod
    def parse_params(params):
        """
        Parse list of parameters.

        :param params: List of arguments (ie. ['myparam=6', 'myotherparam=h'])
        :return: Dictionary of parameters (ie. {'myparam': '6', 'myotherparam': 'h'})
        """
        return dict([p.split('=') for p in params])

    def launch_studies(self):
        logger = logging.getLogger('data-pm')
        logger.debug("** BEGINNING STUDIES")
        try:
            self._launch_studies()
        except StudyError as e:
            e.print_error()
            sys.exit(1)

        logger.info("** STUDIES RAN SUCCESSFULLY")

    def _launch_studies(self):
        dag = get_factory().create_scheduling_dag(
            working_branch=self.d_namespace['branch'],
            study_names=self.d_namespace['studies'],
            project_names=self.d_namespace['projects'],
            max_depth_check=self.d_namespace['max_depth_check'],
            min_depth_force=self.d_namespace['min_depth_force'],
            ignore_parent=self.d_namespace['ignore_parent']
        )
        for study_ctrl, data in dag.studies_to_run():
            self.try_launch(study_ctrl)

    def try_launch(self, study_ctrl):
        logger = logging.getLogger('data-pm')

        exit_if_wrong_branch(study_ctrl, self.d_namespace)

        if not self.d_namespace['unlock']:
            exit_if_running(study_ctrl)

        if study_ctrl.is_shared:
            logger.info('Making shared study "{}" on "{}"'
                        .format(study_ctrl.name, study_ctrl.branch_out))
        else:
            logger.info('Making study "{}" for project "{}" on "{}"'
                        .format(study_ctrl.name, study_ctrl.project_name, study_ctrl.branch_out))

        study = get_factory().create_study(study_ctrl)
        study.origin = self.d_namespace['origin']

        study.launch(self.d_params)

    def submit_studies(self, working_dir, queue):
        async_result = tasks.launch_studies_async(self.d_namespace, queue)
        try:
            n_bytes = working_dir.upload(async_result.id)
            return async_result, n_bytes
        except ConnectionError as e:
            raise tasks.SubmitError('Failed to connect to "{}"'.format(e.request.url), async_result.id)
        except KeyboardInterrupt:
            raise tasks.SubmitError('KeyboardInterrupt', async_result.id)
        except Exception as e:
            raise tasks.SubmitError(e.args[0], async_result.id)


def exit_if_running(study_ctrl):
    """
    Stop if study is already running.
    """
    r = client.get_status(dict(
        branch=study_ctrl.branch_out,
        studies=study_ctrl.name,
        project=study_ctrl.project_name,
    ))

    if r.status_code != 200:
        return

    data = r.content
    if isinstance(data, bytes):
        data = data.decode('utf-8')

    obj = json.loads(data)

    if study_ctrl.is_shared:
        status = obj[study_ctrl.name].get('null', 'unknown')
    else:
        status = obj[study_ctrl.name].get(study_ctrl.project_name, 'unknown')

    if status in ['running', 'debugging']:
        print('Aborted: {} is already running or debugging.'.format(study_ctrl))
        sys.exit(1)


def exit_if_wrong_branch(study_ctrl, d_namespace):
    """
    data-pm does not support computations on parent branches.
    """
    if study_ctrl.branch_out == d_namespace['branch']:
        return

    print('Aborted: working on branch "{}", but study "{}" is not up-to-date on branch "{}"'
          .format(d_namespace['branch'], study_ctrl.name, study_ctrl.branch_out))

    # if study_ctrl.is_shared:
    #     url = settings['data_pm.api.url'] + '/dashboard/plot/shared/branch/{}/study/{}'\
    #         .format(study_ctrl.branch_out, study_ctrl.name)
    # else:
    #     url = settings['data_pm.api.url'] + '/dashboard/plot/project/{}/branch/{}/study/{}'\
    #         .format(study_ctrl.project_name, study_ctrl.branch_out, study_ctrl.name)
    #
    # print(url)
    sys.exit(1)
