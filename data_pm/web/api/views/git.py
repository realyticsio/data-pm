import flask_restplus

import data_pm
from data_pm.web.dashboard.tools.repositories import RepositoryManager


ns = flask_restplus.Namespace('git')


@ns.route('/create_branch/<branch_name>')
class Index(flask_restplus.Resource):
    def post(self, branch_name):
        """
        Create a new branch ``branch_name`` on the repo-api repository
        """

        with RepositoryManager(branch_name):
            pass

        return {'message': "ok"}


@ns.route('/pull_branch/<branch_name>')
class Index(flask_restplus.Resource):
    def post(self, branch_name):
        """
        Pull branch ``branch_name`` on the repo-api repository
        """

        with RepositoryManager(branch_name) as repo_mngr:
            repo_mngr.pull()

        return {'message': "ok"}


@ns.route('/delete_branch/<branch_name>')
class Index(flask_restplus.Resource):
    def post(self, branch_name):
        """
        Delete branch ``branch_name`` on the repo-api repository
        """

        repo_mngr = RepositoryManager(branch_name)
        path = repo_mngr.repo_path

        data_pm.file.remove(path, recursive=True)

        return {'message': "ok"}
