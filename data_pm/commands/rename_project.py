#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Rename a project.

Takes care of moving data and converting study logs.
WARNING: this command has to be run on the data-pm server!
WARNING: this will only move data on the central data repository!
"""

# Global imports
from __future__ import print_function
import sys

# Local imports
import data_pm
from data_pm.web.api.backend import session, models
from data_pm import settings
from data_pm.core import paths, drivers, study_mngr
from data_pm.core.study_components import study_control
from data_pm.tools.cli_handler import BaseCommand


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('old_project_name')
        parser.add_argument('new_project_name')

    def handle(self, namespace):
        old_name = namespace.old_project_name
        new_name = namespace.new_project_name

        driver_name = settings['data_pm.redirection.driver']

        # Check whether the target destination does not already exist of the main driver
        main_driver = drivers.get_driver(driver_name)
        new_path = main_driver.abspath(paths.generate_dir(main_driver, new_name))
        if main_driver.exists(new_path):
            raise ValueError('Destination directory {} already exists'.format(new_path))

        # Convert log meta data
        print('Converting study meta logs')
        for study_log in session.query(models.StudyLog).filter(models.StudyLog.project == old_name).all():
            study_log.project = new_name
        session.commit()

        # Convert LastUpdate rows
        print('Converting LastUpdate rows')
        for lu in session.query(models.LastUpdate).filter(models.LastUpdate.project == old_name).all():
            lu.project = new_name
        session.commit()

        s_drivers = set()
        # Move data for each driver
        for study_name in study_mngr.get_study_names():
            study = study_control.StudyControl.from_name(study_name)
            study.set_project_name(old_name)
            study.set_branch_out('master')

            s_drivers |= {output.resource.driver for _, output in study.get_outputs().items()}

        s_drivers.add(main_driver)
        s_drivers.add(data_pm.file)

        for driver in s_drivers:
            old_path = driver.abspath(paths.generate_dir(driver, old_name))
            new_path = driver.abspath(paths.generate_dir(driver, new_name))

            if driver.exists(old_path):
                print('Moving {} to {}'.format(old_path, new_path))
                driver.rename(old_path, new_path)

        session.close()
