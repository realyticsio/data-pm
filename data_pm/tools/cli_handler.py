# Global imports
import os
import pkgutil
import importlib
import argparse
import sys
import warnings
# Local imports
from data_pm.core import study_mngr


def handle(args, dirs=list(['commands'])):
    """
    Handle command line arguments, optionally loading additional commands.

    Will exit python with the command return code.

    :param args: List of command line arguments
    :param dirs: Additional directories to load commands from
    """

    from data_pm.log import initialize_logger
    initialize_logger()

    clp = MultiCommandsParser(dirs)
    ret = clp.parse_args(args)

    if ret != 0:
        sys.exit(ret)


class MultiCommandsParser(object):
    def __init__(self, dirs):
        """
        Build a command line parser with argparse, loading commands on the fly.

        General usage is ``pm command args``. The class automatically detects available commands by listing
        modules in the specified `dirs` but will not load these modules until it is actually necessary.

        :param dirs:
        """
        self.dirs = dirs
        self.main_parser = argparse.ArgumentParser(prog='pm', formatter_class=argparse.RawTextHelpFormatter)
        self.parsers = self.main_parser.add_subparsers(
            dest='command',
            #metavar='command',
            title='Available commands',
            help='To get help with a command, run `pm command -h`'
            )

        d = {}

        for commands_dir in dirs:
            path = os.path.join(os.path.dirname(os.path.dirname(__file__)), commands_dir)

            # Load data-pm commands
            d.update(fetch_commands(path, 'data_pm.' + commands_dir + '.'))
            # Load current workspace commands
            d.update(fetch_commands(commands_dir, commands_dir + '.'))

        self.d_commands = {name: CommandParser(name, module, self.parsers)
                           for name, module in sorted(d.items())}

    def load_all(self):
        """
        Load all command modules
        """
        for name in self.d_commands:
            try:
                self.load(name)
            except ImportError:
                warnings.warn('Could not load module for command "{}"'.format(name))

    def load(self, name):
        """
        Load a specific command module
        :param name:
        :return:
        """
        self.d_commands[name].load()

    def parse_args(self, args=None):
        """
        Parse arguments and handle the commands.

        If a valid command has been recognized in the args, the command module will be loaded and used.
        Otherwise, all the command modules will be loaded to print a listing of available commands and
        a short description (first line of help attribute).

        :param list[str] args: If None, will use sys.argv
        :return: command return code
        :rtype: int
        """
        if args is None:
            args = sys.argv

        prog, args = args[0], args[1:]

        # Move the command at the beginning of the args
        # (to replace "pm -h launch" by "pm launch -h" that is more helpful)
        for i, arg in list(enumerate(args)):
            if arg in self.d_commands:
                args.insert(0, args.pop(i))
                break

        name = args[0] if len(args) > 0 else None
        if name in self.d_commands:
            # Load the command
            self.load(name)

            namespace = self.main_parser.parse_args(args)
            return self.d_commands[name].handle(namespace)
        else:
            # No command, load all to show help
            self.parsers.container.description = self.print_command_help()
            self.main_parser.parse_args(args)
            return 0

    def print_command_help(self):
        self.load_all()
        pad = max(map(len, self.d_commands))
        template = '{{:<{pad}}}: {{}}'.format(pad=pad)

        return '\n'.join([
            template.format(name, get_first_line(command.command.help)
                                  if command.command and command.command.help else 'N/A')
            for name, command in self.d_commands.items()])


class CommandParser(object):
    def __init__(self, name, module, parsers):
        """
        A command structure containing the parser.

        The inner parser is created blank: arguments and help are added when :func:`load` is called.

        :param str name:
        :param str module:
        :param argparse._SubParsersAction parsers:
        """
        self.name = name
        self.module = module
        self.command = None

        self.parsers = parsers
        self.parser = self.parsers.add_parser(name)

    def load(self):
        """
        Load the command module and add arguments and metadata on the parser
        """
        self.command = importlib.import_module(self.module).Command(self.parsers, self.parser)

        self.parser.help = self.command.help
        self.parser.description = self.command.help
        self.command.add_arguments(self.parser)

    def handle(self, namespace):
        """
        Handle the result of the parsing

        :param argparse.Namespace namespace:
        :rtype: int
        """
        ret = self.command.handle(namespace)

        if ret is None:
            ret = 0

        return ret


class BaseCommand(object):
    help = ''
    """Help of the command, the first line should be a short description"""

    def __init__(self, parsers, parser):
        """
        Base class representing a command.

        Commands are python modules (usually files) containing a class `Command` inheriting from :class:`BaseClass` and
        redefining :attr:`help`, :func:`add_arguments` and `func`:handle:.

        :param argparse.ArgumentParser parser:
        :param argparse._SubParsersAction parsers:
        """
        self.parsers = parsers
        self.parser = parser

    def add_arguments(self, parser):
        """
        Redefine this method to add arguments to the parser.

        :param argparse.ArgumentParser parser:
        """
        pass

    def handle(self, namespace):
        """
        Redefine this method to do something with the result of the command parsing.

        :param argparse.Namespace namespace:
        :return: Command line return code
        :rtype: int|None
        """
        raise NotImplementedError()


def fetch_commands(path, prefix):
    commands = {name: '{}{}'.format(prefix, name) for (_, name, is_pkg) in pkgutil.iter_modules([path])
                if not is_pkg and not name.startswith('_')}
    return commands


def get_first_line(txt):
    lines = txt.split('\n')
    for line in lines:
        if len(line.strip()) > 0:
            return line

    return ''


def refactor(namespace, all_projects):
    args = set(namespace.projects_or_studies)
    all_studies = set(study_mngr.get_all_study_names())

    projects, studies = args & all_projects, args & all_studies
    others = args - all_projects - all_studies

    if len(others) > 0:
        print('Unknown studies or projects: {}'.format(', '.join(sorted(others))))
        sys.exit(1)

    setattr(namespace, 'projects', sorted(projects))
    setattr(namespace, 'studies', sorted(studies))
    del namespace.projects_or_studies
