# coding: utf-8

# Global imports
from __future__ import print_function
import os
import git
import socket
import subprocess


def get_repo(path):
    return git.Repo(path)


def clone(url, path, branch='master', single_branch=False):
    args = ['git', 'clone', url, path, '-b', branch]

    if single_branch:
        args.append('--single-branch')

    # import os
    # import pwd
    # print(pwd.getpwuid(os.getegid()).pw_name)
    # print(pwd.getpwuid(os.getuid()).pw_name)
    #
    # print(subprocess.check_output(['ssh', '-v', 'git@bitbucket.org']))

    subprocess.check_output(args)

    return git.Repo(path)


def get_current(repo):
    return repo.active_branch.name


def get_locals(repo):
    return repo.branches


def is_local(repo, branch_name):
    return branch_name in get_locals(repo)


def remote_update(path):
    subprocess.check_call(
        ['git', 'remote', 'update', '--prune'],
        cwd=path
    )


def get_remotes(repo):
    branches = repo.remotes.origin.refs
    return [os.path.basename(b.abspath) for b in branches]


def is_remote(repo, branch_name):
    return branch_name in get_remotes(repo)


def create_branch(repo, branch_name):
    if not is_local(repo, branch_name):
        repo.create_head(branch_name)


def checkout(repo, branch_name, create=False):
    if create is True:
        create_branch(repo, branch_name)
    if branch_name != get_current(repo):
        repo.git.checkout(branch_name)


def fetch(repo):
    repo.git.fetch()


def reset_state(func):
    def wrapper(repo, branch_name):
        saved_branch = get_current(repo)
        func(repo, branch_name)
        checkout(repo, saved_branch)
    return wrapper


@reset_state
def pull_branch(repo, branch_name):
    if not is_local(repo, branch_name):
        fetch(repo)
    checkout(repo, branch_name)
    repo.git.pull()


def commit(repo):
    repo.git.add('--all')
    if repo.is_dirty():
        # Commit changes in order to be able to checkout later
        if not get_value(repo, 'email'):
            set_value(repo, 'email', socket.gethostname())
        if not get_value(repo, 'name'):
            set_value(repo, 'name', socket.gethostname())

        repo.git.commit('-am', '@@ Automatic commit @@ DO NOT PUSH @@')


def get_value(repo, key):
    """
    Get a value from the "user" section of the git config

    :param git.Repo repo:
    :param str key:
    :return: the value, or None if the "user" section could not be found or if the key could not be found
    """
    config_reader = repo.config_reader()

    if not config_reader.has_section('user'):
        return None

    if not config_reader.has_option('user', key):
        return None

    return config_reader.get_value('user', key)


def set_value(repo, key, value):
    repo.config_writer().set_value('user', key, value)
