# Global settings
import logging
from logging import handlers, Handler

# Local settings
from data_pm import settings, client


def initialize_logger():
    logger = logging.getLogger('data-pm')

    # Remove handlers
    logger.handlers = []

    log_level = logging.getLevelName(settings['data_pm.log.level'])
    logger.setLevel(log_level)

    attach_handler('stream')

    return logger


def attach_handler(handler_name, **kwargs):
    logger = logging.getLogger('data-pm')

    handler = create_handler(handler_name, **kwargs)
    logger.addHandler(handler)


def create_handler(handler_name, **kwargs):
    log_level = logging.getLevelName(settings['data_pm.log.level'])

    log_format = settings['data_pm.log.format']
    date_format = settings['data_pm.log.date.format']
    log_formatter = logging.Formatter(fmt=log_format, datefmt=date_format)

    if handler_name == 'stream':
        handler = logging.StreamHandler()

    elif handler_name == 'rotating_file':
        handler = handlers.RotatingFileHandler(kwargs['path'], backupCount=10)

        if kwargs.get('rollover'):
            handler.doRollover()

    elif handler_name == 'requests':
        handler = RequestsHandler(
            branch=kwargs['branch_name'],
            project=kwargs['project_name'],
            study=kwargs['study_name']
        )

    else:
        # Bad handler name
        raise ValueError('handler_name "{}" is not valid')

    handler.setFormatter(log_formatter)
    handler.setLevel(log_level)

    return handler


class RequestsHandler(Handler):
    def __init__(self, **kwargs):
        """
        Send log messages to an API as JSON

        :param kwargs: Parameters to add to the json document
        """
        super(RequestsHandler, self).__init__()
        self.kwargs = kwargs

    def emit(self, record, format=True):
        data = self.kwargs.copy()
        data['message'] = self.format(record) if format else record
        client.post_log(data)
