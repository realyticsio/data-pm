"""
description: |-
  Counts the number of N-grams

inputs:
- alias: doc
  name: doc.txt
  parent: generate_data

outputs:
- alias: counts
  name: counts.csv
  driver: s3
  description: |-
    csv file containing the number of each ngrams
    Example:
      ngram,count
      abc|def,3
      ghi|def,10

parameters:
- name: n
  default: 2
  dtype: int


"""

import collections
import pandas as pd

import data_pm


class Study(data_pm.BaseStudy):
    def run(self):
        # Parse input file
        with data_pm.s3.read(self.i.doc) as reader:
            txt = reader.read()

        d_ngrams = collections.Counter()

        # Count trigrams
        words = tokenize(txt)
        for i in range(len(words) - self.p.n):
            key = '|'.join(words[i:i + self.p.n])
            d_ngrams[key] += 1

        # Write counts to output file
        with data_pm.s3.write(self.o.counts) as writer:
            pd.Series(d_ngrams).to_csv(writer)


def tokenize(txt):
    """
    Tokenize a text

    Use nltk in a real case

    :param str txt:
    :rtype: list[str]
    """
    words = txt.replace('\n', ' ').split(' ')

    return words
