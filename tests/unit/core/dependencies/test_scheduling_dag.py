# Global imports
import mock
from unittest import TestCase

# Local imports
from data_pm.core.factory import test_factory, set_factory, get_factory


def studies_to_run(dag):
    l_result = []
    for study_ctrl, data in dag.studies_to_run():
        datanode = data['datanode']
        l_result.append((
            datanode.max_depth,
            study_ctrl.name,
            study_ctrl.is_shared,
            study_ctrl.project_name,
            study_ctrl.branch_out,
        ))
    return l_result


class ProjectTestCase(TestCase):

    def setUp(self):
        factory = test_factory.TestFactoryProject()
        set_factory(factory)

        self.d_last_updates = {
            'master': {
                'A': {'alpha': '2015-01-01'},
                'B': {'alpha': '2015-01-02'},
            },
            'dev1': {
                'C': {'alpha': '2015-01-03'},
                'D': {'alpha': '2015-01-04'},
            },
        }

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_1(self, mock_lu):
        """
        Simulation of command `pm submit D alpha`
        A is not up-to-date
        """

        # Set A not up-to-date
        self.d_last_updates['master']['A']['alpha'] = None

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (3, 'A', False, 'alpha', 'master'),
            (2, 'B', False, 'alpha', 'master'),
            (1, 'C', False, 'alpha', 'dev1'),
            (0, 'D', False, 'alpha', 'dev1')
        ]

        study = get_factory().create_study_control('D')
        study.set_project_name('alpha')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study]
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_2(self, mock_lu):
        """
        Simulation of command `pm submit D alpha --ignore-parent`
        A is not up-to-date
        """

        # Set A not up-to-date
        self.d_last_updates['master']['A']['alpha'] = None

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (0, 'D', False, 'alpha', 'dev1')
        ]

        study = get_factory().create_study_control('D')
        study.set_project_name('alpha')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
            ignore_parent=True
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_3(self, mock_lu):
        """
        Simulation of command `pm submit D alpha --max-depth-check=0`
        A is not up-to-date
        """

        # Set A not up-to-date
        self.d_last_updates['master']['A']['alpha'] = None

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (0, 'D', False, 'alpha', 'dev1')
        ]

        study = get_factory().create_study_control('D')
        study.set_project_name('alpha')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
            max_depth_check=0
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_4(self, mock_lu):
        """
        Simulation of command `pm submit D alpha --max-depth-check=1`
        A is not up-to-date
        """

        # Set A not up-to-date
        self.d_last_updates['master']['A']['alpha'] = None

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (0, 'D', False, 'alpha', 'dev1')
        ]

        study = get_factory().create_study_control('D')
        study.set_project_name('alpha')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
            max_depth_check=1
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_5(self, mock_lu):
        """
        Simulation of command `pm submit D alpha`
        Everything is up-to-date
        """

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (0, 'D', False, 'alpha', 'dev1')
        ]

        study = get_factory().create_study_control('D')
        study.set_project_name('alpha')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_6(self, mock_lu):
        """
        Simulation of command `pm submit D alpha --min-depth-force=1`
        Everything is up-to-date
        """

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (1, 'C', False, 'alpha', 'dev1'),
            (0, 'D', False, 'alpha', 'dev1')
        ]

        study = get_factory().create_study_control('D')
        study.set_project_name('alpha')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
            min_depth_force=1
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_7(self, mock_lu):
        """
        Simulation of command `pm submit B D alpha`
        Everything is up-to-date
        """

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (2, 'B', False, 'alpha', 'master'),
            (1, 'C', False, 'alpha', 'dev1'),
            (0, 'D', False, 'alpha', 'dev1')
        ]

        study_1 = get_factory().create_study_control('B')
        study_1.set_project_name('alpha')

        study_2 = get_factory().create_study_control('D')
        study_2.set_project_name('alpha')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study_1, study_2],
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_8(self, mock_lu):
        """
        Simulation of command `pm submit D alpha --max-depth-check=1`
        C is not up-to-date
        """

        # Set C not up-to-date
        self.d_last_updates['master']['B']['alpha'] = None

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (1, 'C', False, 'alpha', 'dev1'),
            (0, 'D', False, 'alpha', 'dev1')
        ]

        study = get_factory().create_study_control('D')
        study.set_project_name('alpha')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
            max_depth_check=1
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_9(self, mock_lu):
        """
        Simulation of command `pm submit D alpha --ignore-parent`
        C is not up-to-date
        """

        # Set C not up-to-date
        self.d_last_updates['dev1']['C']['alpha'] = '1999-01-01'

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (1, 'C', False, 'alpha', 'dev1'),
            (0, 'D', False, 'alpha', 'dev1')
        ]

        study = get_factory().create_study_control('D')
        study.set_project_name('alpha')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
            ignore_parent=True
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_10(self, mock_lu):
        """
        Simulation of command `pm submit A alpha`
        Everything is up-to-date

        Test if the scheduler works with a one-node graph.
        """

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (0, 'A', False, 'alpha', 'master'),
        ]

        study = get_factory().create_study_control('A')
        study.set_project_name('alpha')

        dag = get_factory().create_scheduling_dag(
            working_branch='master',
            studies=[study],
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))


class SharedTestCase(TestCase):
    def setUp(self):
        factory = test_factory.TestFactoryShared()
        set_factory(factory)

        self.d_last_updates = {
            'master': {
                'A': '2015-01-01',
                'B': '2015-01-02',
                'D': '2015-01-01',
                'F': {
                    'alpha': '2015-01-03',
                    'beta': '2015-01-03',
                    'gamma': '2015-01-03'
                },
                'G': '2015-01-04',
            },
            'dev1': {
                'C': '2015-01-02',
                'E': {
                    'alpha': '2015-01-03',
                    'beta': '2015-01-03',
                    'gamma': '2015-01-03'
                },
                'H': '2015-01-05',
            },
        }

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_1(self, mock_lu):
        """
        Simulation of command `pm submit H`
        Everything is up-to-date
        """

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (0, 'H', True, None, 'dev1'),
        ]

        study = get_factory().create_study_control('H')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_2(self, mock_lu):
        """
        Simulation of command `pm submit H`
        C is not up-to-date
        """

        # Set C not up-to-date
        self.d_last_updates['dev1']['C'] = '1999-01-01'

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (2, 'C', True, None, 'dev1'),
            (1, 'E', False, 'alpha', 'dev1'),
            (1, 'E', False, 'beta', 'dev1'),
            (1, 'E', False, 'gamma', 'dev1'),
            (0, 'H', True, None, 'dev1'),
        ]

        study = get_factory().create_study_control('H')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_3(self, mock_lu):
        """
        Simulation of command `pm submit H`
        E is not up-to-date for project alpha
        """

        # Set E not up-to-date for project alpha
        self.d_last_updates['dev1']['E']['alpha'] = '1999-01-01'

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (1, 'E', False, 'alpha', 'dev1'),
            (0, 'H', True, None, 'dev1'),
        ]

        study = get_factory().create_study_control('H')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_4(self, mock_lu):
        """
        Simulation of command `pm submit H`
        A is not up-to-date
        """

        # Set E not up-to-date for project alpha
        self.d_last_updates['master']['A'] = None

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (4, 'A', True, None, 'master'),
            (3, 'B', True, None, 'master'),
            (2, 'C', True, None, 'dev1'),
            (1, 'E', False, 'alpha', 'dev1'),
            (1, 'E', False, 'beta', 'dev1'),
            (1, 'E', False, 'gamma', 'dev1'),
            (2, 'F', False, 'alpha', 'master'),
            (2, 'F', False, 'beta', 'master'),
            (2, 'F', False, 'gamma', 'master'),
            (1, 'G', True, None, 'master'),
            (0, 'H', True, None, 'dev1'),
        ]

        study = get_factory().create_study_control('H')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))

    @mock.patch('data_pm.core.dependencies.scheduling_dag.get_last_updates')
    def test_5(self, mock_lu):
        """
        Simulation of command `pm submit H --ignore-parent`
        B is not up-to-date
        """

        # Set E not up-to-date for project alpha
        self.d_last_updates['master']['B'] = None

        mock_lu.return_value = self.d_last_updates

        l_expected_result = [
            (0, 'H', True, None, 'dev1'),
        ]

        study = get_factory().create_study_control('H')

        dag = get_factory().create_scheduling_dag(
            working_branch='dev1',
            studies=[study],
            ignore_parent=True
        )

        l_result = studies_to_run(dag)
        l_branches = list(reversed(dag.d_branches.keys()))
        l_short_result = [(l_branches.index(x[4]), -x[0], not x[2]) for x in l_result]

        self.assertSetEqual(set(l_result), set(l_expected_result))
        self.assertListEqual(l_short_result, sorted(l_short_result))
