=================
Advanced tutorial
=================

Drivers
*******

Why would you need drivers?
"""""""""""""""""""""""""""
When you write an output of a study, you usually create a file or a folder on a filesystem.
Later on, it may be checked by data_pm to know if the study is up to date and it may be loaded as the input of another study.

Several cases will lead you to use other types of filesystem than your local file system:

- Distributing computations, because a cluster of machines will require a shared file system
- Making data more highly available and resilient using replication
- Creating backups by uploading to a more reliable storage
- ...

So data_pm needs a driver to access data, write data and get modification time.
Several drivers are natively supported, others can be added by implementing the methods that are used to manage
inputs and outputs.


Currently supported drivers
"""""""""""""""""""""""""""

======  ====================================  ============
Scheme  Description                           Dependencies
======  ====================================  ============
file    Local file system
hdfs    Hadoop Distributed File System        Java, Hadoop, the python package "pydoop"
s3      Amazon Simple Storage Service         Python package "boto3"
======  ====================================  ============


Using drivers
"""""""""""""
To use drivers, first install their dependencies.
data_pm will not try to load them until it actually needs to do something with data.

Then, simply set the ``driver_name`` parameter of an output. For example::

    'my_output_alias': data_pm.Output('my_output_name', driver_name='hdfs')

Every action on this output (creating the directory that contains it, checking the modification
time, removing the output, ...) will be done using the corresponding driver, on the corresponding file system.
Note that the path will follow the same template as the one on the local file system.
For instance, the full URL of the last example would be something like::

    hdfs://my-hdfs-hostname:9000/user/my_hdfs_user/PROJECTS/my_project/DATA/my_study/my_output_name



Branches
********

Branches are based on git. They allow different versions of the same data to coexist.
By default, when you launch a study, it will look for the current git branch to read study inputs and write study outputs
corresponding to that branch.

It is possible to use another branch's data. Either fully by using the `--branch` option
or partially by configuring the file branches.yml.

The file branches.yml defines the branches and their dependencies::

    master:
      parent:
      recompute_from:

    dev:
      parent: master
      recompute_from:
        sessions.computing

Here, the "master" definition is empty because we want it to be a root branch.
The "dev" definition makes it inherit data from "parent" for **all** studies **except**
the studies after "sessions.computing" included.

(Insert graph with dependencies)


Shared studies
**************

Introduction
""""""""""""
A shared study means a study that is shared across several projects.
There are two many use cases.

- (Case 1) A unique source of data for several projects::

                                  / (Project "eco", Study "spots_computing")
    (Shared study "channel_data") - (Project "big", Study "spots_computing")
                                  \ (Project "ben", Study "spots_computing")

- (Case 2) A single summary output of several projects::

    (Project "eco", Study "sessions_computing") \
    (Project "big", Study "sessions_computing") -  (Shared study "global_traffic_counting")
    (Project "ben", Study "sessions_computing") /

Of course, you can link shared studies between them.
In fact, if you had only one project, you could use shared study in place of project studies.
Though it is advisable not to do so.

In the documentation, the studies that are NOT shared may be referred as:

- Project studies
- Simple studies
- Normal studies
- Unshared studies


Difference with project studies
"""""""""""""""""""""""""""""""

Because shared studies do not depend on a project to run, they work a bit differently:

- Their module code should be located in `DEV/SHARED_STUDIES` instead of `DEV/STUDIES`

- Their output data and log are located elsewhere:

  - ``SHARED/DATA`` instead of ``PROJECTS/[project_name]/DATA``
  - ``SHARED/LOG`` instead of ``PROJECTS/[project_name]/LOG``

- They can be found in the *Shared Studies* section of the dashboard, instead of a project page.

- In the case of a shared study using project studies output (case 2), the way of handling data is a bit different
  (see :ref:`next section <shared-studies-connections>`)


.. _shared-studies-connections:

Connections between shared and project studies
""""""""""""""""""""""""""""""""""""""""""""""

There are two kind of studies, so there are four many possible connections:


- **Normal -> Normal**

  The most common case, described in the :doc:`tutorial <tutorial>`.

- **Shared -> Shared**

  No difference with previous case. The data will just be loaded from a ``SHARED/...`` directory instead of ``PROJECTS/...``,
  but this does not affect the way of coding.

- **Shared -> Normal (case 1)**

  Still no difference.

- **Normal -> Shared (case 2)**

  The tricky case, as data may be loaded from several projects at once.

  In the shared study code, the inputs may be defined the same as previously.
  You may use the ``projects`` argument when defining the :py:class:`Input<data_pm.core.input.Input>` instances
  to restrict the list of project you want to load data from, otherwise **all the projects** will loaded.

  **AT RUNTIME**, :py:class:`self.inputs<data_pm.core.study.BaseStudy.inputs>` will be modified to contain one
  input per defined input and per project. For example, if you defined::

      inputs = {
          "logs": data_pm.Input('log.collecting', 'logs', projects=['eole', 'starship'])
      }

  ``self.inputs`` will contain this data at runtime::

      {
          "logs__eole": <data_pm.Input configured for eole>,
          "logs__starship": <data_pm.Input configured for starship>
      }


