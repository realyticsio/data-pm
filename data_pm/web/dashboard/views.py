# Global imports
from __future__ import print_function
import re
import math
import flask
import json
import redis
import numpy as np
from datetime import datetime, timedelta
from itertools import groupby

# Local imports
from data_pm import settings
from data_pm.core import project_mngr
from data_pm.web.api import backend
from data_pm.web.api.models import StudyLog
from data_pm.web.dashboard import tools
from data_pm.web.dashboard.tools import repositories
from data_pm.core.factory import get_factory
try:
    from urlparse import urlparse
except:
    from urllib.parse import urlparse


blueprint = flask.Blueprint('dashboard', __name__)


@blueprint.route('/')
def index():
    return flask.redirect(flask.url_for('.projects', branch='master'))


############
# Branches #
############

@blueprint.route('/branches/<branch>', endpoint='branches')
@repositories.switch_branch
def branches_index(branch):
    environments = sorted(backend.get_environments(), key=lambda x: x.datetime, reverse=True)
    ctx = tools.context.load_context(flask.request)
    ctx['current_branch'] = branch
    ctx['environments'] = environments
    return flask.render_template('dashboard_app/branches/index.html', **ctx)


####################
# LIST OF PROJECTS #
####################

@blueprint.route('/projects/branch/<branch>', endpoint='projects')
@repositories.switch_branch
def project_index(branch):
    ctx = tools.context.load_context(flask.request)
    ctx['projects'] = sorted(project_mngr.get_project_names())
    ctx['current_branch'] = branch
    project_mngr.clear_cache()
    return flask.render_template('dashboard_app/project/index.html', **ctx)


###########################
# LIST OF PROJECT STUDIES #
###########################

@blueprint.route('/project/<project>/branch/<branch>', endpoint='project')
@repositories.switch_branch
def project_display(project, branch):
    rows = backend.get_filtered_study_logs(
        branch_name=branch,
        project_name=project
    )

    ctx = tools.context.load_context(flask.request)
    ctx['project_name'] = project
    ctx['study_logs'] = rows
    ctx['current_branch'] = branch

    return flask.render_template('dashboard_app/project/display.html', **ctx)


##########################
# LIST OF SHARED STUDIES #
##########################

@blueprint.route('/shared/branch/<branch>', endpoint='shared_studies')
@repositories.switch_branch
def shared_study_index(branch):
    rows = backend.get_filtered_study_logs(
        branch_name=branch,
        shared_only=True
    )

    ctx = tools.context.load_context(flask.request)
    ctx['study_logs'] = rows
    ctx['current_branch'] = branch

    return flask.render_template('dashboard_app/shared/index.html', **ctx)


#########
# STUDY #
#########

@blueprint.route('/project/<project>/branch/<branch>/study/<study>', endpoint='study')
@blueprint.route('/shared/branch/<branch>/study/<study>', endpoint='study')
@repositories.switch_branch
def study(branch, study, project=None):
    study_ctrl = get_factory().create_study_control(
        study_name=study,
        project_name=project,
        configure_branch=True
    )

    ctx = tools.context.load_context(flask.request)
    ctx['study'] = study_ctrl
    ctx['project_name'] = project
    ctx['current_branch'] = branch

    # Add a function to get study logs for parent studies
    ctx['func'] = func

    # Prefetch study log for current study
    ctx['study_log'] = backend.get_filtered_study_logs(branch, study, project)[0]

    return flask.render_template('dashboard_app/study.html', **ctx)


def func(branch, study, project):
    rows = backend.get_filtered_study_logs(branch, study, project)
    return rows[0] if rows else StudyLog(status='unknown')


##################
# FAILED STUDIES #
##################

@blueprint.route('/failed_studies/branch/<branch>', endpoint='failed_studies')
@repositories.switch_branch
def failed_studies_index(branch):
    rows = backend.get_filtered_study_logs(
        branch_name=branch,
        status=['failed', 'stopped', 'warning']
    )

    with_end = [s for s in rows if s.end_date]
    with_end = sorted(with_end, key=lambda s: s.end_date, reverse=True)

    without_end = [s for s in rows if not s.end_date]
    without_end = sorted(without_end, key=lambda s: s.start_date, reverse=True)

    ctx = tools.context.load_context(flask.request)
    ctx['study_logs'] = without_end + with_end
    ctx['current_branch'] = branch
    ctx['today'] = datetime.now().date()

    return flask.render_template('dashboard_app/failed_studies/index.html', **ctx)


###################
# RUNNING STUDIES #
###################

@blueprint.route('/running_studies/branch/<branch>', endpoint='running_studies')
@repositories.switch_branch
def running_studies_index(branch):
    rows = backend.get_study_logs(status=['running', 'debugging'])
    rows = sorted(rows, key=lambda s: (s.branch, s.start_date), reverse=True)

    ctx = tools.context.load_context(flask.request)
    ctx['study_logs'] = rows
    ctx['current_branch'] = branch

    return flask.render_template('dashboard_app/running_studies/index.html', **ctx)


##############
# STATISTICS #
##############

def to_timedelta(ts):
    if not math.isnan(ts):
        return timedelta(seconds=int(ts))


@blueprint.route('/statistics/branch/<branch>/today/<today>', endpoint='statistics')
@repositories.switch_branch
def statistics(branch, today):
    """
    `groupby` function needs a sorted input with the same key it uses
    """
    top_size = 8

    if today == 'True':
        today = True
    else:
        today = False

    rows = backend.get_filtered_study_logs(
        branch_name=branch,
        status='success',
        today=today
    )

    # -- Compute execution time by project for each study --
    top_studies = []
    for study_name, group in groupby(rows, key=lambda s: s.name):
        group = list(group)
        top_studies.append({
            'study_name': study_name,
            'average': to_timedelta(np.mean([s.iduration for s in group if s.iduration is not None])),
            'sum': to_timedelta(np.sum([s.iduration for s in group if s.iduration is not None])),
            'by_project': sorted(['{} {}'.format(s.duration, s.project)
                                  for s in group if s.duration is not None], reverse=True)
        })
    top_studies = sorted(top_studies, key=lambda x: x['sum'], reverse=True)[:top_size]

    # -- Compute redirection time by project for each study --
    top_redirection = []
    for study_name, group in groupby(rows, key=lambda s: s.name):
        group = list(group)
        top_redirection.append({
            'study_name': study_name,
            'average': to_timedelta(np.mean([s.iredirection_time for s in group if s.iredirection_time is not None])),
            'sum': to_timedelta(np.sum([s.iredirection_time for s in group if s.iredirection_time is not None])),
            'by_project': sorted(['{} {}'.format(s.redirection_time, s.project)
                                  for s in group if s.redirection_time is not None], reverse=True)
        })
    top_redirection = sorted(top_redirection, key=lambda x: x['sum'], reverse=True)[:top_size]

    # -- Compute duration by study for each project --
    top_projects = []
    sorted_by_project = sorted(rows, key=lambda s: str(s.project))
    for project, group in groupby(sorted_by_project, key=lambda s: s.project):
        group = list(group)
        top_projects.append({
            'project_name': project,
            'average': to_timedelta(np.mean([s.iduration for s in group if s.iduration is not None])),
            'sum': to_timedelta(np.sum([s.iduration for s in group if s.iduration is not None])),
            'by_study': sorted(['{} {}'.format(s.duration, s.name)
                                for s in group if s.duration is not None], reverse=True)
        })
    top_projects = sorted(top_projects, key=lambda x: x['sum'], reverse=True)[:top_size]

    # -- Fill in context --
    ctx = tools.context.load_context(flask.request)
    ctx['current_branch'] = branch
    ctx['today'] = str(today)
    ctx['not_today'] = str(not today)
    ctx['top_studies'] = top_studies
    ctx['top_redirection'] = top_redirection
    ctx['top_projects'] = top_projects
    ctx['total_duration'] = to_timedelta(sum([s.iduration for s in rows if s.iduration is not None]))

    return flask.render_template('dashboard_app/statistics/index.html', **ctx)


#################
# PENDING TASKS #
#################

@blueprint.route('/pending_tasks/branch/<branch>', endpoint='pending_tasks')
@repositories.switch_branch
def pending_tasks(branch):
    broker_hostname = urlparse(settings['celery.broker.url']).hostname
    client = redis.Redis(host=broker_hostname)

    # Fetch pending tasks from redis queues
    tasks = []
    for queue in settings['celery.queue.names']:
        tasks += client.lrange(queue, 0, -1)

    # Parse JSON strings
    headers = [json.loads(s.decode('utf-8'))['headers'] for s in tasks]

    result = [(h['id'], eval(h['argsrepr'])[0]) for h in headers]
    """:type: list[(str, dict)]"""

    # -- Fill in context --
    ctx = tools.context.load_context(flask.request)
    ctx['hostname'] = broker_hostname
    ctx['tasks'] = result

    ctx['current_branch'] = branch

    return flask.render_template('dashboard_app/pending_tasks/index.html', **ctx)

