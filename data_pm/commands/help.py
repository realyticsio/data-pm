#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Display help and the data_pm documentation in a web browser.
"""

# Global imports
import sys
from pkg_resources import Requirement, resource_filename
import subprocess

# Local imports
from data_pm.tools.cli_handler import BaseCommand


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('--browser', default='firefox', help='Path to a web browser')

    def handle(self, namespace):
        filename = resource_filename(Requirement.parse("data_pm"), "data_pm_docs/build/html/index.html")
        subprocess.call([namespace.browser, filename])
