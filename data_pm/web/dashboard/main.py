"""
Extends data_pm.web.api.main application by adding dashboard routes
"""

# Global imports
import flask
import flask_restplus
import os


# Local imports
from ..api.main import app, api
from . import views, data_views

# Set template and static folders
_ROOT = os.path.abspath(os.path.dirname(__file__))
app.template_folder = os.path.join(_ROOT, 'templates')
app.static_folder = os.path.join(_ROOT, 'static')

app.register_blueprint(views.blueprint, url_prefix='/dashboard')
app.register_blueprint(data_views.blueprint, url_prefix='/data')


@api.route('/')
class Root(flask_restplus.Resource):
    def get(self):
        return flask.redirect('dashboard')


import datetime
@app.template_filter('strftime')
def strftime(value, format='%Y-%m-%d %H:%M:%S'):
    if value is None:
        return 'N/A'
    else:
        return datetime.datetime.utcfromtimestamp(value).strftime(format)

