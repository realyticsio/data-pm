# Global imports
import os
import shutil
import time
import contextlib
import math


# Local imports
from data_pm import settings
from . import base_driver


_default_driver = None


class Driver(base_driver.BaseDriver):

    name = 'file'

    def __eq__(self, other):
        # File drivers are all the same
        return True

    def __hash__(self):
        return 1

    @staticmethod
    def get_default():
        global _default_driver
        if _default_driver is None:
            _default_driver = Driver()  # Instantiate with global settings
        return _default_driver

    def TempFile(self, prefix='tmp', suffix='', dir=None, destination=None, create=False):
        return super(self.__class__, self).TempFile(
            prefix=prefix,
            suffix=suffix,
            dir=dir if dir is not None else self.settings['data_pm.driver.file.tmpdir'],
            destination=destination,
            create=create
        )

    def TempDir(self, prefix='tmp', suffix='', dir=None, destination=None, create=False):
        return super(self.__class__, self).TempDir(
            prefix=prefix,
            suffix=suffix,
            dir=dir if dir is not None else self.settings['data_pm.driver.file.tmpdir'],
            destination=destination,
            create=create
        )

    def abspath(self, path):
        return os.path.abspath(path)

    def basename(self, path):
        return os.path.basename(path)

    def dirname(self, path):
        return os.path.dirname(path)

    def exists(self, path):
        return os.path.exists(path)

    def getmtime(self, path):
        if os.path.exists(path):
            return math.ceil(os.path.getmtime(path) * 1000.) / 1000.
        else:
            return None

    def getsize(self, path):
        return os.path.getsize(path)

    def isdir(self, path):
        return os.path.isdir(path)

    def isfile(self, path):
        return os.path.isfile(path)

    def join(self, arg, *args):
        return os.path.join(arg, *args)

    def listdir(self, path):
        return os.listdir(path)

    def list_statuses(self, path, recursive=False):
        if not self.exists(path):
            raise OSError('"{}" does not exist'.format(path))

        is_directory = self.isdir(path)
        children_num = len(self.listdir(path)) if is_directory else 0

        yield base_driver.FileStatus(
            absolute_path=path,
            relative_path='',
            is_directory=is_directory,
            mtime=self.getmtime(path),
            size=0 if is_directory else self.getsize(path),
            children_num=children_num
        )

        def _list_statuses_rec(_path, _recursive):
            dirs = []
            for name in self.listdir(_path):
                absolute_path = self.join(_path, name)
                _is_directory = self.isdir(absolute_path)
                _children_num = len(self.listdir(_path)) if _is_directory else 0

                yield base_driver.FileStatus(
                    absolute_path=absolute_path,
                    relative_path=name,
                    is_directory=_is_directory,
                    mtime=self.getmtime(absolute_path),
                    size=0 if _is_directory else self.getsize(absolute_path),
                    children_num=_children_num
                )

                if _is_directory:
                    dirs.append(absolute_path)

            if _recursive:
                for absolute_path in dirs:
                    for _file_status in _list_statuses_rec(absolute_path, _recursive=True):
                        assert _file_status.absolute_path.startswith(_path)
                        _file_status.relative_path = _file_status.absolute_path[len(_path):].strip(os.sep)
                        yield _file_status

        if is_directory:
            for file_status in _list_statuses_rec(path, recursive):
                yield file_status

    def makedirs(self, path):
        if not self.exists(path):
            os.makedirs(path)

    @staticmethod
    def parse_config(url):
        return dict()

    @contextlib.contextmanager
    def read(self, path):
        with open(path, 'rb') as fd:
            yield fd

    def remove(self, path, recursive=False):
        if self.isfile(path):
            os.remove(path)
        else:
            if recursive:
                shutil.rmtree(path)
            else:
                os.rmdir(path)

    def rename(self, src, target):
        shutil.move(src, target)

    def renames(self, src, target):
        if not os.path.exists(os.path.dirname(target)):
            os.makedirs(os.path.dirname(target))

        shutil.move(src, target)

    def setmtime(self, path, mtime=None):
        if mtime is None:
            mtime = time.time()

        os.utime(path, (mtime, mtime))

    def walk(self, path):
        return os.walk(path)

    @contextlib.contextmanager
    def write(self, path):
        with open(path, 'wb') as fd:
            yield fd

