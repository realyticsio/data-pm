# Global imports
import data_pm
import shutil
import git
import subprocess
import os

# Local imports
from data_pm import settings


def snapshot_from_directory(source, destination):
    """
    Take a snapshot from a directory

    :param str source: Directory to take the snapshot from
    :param str destination: Snapshot path (must not exist)
    """
    check_package_list()

    if data_pm.file.exists(destination):
        raise ValueError(u'Snapshot destination "{}" already exist'.format(destination))

    for path in settings['data_pm.package.list']:
        copy(data_pm.file.join(source, path), data_pm.file.join(destination, path))

    return settings['data_pm.package.list']


def snapshot_from_branch(branch, destination):
    """
    Take a snapshot from a specific git branch

    :param str branch: git branch to take the snapshot from
    :param str destination: Snapshot path (must not exist)
    """
    check_package_list()

    data_pm.file.makedirs(destination)

    # Export the whole branch to the destination
    subprocess.check_call('git archive --format=tar {} ./ | tar -xC {}'.format(branch, destination), shell=True)

    # Remove unnecessary files and folders
    for path in data_pm.file.listdir(destination):
        if path not in data_pm.settings['data_pm.package.list']:
            data_pm.file.remove(data_pm.file.join(destination, path), recursive=True)

    r = git.Repo('.')

    # List unversioned files
    l_unversioned = r.git.clean(dry_run=True, d=True, x=True).splitlines()

    # Check that all the lines start with "Would remove "
    assert all([l.startswith('Would remove ') for l in l_unversioned])

    # Remove "Would remove " prefix from each line
    l_unversioned = [l.replace('Would remove ', '') for l in l_unversioned]

    # Remove .pyc and __pycache__ files
    l_unversioned = [l for l in l_unversioned if not l.endswith('.pyc') and not '__pycache__' in l]

    # Keep only unversioned files or folders that are specified in the package list
    l_unversioned = [
        path for path in l_unversioned if any(
            path == allowed_path or path.startswith(allowed_path.strip('/') + '/')
            for allowed_path in ['settings']
        )]

    # Copy unversioned files
    for path in l_unversioned:
        copy(path, data_pm.file.join(destination, path))

    return l_unversioned


def check_package_list():
    """
    Check that the setting data_pm.package.list is correct
    """
    problematic_paths = [p for p in data_pm.settings['data_pm.package.list'] if os.path.sep in p]

    if problematic_paths:
        raise ValueError(u'data_pm.package.list must not contain any path separator (found: {})'
                         .format(problematic_paths))


def copy(src, dst):
    # Create parent directory
    dst_parent_dir = data_pm.file.dirname(dst.rstrip(os.path.sep))
    if not data_pm.file.exists(dst_parent_dir):
        data_pm.file.makedirs(dst_parent_dir)

    if data_pm.file.isdir(src):
        shutil.copytree(src, dst, ignore=shutil.ignore_patterns('*.pyc'))
    else:
        shutil.copy(src, dst)
