from urllib3.util.retry import Retry
from requests import Session
from requests.adapters import HTTPAdapter

from data_pm import settings


class Client(object):
    """
    Communication with data-pm API.
    Implements a retry mechanism thanks to an HttpAdapter.
    """
    def __init__(self):
        api_url = settings['data_pm.api.url'].strip('/')

        self.url_debug = api_url + '/debug/{}'
        self.url_workspace = api_url + '/workspace/{}'
        self.url_log = api_url + '/log/'
        self.url_status = api_url + '/status/'
        self.url_last_update_query = api_url + '/last_update/query'
        self.url_last_update_nullify = api_url + '/last_update/nullify'
        self.url_stream_stdout = api_url + '/log/stream/stdout/{}'
        self.url_stream_stderr = api_url + '/log/stream/stderr/{}'
        self.url_environment = api_url + '/environment/'
        self.url_view = api_url + '/data_view/'

        self.s = Session()
        self.s.mount(
            prefix=api_url,
            adapter=HTTPAdapter(
                max_retries=Retry(
                    total=5,
                    backoff_factor=0.2,
                    method_whitelist=['GET', 'POST', 'PUT', 'DELETE']
                )
            )
        )

    def get_stream_stdout(self, task_id, position):
        return self.s.get(
            url=self.url_stream_stdout.format(task_id),
            json={'pos': position}
        )

    def post_stream_stdout(self, task_id, message):
        return self.s.post(
            url=self.url_stream_stdout.format(task_id),
            data=message
        )

    def delete_stream_stdout(self, task_id):
        return self.s.delete(
            url=self.url_stream_stdout.format(task_id)
        )

    def get_stream_stderr(self, task_id, position):
        return self.s.get(
            url=self.url_stream_stderr.format(task_id),
            json={'pos': position}
        )

    def post_stream_stderr(self, task_id, message):
        return self.s.post(
            url=self.url_stream_stderr.format(task_id),
            data=message
        )

    def delete_stream_stderr(self, task_id):
        return self.s.delete(
            url=self.url_stream_stderr.format(task_id)
        )

    def get_debug(self, task_id):
        return self.s.get(
            url=self.url_debug.format(task_id)
        )

    def post_debug(self, task_id, dictionary):
        return self.s.post(
            url=self.url_debug.format(task_id),
            json=dictionary
        )

    def delete_debug(self, task_id):
        return self.s.delete(
            url=self.url_debug.format(task_id)
        )

    def get_workspace(self, task_id):
        return self.s.get(
            url=self.url_workspace.format(task_id)
        )

    def put_workspace(self, task_id, fd):
        return self.s.put(
            url=self.url_workspace.format(task_id),
            files=[('file', fd)]
        )

    def delete_workspace(self, task_id):
        return self.s.delete(
            url=self.url_workspace.format(task_id),
        )

    # def get_view(self, params):
    #     return self.s.get(
    #         url=self.url_view,
    #         params=params
    #     )

    def put_view(self, dictionary, fd):
        return self.s.put(
            url=self.url_view,
            data=dictionary,
            files=[('html_file', fd)]
        )

    # def delete_view(self, dictionary):
    #     return self.s.delete(
    #         url=self.url_view,
    #         json=dictionary
    #     )

    def post_log(self, dictionary):
        return self.s.post(
            url=self.url_log,
            json=dictionary
        )

    def get_status(self, params):
        return self.s.get(
            url=self.url_status,
            params=params
        )

    def post_last_update_query(self, dictionary):
        return self.s.post(
            url=self.url_last_update_query,
            json=dictionary
        )

    def post_last_update_nullify(self, dictionary):
        return self.s.post(
            url=self.url_last_update_nullify,
            json=dictionary
        )

    def post_environment(self, hostname, requirements_hash):
        return self.s.post(
            url=self.url_environment,
            json={
                'hostname': hostname,
                'hash_code': requirements_hash
            }
        )


client = Client()
