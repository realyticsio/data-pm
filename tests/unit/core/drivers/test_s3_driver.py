# Global imports
import unittest
import time

# Local imports
from data_pm.core.drivers import s3_driver
from data_pm import settings


# Customize this line for other drivers
Driver = s3_driver.Driver


class TestDriver(unittest.TestCase):
    def setUp(self):
        settings.load()

        self.driver = Driver.get_default()

        self.path = 's3://realytics-tmp/tmp/data-pm-test'

        # Do some cleaning first
        if self.driver.exists(self.path):
            self.driver.remove(self.path, recursive=True)

    def tearDown(self):
        # Clean path
        if self.driver.exists(self.path):
            self.driver.remove(self.path, recursive=True)

    def test_path_manipulation(self):
        path = 's3://realytics-tmp/tmp/data-pm-test/a/b.txt'

        self.assertEqual(self.driver.join(self.driver.dirname(path), self.driver.basename(path)), path)

    def test_directory(self):
        # The path should NOT exist
        self.assertFalse(self.driver.exists(self.path))

        # Create the directory
        self.driver.makedirs(self.path)

        # The path should exist
        self.assertTrue(self.driver.exists(self.path))
        #self.assertFalse(self.driver.isfile(self.path))
        #self.assertTrue(self.driver.isdir(self.path))

        # Remove the directory
        self.driver.remove(self.path, recursive=True)

        # The path should NOT exist again
        self.assertFalse(self.driver.exists(self.path))

    def test_read_write(self):
        with self.driver.write(self.path) as writer:
            writer.write(b'hello world')

        # The path should exist
        self.assertTrue(self.driver.exists(self.path))
        self.assertTrue(self.driver.isfile(self.path))
        #self.assertFalse(self.driver.isdir(self.path))

        with self.driver.read(self.path) as reader:
            self.assertEqual(reader.read(), b'hello world')

        # Test getsize()
        self.assertEqual(self.driver.getsize(self.path), len(b'hello world'))

    def test_mtime(self):
        # Create an empty file
        with self.driver.write(self.path) as _:
            pass

        mtime = self.driver.getmtime(self.path)
        # The modification time of the file should be close to the local time
        self.assertAlmostEqual(mtime, time.time(), delta=5)

        # Set the modification time of the file to 2016-04-04 10:10:00
        #self.driver.setmtime(self.path, 1459764600)

        #mtime = self.driver.getmtime(self.path)
        # The modification time of the file should be close to the one specified
        #self.assertAlmostEqual(mtime, 1459764600, delta=1)


class TestTempFile(unittest.TestCase):
    def setUp(self):
        settings.load()

        self.destination = 's3://realytics-tmp/tmp/data-pm-test/a/b.txt'
        self.driver = Driver.get_default()
        self.tmp_file = self.driver.TempFile(destination=self.destination)

    def tearDown(self):
        self.tmp_file.remove()

        if self.driver.exists(self.destination):
            self.driver.remove(self.destination)

    def test_move_to_destination(self):
        """
        Test moving a file to its destination

        """
        # Tmp file does not exist yet
        self.assertFalse(self.driver.exists(self.tmp_file.path))

        # Write into tmp file
        with self.driver.write(self.tmp_file.path) as writer:
            writer.write(b'hello world\n')

        tmp_path = self.tmp_file.path

        # Tmp file exists, destination does not exist
        self.assertTrue(self.driver.exists(tmp_path))
        self.assertFalse(self.driver.exists(self.destination))

        # Perform the move to destination
        self.tmp_file.move_to_destination()

        # Tmp file does exist anymore, destination file exists
        self.assertFalse(self.driver.exists(tmp_path))
        self.assertTrue(self.driver.exists(self.destination))
        self.assertIsNone(self.tmp_file.path)

        # Destination file contains the text inserted into tmp file
        with self.driver.read(self.destination) as reader:
            self.assertEqual(reader.read(), b'hello world\n')

    def test_move_to_destination_is_present(self):
        """
        Test moving a file to its destination when the destination already exists

        """
        # Write into tmp file
        with self.driver.write(self.tmp_file.path) as writer:
            writer.write(b'hello world\n')

        # Write into destination file
        with self.driver.write(self.destination) as writer:
            writer.write(b'bonjour monde\n')

        # Perform the move to destination
        self.tmp_file.move_to_destination()

        # Destination file contains the text inserted into tmp file
        with self.driver.read(self.destination) as reader:
            self.assertEqual(reader.read(), b'hello world\n')

    def test_remove(self):
        """
        Ensure tmp file is actually removed with remove()
        """
        tmp_path = self.tmp_file.path

        with self.driver.write(tmp_path) as writer:
            writer.write(b'hello world')

        self.tmp_file.remove()

        self.assertFalse(self.driver.exists(tmp_path))

        self.assertIsNone(self.tmp_file.path)


class TestTempDir(unittest.TestCase):
    def setUp(self):
        settings.load()

        self.destination = 's3://realytics-tmp/tmp/data-pm-test/a/b'
        self.driver = Driver.get_default()
        self.tmp_file = self.driver.TempDir(destination=self.destination)

    def tearDown(self):
        self.tmp_file.remove()

        if self.driver.exists(self.destination):
            self.driver.remove(self.destination, recursive=True)

    def test_move_to_destination(self):
        # Create a subdir named "subdir" into tmp dir
        self.driver.makedirs(self.driver.join(self.tmp_file.path, 'subdir'))

        # Create a file named file into tmp dir
        with self.driver.write(self.driver.join(self.tmp_file.path, 'file')) as _:
            pass

        tmp_path = self.tmp_file.path

        # Destination does not exist yet
        self.assertFalse(self.driver.exists(self.destination))

        # Perform move to destination
        self.tmp_file.move_to_destination()

        self.assertFalse(self.driver.exists(tmp_path))
        self.assertTrue(self.driver.exists(self.destination))
        self.assertIsNone(self.tmp_file.path)

        # We should find subdir and file in the destination dir
        self.assertEqual(sorted(self.driver.listdir(self.destination)), ['file', 'subdir'])

    def test_move_content_to_destination_is_present(self):
        # Create destination dir and a subdir, and write into a file
        self.driver.makedirs(self.destination)
        self.driver.makedirs(self.driver.join(self.destination, 'a_subdir'))
        with self.driver.write(self.driver.join(self.destination, 'file')) as writer:
            writer.write(b'bonjour monde\n')

        # Create a subdir named "subdir" into tmp dir
        self.driver.makedirs(self.driver.join(self.tmp_file.path, 'another_subdir'))

        # Create a file named file into tmp dir
        with self.driver.write(self.driver.join(self.tmp_file.path, 'file')) as writer:
            writer.write(b'hello world\n')

        # Perform move to destination
        self.tmp_file.move_content_to_destination()

        # Check "file" has been uploaded
        with self.driver.read(self.driver.join(self.destination, 'file')) as reader:
            self.assertEqual(reader.read(), b'hello world\n')

        # We should find subdir and file in the destination dir
        self.assertEqual(sorted(self.driver.listdir(self.destination)), ['a_subdir', 'another_subdir', 'file'])

    def test_remove(self):
        """
        Ensure tmp file is actually removed with remove()
        """
        tmp_path = self.tmp_file.path

        self.tmp_file.remove()

        self.assertFalse(self.driver.exists(tmp_path))
        self.assertIsNone(self.tmp_file.path)

