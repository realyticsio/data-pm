# Global imports
import networkx as nx

# Local imports
from data_pm.core.factory import get_factory


class DataNode(object):
    def __init__(self, study_name, is_shared=False):
        self.study_name = study_name
        self.is_shared = is_shared
        self.branches_in = {}
        self.branch_out = None
        self.max_depth = None
        self.out_of_date = set()
        self.forced_by_cli = set()
        self.studies = set()

    def __repr__(self):
        return '<DataNode {}>'.format(self.study_name)

    def set_branch_in(self, key, value):
        self.branches_in[key] = value
        return self

    def set_branch_out(self, branch_out):
        self.branch_out = branch_out
        return self

    def set_max_depth(self, max_depth):
        self.max_depth = max_depth
        return self

    def add_out_of_date(self, project):
        self.out_of_date.add(project)
        return self

    def all_out_of_date(self):
        self.out_of_date = {s.project_name for s in self.studies}
        return self

    def add_forced_by_cli(self, project):
        self.forced_by_cli.add(project)
        return self

    def all_forced_by_cli(self):
        self.forced_by_cli = {s.project_name for s in self.studies}
        return self

    def add_study(self, study_ctrl):
        self.studies.add(study_ctrl)
        return self

    def iter_studies(self):
        if self.is_shared:
            yield


class BaseDAG(object):
    """Abstract class for dependencies.
    DAG stands for Directed Acyclic Graph."""

    def __init__(self, cli_studies, min_depth_force=0, max_depth_check=float('inf'), ignore_parent=False):
        branch_config = get_factory().create_branch_configuration()
        self.d_branches = branch_config.get_hierarchy()
        self.cli_studies = cli_studies
        self.max_depth_check = max_depth_check
        self.min_depth_force = min_depth_force
        self.ignore_parent = ignore_parent
        self.G = nx.DiGraph()

    def populate(self, study):
        """
        Recursive function that will go up through study inputs
        """
        raise NotImplementedError()

    def spread_branches(self):
        """
        Propagates branches according to the branch configuration.
        """

        # reversed: apply flow with parents first
        for branch_name, study_names in reversed(self.d_branches.items()):
            if not study_names:
                sources = get_sources_list(self.G)
            else:
                sources = [n for n in self.G.nodes() if n in study_names]

            if len(sources) == 0:
                continue

            for src in sources:
                self.G.nodes[src]['datanode'].set_branch_out(branch_name)

            edges = list(nx.edge_dfs(self.G, sources))

            for u, v in edges:
                self.G.nodes[v]['datanode'].set_branch_in(u, branch_name)
                self.G.nodes[v]['datanode'].set_branch_out(branch_name)

        return self.G

    def to_dot(self, d_colors):
        dot = 'digraph graphname {\n'
        dot += '\n'.join(self.nodes_to_dot(self.G, d_colors)) + '\n'
        dot += '\n'.join(self.edges_to_dot(self.G, d_colors)) + '\n'
        dot += '}'
        return dot

    @staticmethod
    def edges_to_dot(graph, d_colors):
        """Generates edges to dot format"""

        for src, dst in graph.edges():
            branch_out = graph.nodes[src]['datanode'].branch_out

            dot_edge = '"{}" -> "{}" [color={}]' \
                .format(src, dst, d_colors[branch_out])

            yield dot_edge

    @staticmethod
    def nodes_to_dot(graph, d_colors):
        """Generates nodes to dot format"""

        for node, data in graph.nodes.items():
            branch_out = data['datanode'].branch_out
            is_shared = data['datanode'].is_shared

            shape = 'box' if is_shared else 'ellipse'
            dot_node = '"{}" [color={}] [shape={}]' \
                .format(node, d_colors[branch_out], shape)

            yield dot_node


def get_sources_list(g):
    """
    Return nodes without incoming edge
    """
    return [n for n in g.nodes() if g.in_degree(n) == 0]
