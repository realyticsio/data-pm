# Global imports
import re
import time
import contextlib

import boto3
import botocore
import botocore.exceptions
import posixpath
import datetime
import mimetypes

# Local imports
from data_pm import settings
from . import base_driver, file_driver


_default_driver = None


class Driver(base_driver.BaseDriver):

    name = 's3'

    def __init__(self, _settings, profile_name='default'):
        """
        S3 Driver

        Warning: as there is no concept of directory in S3, the following functions will have a special behavior:
        - makedirs(): creates an empty file, but does not actually create intermediate directories
        - rename(): acts as renames(), not caring about the existence of intermediate directories

        """

        super(self.__class__, self).__init__(_settings)

        # profiles are specified by default in ~/.aws/credentials
        session = boto3.session.Session(profile_name=profile_name)
        self.s3 = session.resource('s3')

    @staticmethod
    def get_default():
        global _default_driver
        if _default_driver is None:
            _default_driver = Driver(settings)
        return _default_driver

    @staticmethod
    def create(profile_name):
        return Driver(settings, profile_name)

    def TempFile(self, prefix='tmp', suffix='', dir=None, destination=None, create=False):
        bucket = None

        if dir is not None:
            bucket, _ = self._check_url(dir)

        if bucket is None and destination is not None:
            bucket, _ = self._check_url(destination)

        if bucket is None:
            raise ValueError('Either dir or destination must be set to guess a S3 bucket')

        return super(self.__class__, self).TempFile(
            prefix=prefix,
            suffix=suffix,
            dir=build_s3_path(bucket, self.settings['data_pm.driver.s3.tmpdir']),
            destination=destination,
            create=create
        )

    def TempDir(self, prefix='tmp', suffix='', dir=None, destination=None, create=False):
        bucket = None

        if dir is not None:
            bucket, _ = self._check_url(dir)

        if bucket is None and destination is not None:
            bucket, _ = self._check_url(destination)

        if bucket is None:
            raise ValueError('Either dir or destination must be set to guess a S3 bucket')

        return super(self.__class__, self).TempDir(
            prefix=prefix,
            suffix=suffix,
            dir=build_s3_path(bucket, self.settings['data_pm.driver.s3.tmpdir']),
            destination=destination,
            create=create
        )

    def _check_url(self, url):
        """
        Check that the url contains the bucket name

        :param url:
        :return: (bucket, path)
        """
        bucket, path = self.parse(url)

        if bucket is None:
            raise ValueError('Bucket is not specified in url "{}"'.format(url))

        return bucket, path

    def abspath(self, url):
        bucket, path = self.parse(url)
        return build_s3_path(bucket, path)

    def basename(self, url):
        bucket, path = self.parse(url)
        basename = posixpath.basename(path)
        return basename

    def build_url(self, bucket, path):
        """
        Build an S3 url from the bucket name and the path

        Example:
          - Input : build_url('realytics-events', 'dir/file.txt')
            Output: 's3://realytics-events/dir/file.txt'

        Raises if bucket is None

        :param str bucket:
        :param str path:
        :return:
        """
        if bucket is None:
            raise ValueError('Can not build a S3 path without a bucket (path="{}")'.format(path))
        else:
            return build_s3_path(bucket, path)

    def dirname(self, url):
        bucket, path = self.parse(url)
        dirname = posixpath.dirname(path)
        return build_s3_path(bucket, dirname)

    def download(self, url, local_path):
        """
        Download an object from S3 to the local filesystem

        :param url: S3 url
        :param local_path: local path
        """
        self.get_object(url).download_file(local_path)

    def exists(self, url):
        obj = self.get_object(url)
        if check_object_exists(obj):
            return True
        else:
            files = self.listdir(url, absent_ok=True)
            if len(files) > 0:
                return True
            else:
                return False

    def get_bucket(self, url):
        """
        Get an S3 object from an url

        Examples:
          - get_bucket('s3://my-bucket/')
          - get_bucket('my-bucket')

        :param url:
        :rtype: boto3.resources.factory.s3.Bucket
        """
        bucket, path = self.parse(url)

        # If the bucket is not specified, use the path as the bucket name
        if bucket is None:
            bucket = path

        return self.s3.Bucket(bucket)

    def get_object(self, url):
        """
        Get an S3 object from an url

        :param url:
        :rtype: boto3.resources.factory.s3.Object
        """
        bucket, path = self._check_url(url)
        if len(path.strip('/')) == 0:
            raise ValueError('Can not get object /')

        obj = self.s3.Object(bucket, path)
        return obj

    def getmtime(self, url):
        obj = self.get_object(url)

        if not check_object_exists(obj):
            return None
        else:
            tz = obj.last_modified.tzinfo
            mtime = (obj.last_modified - datetime.datetime.fromtimestamp(0, tz=tz)).total_seconds()

            return mtime

    def getsize(self, url):
        obj = self.get_object(url)
        return obj.content_length

    def isdir(self, url):
        raise NotImplementedError('There is no concept of directory in S3')

    def isfile(self, url):
        """
        Checks whether the url points to an existing S3 object.

        Be careful, as a directory may also be an empty file on S3.

        :param url:
        :rtype: bool
        """
        obj = self.get_object(url)
        return check_object_exists(obj)

    def join(self, arg, *args):
        bucket, path = self.parse(arg)
        ret = posixpath.join(path, *args)
        return build_s3_path(bucket, ret)

    def listdir(self, url, absent_ok=False):
        bucket, path = self._check_url(url)
        b = self.get_bucket(url)

        prefix = path + '/' if len(path) > 0 else ''

        dirs = []
        files = []

        paginator = b.meta.client.get_paginator('list_objects_v2')
        for result in paginator.paginate(Bucket=b.name, Prefix=prefix, Delimiter='/'):
            dirs += [self.basename(p['Prefix'].rstrip('/')) for p in result.get('CommonPrefixes', [])]
            files += [self.basename(c['Key']) for c in result.get('Contents', []) if not c['Key'].endswith('/')]

        ret = files + dirs

        if len(ret) == 0 and not absent_ok:
            if not check_object_exists(self.get_object(url)):
                raise OSError('No such S3 directory: "{}"'.format(url))

        return ret

    def list_statuses(self, url, recursive=False):
        # Example url: s3://my-bucket/p1/p2
        if not self.exists(url):
            raise Exception('"{}" does not exist'.format(url))

        is_file = self.isfile(url)
        children_num = 0 if is_file else len(self.listdir(url))

        yield base_driver.FileStatus(
            absolute_path=self.abspath(url),
            relative_path='',
            is_directory=not is_file,
            mtime=self.getmtime(url),
            size=0 if not is_file else self.getsize(url),
            children_num=children_num
        )

        def _list_statuses_rec(_url):
            bucket, path = self._check_url(_url)
            b = self.get_bucket(_url)

            prefix = path + '/' if len(path) > 0 else ''
            # Example prefix: p1/p2/

            paginator = b.meta.client.get_paginator('list_objects_v2')
            dirs = []
            for result in paginator.paginate(Bucket=b.name, Prefix=prefix, Delimiter='/'):
                # List directories
                for cp in result.get('CommonPrefixes', []):
                    absolute_prefix = cp['Prefix']
                    # Example absolute_prefix: p1/p2/p3/

                    assert absolute_prefix.startswith(prefix)

                    absolute_path = self.build_url(b.name, cp['Prefix'].rstrip('/'))
                    # Example absolute_path: s3://my-bucket/p1/p2/p3

                    relative_path = absolute_prefix[len(prefix):].strip('/')
                    # Example relative_path: p3

                    yield FileStatus(
                        absolute_path=absolute_path,
                        relative_path=relative_path,
                        is_directory=True,
                        mtime=None,
                        size=0,
                        children_num=len(result.get('CommonPrefixes', []))
                    )

                    dirs.append(absolute_path)

                # List files
                for c in result.get('Contents', []):
                    absolute_key = c['Key']
                    # Example absolute_key: p1/p2/p3/file.txt

                    assert absolute_key.startswith(prefix)

                    absolute_path = self.build_url(b.name, c['Key'].rstrip('/'))
                    # Example absolute_path: s3://my-bucket/p1/p2/p3/file.txt

                    relative_path = absolute_key[len(prefix):].strip('/')
                    # Example relative_path: p3/file.txt

                    tz = c['LastModified'].tzinfo
                    mtime = (c['LastModified'] - datetime.datetime.fromtimestamp(0, tz=tz)).total_seconds()

                    yield base_driver.FileStatus(
                        absolute_path=absolute_path,
                        relative_path=relative_path,
                        is_directory=False,
                        mtime=mtime,
                        size=c['Size'],
                        children_num=0
                    )

                # Descend in every sub-directory from the current url
                if recursive:
                    for absolute_path in dirs:
                        for _file_status in _list_statuses_rec(absolute_path):
                            # Make the file status relative to the current url
                            assert _file_status.absolute_path.startswith(_url)
                            _file_status.relative_path = _file_status.absolute_path[len(_url):].strip('/')
                            yield _file_status

        if not is_file:
            for file_status in _list_statuses_rec(url):
                yield file_status

    def makedirs(self, url):
        # Nothing to do, we don't create directory for s3, as this is an object storage and not a filesystem.
        pass

    def parse(self, url):
        """
        Parses a S3 url to extract the bucket name and the path

        Examples:
          - Input:  parse("s3://realytics-events/path/file.txt")
            Output: ('realytics-events', 'path/file.txt')
          - Input:  parse("path/file.txt")
            Output: (None, 'path/file.txt')
          - Input:  parse("/path/file.txt")
            Output: (None, 'path/file.txt')
          - Input:  parse("s3://realytics-events")
            Output: ('realytics-events', '')

        :param url:
        :return: (bucket_name, path)
        """

        if '://' not in url:
            # The URL is just path
            path = url
            bucket_name = None
        else:
            # Separate the scheme from the rest of the url
            scheme, rest = url.split('://', 1)
            assert scheme.startswith('s3'), 'Url "{}" does not start with "s3"'.format(url)

            # Check if the rest of the url contains a path
            if '/' in rest:
                # Separate the bucket from the path
                bucket_name, path = rest.split('/', 1)
            else:
                # There is no path, the rest of the url is the bucket name
                bucket_name = rest
                path = ''

        return bucket_name, path.strip('/')

    @staticmethod
    def parse_config(url):
        pattern = 's3(a|n){,1}://(?P<bucket>(\w|-)+)/?.*'
        m = re.search(pattern, url)
        if m is not None:
            return {
                'data_pm.driver.s3.bucket': m.groupdict()['bucket']
            }

    @contextlib.contextmanager
    def read(self, url):
        obj = self.get_object(url)
        yield obj.get()['Body']

    def remove(self, url, recursive=False, absent_ok=False):
        """
        Remove a S3 file or "directory"

        Will raise OSError if this S3 file does not exist or if there is no file under this S3 "directory"

        :param url:
        :param recursive:
        :param absent_ok: do not raise if the file does not exist
        :return: list of deleted paths
        :rtype: list[str]
        """
        bucket, path = self._check_url(url)
        obj = self.get_object(url)
        object_exists = check_object_exists(obj)

        l_removed = []

        # Delete the object
        if object_exists:
            obj.delete()
            l_removed.append(path)

        # Now walk through the directories to find underneath objects
        l_to_remove = []
        for dirpath, dirnames, filenames in self.walk(url):
            l_to_remove += [self.join(dirpath, filename) for filename in filenames]

        if len(l_to_remove) > 0 and not recursive:
            raise OSError('S3 "directory" not empty: "{}"'.format(url))

        if not absent_ok and not object_exists and len(l_to_remove) == 0:
            raise OSError('No such S3 file or directory: "{}"'.format(url))

        # Actually delete underneath objects
        while len(l_to_remove) > 0:
            objects = {'Objects': [{'Key': p} for p in l_to_remove[:1000]]}
            self.s3.meta.client.delete_objects(Bucket=bucket, Delete=objects)
            l_removed += l_to_remove[:1000]
            l_to_remove = l_to_remove[1000:]

        return l_removed

    def rename(self, src, target):
        # Always allow creating intermediate directories
        self.renames(src, target)

    def renames(self, src, target):
        src_bucket, src_path = self.parse(src)
        target_bucket, target_path = self.parse(target)

        obj = self.get_object(src)

        object_exists = check_object_exists(obj)

        # Move the object "src" if it is a file
        if object_exists:
            # Move the object
            self.s3.Object(target_bucket, target_path).copy_from(CopySource=self.join(src_bucket, src_path),
                                                                 StorageClass='INTELLIGENT_TIERING')
            self.s3.Object(src_bucket, src_path).delete()

        # Move the files underneath if it is a "directory"
        l_to_copy = []
        for dirpath, dirnames, filenames in self.walk(src):
            l_to_copy += [self.join(dirpath, filename) for filename in filenames]

        if len(l_to_copy) == 0 and not object_exists:
            raise ValueError('No such S3 file or directory: "{}"'.format(src))

        for path in l_to_copy:
            t = self.join(target_path, path[len(src_path):].strip('/'))
            self.s3.Object(target_bucket, t).copy_from(CopySource=self.join(src_bucket, path),
                                                       StorageClass='INTELLIGENT_TIERING')
            self.s3.Object(src_bucket, path).delete()

    def setmtime(self, path, mtime=None):
        if mtime is None:
            mtime = time.time()

        #os.utime(path, (mtime, mtime))

    def upload(self, local_path, url):
        """
        Upload an object from the local filesystem to S3

        :param local_path: local path
        :param url: S3 url
        """
        extra_args = {'StorageClass': 'INTELLIGENT_TIERING'}

        # Attempt detecting mime type
        guessed_mime_type = mimetypes.guess_type(self.basename(url))[0]
        if guessed_mime_type:
            extra_args['ContentType'] = guessed_mime_type

        # Acutally upload file
        self.get_object(url).upload_file(local_path, ExtraArgs=extra_args)

    def walk(self, url):
        bucket, path = self._check_url(url)
        b = self.get_bucket(url)

        prefix = path + '/' if len(path) > 0 else ''

        paginator = b.meta.client.get_paginator('list_objects_v2')
        for result in paginator.paginate(Bucket=b.name, Prefix=prefix, Delimiter='/'):
            dirs = [self.basename(p['Prefix'].rstrip('/')) for p in result.get('CommonPrefixes', [])]
            files = [self.basename(c['Key']) for c in result.get('Contents', [])]

            yield path, dirs, files

            for subdir in dirs:
                for tup in self.walk(self.join(url, subdir)):
                    yield tup

    @contextlib.contextmanager
    def write(self, url):
        """
        This method writes bytes to a local temporary, and uploads to S3 when the with clause ends

        :param url:
        :return:
        """

        tmp_file = file_driver.Driver.get_default().TempFile()

        try:
            with open(tmp_file.path, 'wb') as fd:
                yield fd

            with open(tmp_file.path, 'rb') as fd:
                obj = self.get_object(url)
                obj.put(Body=fd, StorageClass='INTELLIGENT_TIERING')

        finally:
            tmp_file.remove()

    def get_limited_access_url(self, url: str, expiration_in_seconds: int) -> str:
        """
        Get an url that is only valid for a limited time in seconds.
        This should be generally used when wanting to give a limited access to an external user,
        while keeping the resource private.
        """
        bucket_name, object_name = self.parse(url)
        return self.s3.meta.client.generate_presigned_url(
            'get_object',
            Params={
                'Bucket': bucket_name,
                'Key': object_name
            },
            ExpiresIn=expiration_in_seconds
        )


def build_s3_path(bucket, path):
    """

    :param str bucket:
    :param str path:
    :return:
    """
    if bucket is None:
        return path.strip('/')
        #raise ValueError('Can not build a S3 path without a bucket (path="{}")'.format(path))
    else:
        return 's3://{}/{}'.format(bucket, path.lstrip('/'))


def check_object_exists(obj):
    """
    Returns True if the object exists

    :param obj: S3 Object
    """
    try:
        obj.load()
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            exists = False
        else:
            raise
    else:
        exists = True

    return exists


class FileStatus(base_driver.FileStatus):
    """Special FileStatus that forbids the use of mtime"""

    @property
    def mtime(self):
        raise ValueError('No mtime can be requested on a S3 directory ({})'.format(self.absolute_path))


if __name__ == "__main__":
    s3_driver = Driver({})
    tmp_url = s3_driver.get_limited_access_url("s3://realytics-dtc/creatives/10001.mp4", 60)
    print(tmp_url)
