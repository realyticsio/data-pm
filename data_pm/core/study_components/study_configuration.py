
def require_project_set(func):
    def wrapper(self, *args, **kwargs):
        assert self.study_control.is_shared or self.study_control.project_name is not None, 'Project is not set'
        return func(self, *args, **kwargs)
    return wrapper


def require_output_branch_set(func):
    def wrapper(self, *args, **kwargs):
        assert self.study_control.branch_out is not None, 'Output branch is not set'
        return func(self, *args, **kwargs)
    return wrapper


