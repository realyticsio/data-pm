from data_pm.core import drivers
from data_pm import settings
import requests


all_projects = None


def get_project_names(path=''):
    global all_projects
    if all_projects is None:
        all_projects = _get_project_names(path)
    return all_projects


def _get_project_names(path):
    # Get result from API instead of driver directory listing to allows driver where directory doesn't exist.
    url = "http://api-internal.realytics.net/projects/"
    headers = {
        "Accept-Encoding": "gzip, deflate",
        "Realytics-App-Token": "iqp6aedMnfn87irJ127ptNQFdtzpnEbM"
    }

    r = requests.get(url, headers=headers)

    if r.status_code != 200:
        raise ValueError('Unable to fetch "{}": "{}"'.format(r.url, r.reason))

    return sorted([project['normalizedName'] for project in r.json()['result']])


def clear_cache():
    global all_projects
    all_projects = None
