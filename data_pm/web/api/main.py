# Global imports
import flask
import flask_restplus

# Local imports
from . import views


app = flask.Flask('data-pm')


class Api(flask_restplus.Api):
    def _register_doc(self, app_or_blueprint):
        # HINT: This is just a copy of the original implementation with the last line removed
        if self._add_specs and self._doc:
            # Register documentation before root if enabled
            app_or_blueprint.add_url_rule(self._doc, 'doc', self.render_doc)

    @property
    def base_path(self):
        return ''


api = Api(
    app=app,
    title='Data PM API',
    version='1.0',
    doc='/doc',
)


app.secret_key = 'OIUYTFVBJKIUYTGBNJUYG'

# Register api namespaces
api.add_namespace(views.workspace.ns)
api.add_namespace(views.git.ns)
api.add_namespace(views.log.ns)
api.add_namespace(views.debug.ns)
api.add_namespace(views.status.ns)
api.add_namespace(views.last_update.ns)
api.add_namespace(views.environment.ns)
api.add_namespace(views.task.ns)
api.add_namespace(views.data_view.ns)


# @api.route("/status")
# class Status(flask_restplus.Resource):
#     """
#     Simply return 200 for HAPROXY
#     """
#     def get(self):
#         return {'message': 'ok'}
#
#     def options(self):
#         return {'message': 'ok'}


@app.teardown_appcontext
def shutdown_session(exception=None):
    from .backend import session
    session.close()
