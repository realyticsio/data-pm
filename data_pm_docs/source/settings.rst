********
Settings
********

Settings configure the paths and the behaviour of data_pm. It consists of yaml files that must be loaded at runtime.
To manually load the settings, use::

    from data_pm import settings
    settings.load()

Note that settings are automatically loaded when you launch a :doc:`pm command<commands>`.


There are three levels of settings:

1. data_pm's default settings (hardcoded in python)
2. user's settings (in ``~/.local/data-pm/settings.yml``)
3. your workspace settings (``settings.yml``)


Default settings
****************
.. autoattribute:: data_pm.configuration.settings.DEFAULT_CONF


settings.yml
************

Structure: key->value.

You may import other yaml files as a list of strings in the field "imports".
These files will be loaded at the very END of the current file.

Example file::

    data_pm.tmp.dir: /mnt
    data_pm.logger.level: DEBUG
    imports:
      - /home/chris/my_project/another_settings.yml

