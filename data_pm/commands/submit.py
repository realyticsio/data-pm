#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Remotely launch studies for a set of clients
"""

# Global imports
from __future__ import print_function
import sys
import time
import json
import os
import git
import signal
import getpass
import pandas as pd

# Local imports
import data_pm
from data_pm import settings
from data_pm.core import project_mngr, multitasking, multitasking
from data_pm.core.analysis_tool import AnalysisTool
from data_pm.core.factory import get_factory
from data_pm.tools import git_helper
from data_pm.tools.cli_handler import BaseCommand, refactor
from data_pm.deployment import tasks
from data_pm.deployment.environments import WorkingDirHelper
from data_pm.deployment.monitoring import activate_debug, activate_monitoring
# Import urlparse
try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse


def assert_command_well_built(namespace):
    if namespace.parallel >= 2 and namespace.detach:
        raise ValueError('--detach and --parallel are not supported together.')
    if namespace.parallel <= 1 and namespace.dry:
        raise ValueError('--dry is only available for parallel mode.')


def resolve_origin(input_origin):
    if input_origin is not None:
        origin = input_origin
    else:
        try:
            repo = git.Repo('.')
            origin = git_helper.get_value(repo, 'name') or getpass.getuser()
        except git.InvalidGitRepositoryError:
            origin = getpass.getuser()

    return origin


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('projects_or_studies', nargs='+', metavar='PROJECT OR STUDY')
        parser.add_argument('-p', '--params', dest='params', nargs='*', type=str,
                            help='Parameters in the form myparam=myvalue')

        parser.add_argument('--branch', dest='branch', type=str, default='current',
                            help='Git branch to take the code snapshot from and to write the data to. '
                                 'Defaults to "current": take the snapshot from the current working directory '
                                 '(with unversionned changes) and write the data to the current git branch')

        parser.add_argument('--min-depth-force', type=int, default=0,
                            help='Force compute studies deeper than this value. '
                                 'By default, only force compute the provided studies.')
        parser.add_argument('--max-depth-check', type=int, default=1000,
                            help='Do not check the status of studies deeper than this value. '
                                 'By default, check as deep as possible (1000).')
        parser.add_argument('--ignore-parent', action='store_true',
                            help='Do not check the status of studies of parent branch.')

        parser.add_argument('--parallel', type=int, default=1,
                            help='Run multiple `pm submit` commands in parallel')
        parser.add_argument('--dry', action='store_true',
                            help='If multitasking is engaged, print scheduled tasks.')

        parser.add_argument('--detach', action='store_true',
                            help='Return immediately after submitting the task')
        parser.add_argument('--unlock', action='store_true',
                            help='Do not check if study is already running')
        parser.add_argument('--debug', action='store_true',
                            help='Enter debug mode and run a remote python console')

        parser.add_argument('-Q', '--queue', dest='queue', type=str, default='default',
                            choices=settings['celery.queue.names'],
                            help='Celery queue in which the task will be pushed')
        parser.add_argument('--slack', type=str, default=None,
                            help='Send notifications to the given slack channel')
        parser.add_argument('--origin', type=str, default=None,
                            help='User or host that issues the task')

    def handle(self, namespace):
        assert settings['data_pm.redirection.driver'] != 'file'
        assert_command_well_built(namespace)

        init_path = os.getcwd()

        wd = WorkingDirHelper()
        if namespace.branch == 'current':
            wd.snapshot_from_directory('.')
            namespace.branch = git.Repo('.').active_branch.name
        else:
            wd.snapshot_from_branch(namespace.branch)

        # Load projects before changing cwd
        all_projects = set(project_mngr.get_project_names())

        try:
            os.chdir(wd.path)
            refactor(namespace, all_projects)
            namespace.origin = resolve_origin(namespace.origin)

            if namespace.parallel >= 2:
                return handle_parallel(namespace, wd)
            else:
                try:
                    r = handle_queue(namespace, wd)
                    return r
                except tasks.SubmitError as e:
                    print(e.args[0])
                    tasks.revoke(e.task_id)
                    print('Task revoked')
                    return 1
        finally:
            os.chdir(init_path)
            wd.clear()


def handle_queue(namespace, working_dir):
    t = time.time()
    async_result, n_bytes = AnalysisTool(namespace).submit_studies(
        working_dir=working_dir,
        queue=namespace.queue
    )
    upload_time = time.time() - t
    try:
        broker_uri = async_result.app.broker_connection().as_uri().strip('/')
        parsed_url = urlparse(settings['data_pm.api.url'])
        hostname, port = parsed_url.hostname, parsed_url.port

        print(
            'Task sent to {}\n'
            '  - Flower    : http://{}:5555/task/{}\n'
            '  - Dashboard : http://{}:{}/dashboard/running_studies/branch/{}\n'
            '  - Queue     : {}\n'
            '  - Branch    : {}'
            .format(broker_uri, hostname, async_result.id, hostname, port,
                    namespace.branch, namespace.queue, namespace.branch)
        )

        data_pm.logger.info(
            'Uploaded code: {} kB uploaded in {:.2} seconds'
            .format(int(n_bytes/1000), upload_time)
        )

        if namespace.debug:
            return activate_debug(async_result, namespace)
        elif not namespace.detach:
            rv, msg = activate_monitoring(async_result)
            print(msg)
            return rv

    except KeyboardInterrupt:
        raise tasks.SubmitError('KeyboardInterrupt', async_result.id)


#################
# Parallel mode #
#################

def handle_parallel(namespace, working_dir):
    ns, wd = namespace, working_dir

    seq = multitasking.BunchSequencer(
        processes=ns.parallel,
        dry=ns.dry,
        critical=True
    )

    dag = get_factory().create_scheduling_dag(
        working_branch=ns.branch,
        study_names=ns.studies,
        project_names=ns.projects,
        max_depth_check=ns.max_depth_check,
        min_depth_force=ns.min_depth_force,
        ignore_parent=ns.ignore_parent
    )

    for group in dag.groups_to_run():
        for study, data in group:
            if study.branch_out != namespace.branch:
                raise RuntimeError(
                    'Working on branch "{}", but study "{}" is not up-to-date on branch "{}"'
                    .format(namespace.branch, study.name, study.branch_out)
                )
            args = (
                data['datanode'].max_depth,
                study.name,
                study.project_name or '',
                study.branches_in
            )
            seq.add(*args)
        seq.checkpoint()

    seq.set_routine(get_routine(wd, ns))

    try:
        seq.run()
        rv = 0
    except KeyboardInterrupt:
        print('KeyboardInterrupt')
        rv = 1
    except Exception as e:
        print(e)
        rv = 1

    return rv


def get_routine(working_dir, namespace):
    """
    Returns the function to apply on each (study_name, project_name).
    The routine will send a task to celery.
    """
    def routine(lock, *args):
        max_depth, study_name, project_name, branches_in = args

        d_namespace = {
            'studies': [study_name],
            'projects': [project_name] if project_name else [],
            'branch': namespace.branch,
            'max_depth_check': 0,
            'origin': namespace.origin,
            'branches_in': json.dumps(branches_in)
        }

        lock.acquire()
        async_result, n_bytes = AnalysisTool(d_namespace).submit_studies(
            working_dir=working_dir,
            queue=namespace.queue
        )
        lock.release()

        r_args = d_namespace['studies'] + d_namespace['projects']
        date_str = pd.Timestamp.now().strftime('%Y-%m-%d %H:%M:%S')
        print('pid={} | {} | {} | depth={} | {}'
              .format(os.getpid(), date_str, async_result.id[:8], max_depth, ' '.join(r_args)))

        try:
            with open(os.devnull, 'w') as null:
                rv, msg = activate_monitoring(
                    async_result=async_result,
                    stdout=null,
                    stderr=null
                )
        except tasks.SubmitError as e:
            return 1, str(e)

        return rv, msg

    return routine
