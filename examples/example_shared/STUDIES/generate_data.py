"""
description: |-
  Generates a text file containing permutations of the project name

  This study could be replaced by a study that does nothing and an external process that writes the file
  at the right location (PROJECTS/project_name/DATA/generate_data/doc.txt)

inputs: []

outputs:
- alias: doc
  name: doc.txt
  driver: file
  description: text file (100 lines of 10 words)

parameters:
- name: word_size
  default: 3
  dtype: int


"""

import random

import data_pm


class Study(data_pm.BaseStudy):
    def run(self):
        alphabet = self.project_name[:3]

        # Generate text
        txt = generate_paragraph(alphabet, self.p.word_size)

        with data_pm.file.write(self.o.doc) as writer:
            writer.write(txt)


def generate_paragraph(alphabet, word_size):
    return '\n'.join(generate_line(alphabet, word_size) for _ in range(1000)) + '\n'


def generate_line(alphabet, word_size):
    return ' '.join([generate_word(alphabet, word_size) for _ in range(10)])


def generate_word(alphabet, size):
    """
    Generate a word

    :param str alphabet: Text string to pick letters from
    :param int size: Size of the word to create
    :rtype: str
    """
    out = [random.choice(alphabet) for _ in range(size)]

    # Reform a string
    return ''.join(out)
