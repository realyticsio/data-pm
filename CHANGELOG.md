## data-pm 2.15

- Réécriture et unification des drivers (file, hdfs)
  Les drivers permettent aux studies d'écrire leur output sur un système de fichier.
  Tous les drivers implémentent désormais les méthodes d'une classe de base inspirée de "os"
  Il est hautement recommandé d'utiliser ces drivers et notamment leur gestion de dossiers temporaires

- Ajout d'un driver s3

- Possibilité de récupérer une instance d'un driver dans le run() d'une étude:
  ```
  self.file_driver.listdir('/tmp')
  self.hdfs_driver.listdir('/tmp')
  self.s3_driver.listdir('s3://realytics-dtc/db')
  ```

  Ou directement depuis data_pm:
  ```
  data_pm.get_driver('file').listdir('/tmp')
  data_pm.get_driver('hdfs').listdir('/tmp')
  data_pm.get_driver('s3').listdir('s3://realytics-dtc/db')
  ```

- Chargement des paramètres depuis ces 4 types uniquement: str, int, float, bool
  Pour bool, les chaines "y", "yes", "true", "t" (case unsensitive) sont converties en True,
  les chaines "n", "no", "false", "f"  (case unsensitive) sont converties en False

- Changement de la signature de data_pm.Parameter: l'argument "type" devient "dtype"

