# Global imports
import sys
import importlib

# Local imports
from data_pm.core import drivers
from data_pm.core.factory import get_factory
from data_pm.core.study_components import study_configuration
from data_pm.tools import sync


class Input(object):
    def __init__(self, name, parent, alias=None, projects=None, branch=None, description=None, view=False):
        """
        :param str name:
        :param str parent: Parent study name
        :param str alias: Alias to use in shortcuts self.i.*
        :param projects: List of projects (when the current study is shared and the parent is not)
        :param branch:
        :param description:
        """

        self.name = name
        self.parent_study_name = parent
        self.alias = alias if alias is not None else name
        self.branch_name = branch
        self.description = description
        self.view = view
        self._projects = projects
        self._parent_study = None
        self._resource = None
        self._source = None
        self._driver = None
        self._output = None
        self._activated = False

        self.ext = '.' + self.name.split('.')[-1] if '.' in self.name else ''
        """File extension. Example: ".csv" for "myfile.csv", "" for "myfile" """

        self.study_control = None
        """:type: data_pm.core.study_control.StudyControl"""

    def set_branch_name(self, branch_name):
        self.branch_name = branch_name
        if self._parent_study is not None:
            self._parent_study.synchro_branch_out(branch_name)

    @property
    def projects(self):
        # Populate project list if necessary
        if self._projects is not None and not isinstance(self._projects, list):
            full_name_func = self._projects
            """:type: str"""
            self._projects = populate_projects(full_name_func)
        return self._projects

    @property
    def parent_study(self):
        if self._parent_study is None:
            self._parent_study = get_factory().create_study_control(
                study_name=self.parent_study_name,
                project_name=self.study_control.project_name
            )
            self.set_branch_name(self.branch_name)

        return self._parent_study

    @property
    def driver(self):
        if self._driver is None:
            self._driver = self._get_output().driver

        return self._driver

    @property
    def resource(self):
        if self._resource is None:
            self._resource = self._get_output().resource

        return self._resource

    def get_mtime(self):
        return self._get_output().get_mtime()

    @study_configuration.require_project_set
    def _get_output(self):
        if self._output is None:
            for output_name, o in self.parent_study.outputs.items():
                if o.name == self.name:
                    self._output = o

            if self._output is None:
                raise ValueError('Could not find output "{}" in study "{}", required by study "{}"'
                                 .format(self.name, self.parent_study_name, self.study_control.name))

        return self._output

    @property
    def path(self):
        if not self._activated:
            raise ValueError('input must be activated when trying to access its path')

        return self._source.path

    @study_configuration.require_project_set
    def activate(self):
        if self._activated:
            raise ValueError('input must be deactivated when trying to activate it')

        self._source = Source(self)
        self._source.activate()

        self._activated = True

    def deactivate(self):
        if not self._activated:
            raise ValueError('input must be activated when trying to deactivate it')

        self._source.deactivate()
        self._activated = False

    def clear(self):
        if self._source is not None:
            self._source.clear()

    def configuration_changed(self):
        self._parent_study = None


def populate_projects(full_function_name):
    """
    Load the function and call it to return a function name

    :param str full_function_name: Example: mypackage.submodule.generate_project_list
    :return: list[projects]
    :rtype: list[str]
    """

    parts = full_function_name.split('.')
    module_name = '.'.join(parts[:-1])
    function_name = parts[-1]

    if sys.version_info >= (3, 4):
        module = importlib.reload(importlib.import_module(module_name))
    else:
        module = reload(importlib.import_module(module_name))

    if hasattr(module, function_name):
        func = getattr(module, function_name)
    else:
        raise AttributeError('Module "{}" has no function "{}"'.format(module_name, function_name))

    return func()


class Source:
    def __init__(self, input_, _settings=None):
        self.driver_name = input_.driver.name
        self.driver = drivers.get_driver(input_.driver.name, _settings)
        self._input = input_
        self._resource = input_.resource
        self._tmp = None
        self._activated = False

    def activate(self):
        if self._activated:
            raise ValueError('source must be deactivated when trying to activate it')

        if drivers.need_redirection(self.driver_name):
            self._tmp = self.driver.TempDir(suffix=self._input.ext)

            if self._resource.exists():
                # Synchronize resource and source, preserves modification times for dependencies
                sync.synchronize(self._resource.path, self._resource.driver, self._tmp.path, self.driver,
                                 preserve_mtimes=True)

        self._activated = True

    def deactivate(self):
        if not self._activated:
            raise ValueError('source must be activated when trying to deactivate it')

        if drivers.need_redirection(self.driver_name):
            self.clear()

        self._activated = False

    def clear(self):
        if self._tmp is not None:
            self._tmp.remove()
            self._tmp = None

    @property
    def path(self):
        if not self._activated:
            raise ValueError('source must be activated when trying to access its path')

        if self._tmp:
            return self._tmp.path

        return self._resource.path
