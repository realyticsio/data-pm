#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Renames the outputs of studies from a branch to another.

Remove PROJECTS/[project_name]/DATA/[branch_name]/[study_name]
and/or SHARED/DATA/[branch_name]/[study_name].
Use with caution, there is no backup.
"""

# Global imports
from __future__ import print_function

import argparse
import sys

# Local imports
from data_pm import settings
from data_pm.core import study_mngr, project_mngr, branch
from data_pm.core.study_components import study_control
from data_pm.tools import git_helper
from data_pm.tools.cli_handler import BaseCommand


class MyAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        all_projects = set(project_mngr.get_project_names())
        project_studies = set(study_mngr.get_study_names())
        shared_studies = set(study_mngr.get_shared_study_names())

        setattr(namespace, 'projects', [v for v in values if v in all_projects])
        setattr(namespace, 'project_studies', [v for v in values if v in project_studies])
        setattr(namespace, 'shared_studies', [v for v in values if v in shared_studies])


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('projects_or_studies', nargs='*', action=MyAction, metavar='PROJECT OR STUDY')

    def handle(self, namespace):
        repo = git_helper.get_repo('.')
        branch_name = git_helper.get_current(repo)
        parent_branch_name = branch.parent(repo, branch_name)

        if not parent_branch_name:
            print('Branch "{}" has no parent'.format(branch_name))
            sys.exit(0)

        studies_to_merge = find_studies_to_merge(namespace, branch_name)
        sentences = sorted(set(['Study {} for project {}'.format(s, p) if p else 'Shared {}'.format(s)
                                for s, p in studies_to_merge]))

        sys.stdout.write(
            "@@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@\n"
            '{} --overwrites--> \033[41m{}\033[0m\n'
            'WARNING: You are going to remove ALL the outputs of the following studies:\n'
            '\n\t{}\n\n'
            "@@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@\n"
            .format(branch_name, parent_branch_name, '\n\t'.join(sentences)))

        confirm_or_exit()

        for study_name, project_name in studies_to_merge:
            s = study_control.StudyControl.from_name(study_name)

            for o in s.get_outputs().values():
                # Configure source
                o_src = o.copy()
                o_src.set_configuration(branch_name, study_name, project_name)

                # Configure destination
                o_dst = o.copy()
                o_dst.set_configuration(parent_branch_name, study_name, project_name)

                # Move source to destination
                if o_src.resource.exists():
                    src, dst = o_src.resource.path, o_dst.resource.path
                    try:
                        if o_src.driver.isdir(src):
                            o_dst.driver.makedirs(dst)
                    except NotImplementedError:
                        pass

                    o_dst.remove()
                    o_dst.driver.rename(src, dst)


def confirm_or_exit():
    sys.stdout.write('\nPlease confirm by writing "yesremoveallthis[ENTER]": ')

    try:
        confirm_text = sys.stdin.readline().strip()
    except KeyboardInterrupt:
        print('\nRemoval cancelled')
        sys.exit(0)

    if confirm_text != 'yesremoveallthis':
        print('Removal cancelled')
        sys.exit(0)


def find_studies_to_merge(namespace, branch_name):
    if not namespace.project_studies \
            and not namespace.shared_studies \
            and not namespace.projects:
        namespace.projects = project_mngr.get_project_names()
        namespace.shared_studies = study_mngr.get_shared_study_names()
        namespace.project_studies = study_mngr.get_study_names()

    if namespace.project_studies and not namespace.projects:
        namespace.projects = project_mngr.get_project_names()

    if namespace.projects and not namespace.project_studies:
        namespace.project_studies = study_mngr.get_study_names()

    studies_to_merge = []

    for driver_name in settings['data_pm.drivers']:
        # driver = drivers.get_driver(driver_name)

        for study_name in namespace.project_studies:
            for project_name in namespace.projects:
                # p = paths.path_to_data(driver, branch_name, study_name, project_name)
                # if driver.exists(p):
                #     studies_to_merge.append((study_name, project_name))
                studies_to_merge.append((study_name, project_name))

        for study_name in namespace.shared_studies:
            # p = paths.path_to_data(driver, branch_name, study_name)
            # if driver.exists(p):
            #     studies_to_merge.append((study_name, None))
            studies_to_merge.append((study_name, None))

    return set(studies_to_merge)
