import tempfile
import collections

from data_pm import settings


class BaseDriver(object):
    """
    Abstract driver the other drivers should inherit from
    """

    name = 'base'
    """Name of the driver"""

    def __init__(self, _settings=None):
        if _settings is None or _settings == settings:
            self.settings = settings
        else:
            self.settings = dict(settings.items(), **_settings)

    @staticmethod
    def get_default():
        """
        Get a default (maybe cached) instance of the class
        :return: BaseDriver
        """
        raise NotImplementedError()

    def TempFile(self, prefix='tmp', suffix='', dir=None, destination=None, create=False):
        """
        See data_pm.core.drivers.base_driver.TempFile

        :return: TempFile
        """
        return TempFile(driver=self, prefix=prefix, suffix=suffix, dir=dir, destination=destination, create=create)

    def TempDir(self, prefix='tmp', suffix='', dir=None, destination=None, create=False):
        """
        See data_pm.core.drivers.base_driver.TempDir

        :return: TempFile
        """
        return TempDir(driver=self, prefix=prefix, suffix=suffix, dir=dir, destination=destination, create=create)

    def abspath(self, path):
        """
        Returns an absolute path (see os.path.abspath)

        :param path:
        :return: str
        """
        raise NotImplementedError()

    def basename(self, path):
        """
        Returns the final component of a pathname (see os.path.basename)

        :param str path:
        :return: str
        """
        raise NotImplementedError()

    def dirname(self, path):
        """
        Returns the directory component of a pathname (see os.path.dirname)

        :param str path:
        :rtype: str
        """
        raise NotImplementedError()

    def exists(self, path):
        """
        Returns True if the path exists

        :param str path:
        :return: bool
        """
        raise NotImplementedError()

    def getmtime(self, path):
        """
        Returns last modification date

        :param path:
        :return: Unix timestamp (UTC) (in seconds), or None if the path does not exist
        :rtype: float
        """
        raise NotImplementedError()

    def getsize(self, path):
        """
        Returns file size

        :param path:
        :return: File size (count of bytes)
        :rtype: int
        """
        raise NotImplementedError()

    def isdir(self, path):
        """
        Returns True if the path is a directory
        Returns False if the path is a file or if it does not exist

        :param str path:
        :return: bool
        """
        raise NotImplementedError()

    def isfile(self, path):
        """
        Returns True if the path is a file
        Returns False if the path is a directory or if it does not exist

        :param str path:
        :return: bool
        """
        raise NotImplementedError()

    def join(self, arg, *args):
        """
        Joins two or more pathname components (see os.path.join)

        :param str arg: path start
        :param str args: other path parts
        :return: str
        """
        raise NotImplementedError()

    def listdir(self, path):
        """
        Lists the content of a directory

        :param str path:
        :return: list[str]
        """
        raise NotImplementedError()

    def list_statuses(self, path, recursive=False):
        """
        Directory tree generator.

        Example usage::
            # List a directory like listdir()
            [file_info.relative_path for file_info in driver.list_statuses(path)]

            # Find the latest maximum time in a directory hierarchy
            max([file_info.mtime for file_info in driver.list_statuses(path, recursive=True)])

        *Warning*: file_info.absolute_path will only be a true absolute path if :param:`path` is an absolute path.

        :param str path: Preferably an absolute path
        :param bool recursive: True to descend in subdirectories recursively
        :rtype: collections.Iterable[FileStatus]
        """
        raise NotImplementedError()

    def makedirs(self, path):
        """
        Creates a directory and intermediate directories if they do not exist (like "mkdir -p")

        Won't raise if the path already exists.

        :param str path:
        """
        raise NotImplementedError()

    @staticmethod
    def parse_config(url):
        """
        Parse client configuration from url

        :param url: influxdb://hostname:port/database/path
        :return: dict(hostname, port, database)
        """
        raise NotImplementedError()

    def read(self, path):
        """
        Reads from a file

        Example usage:
            with client.read('mypath/myfile.txt') as reader:
                txt = reader.read()  # reader is a file-like object

        :param str path:
        :return: contextlib.GeneratorContextManager
        """
        raise NotImplementedError()

    def remove(self, path, recursive=False):
        """
        Deletes (optionally recursively) a file or folder

        Will raise an error if the path does not exist

        :param str path:
        :param bool recursive: When True, allow deleting a directory that contains files or subfolders
        """
        raise NotImplementedError()

    def rename(self, src, target):
        """
        Renames a source path to a target path

        :param str src:
        :param str target:
        """
        raise NotImplementedError()

    def renames(self, src, target):
        """
        Renames a source path to a target path, creating directories if necessary

        :param str src:
        :param str target:
        """
        raise NotImplementedError()

    def setmtime(self, path, mtime=None):
        """
        Sets modification time on a path

        :param str path:
        :param float mtime: Modification time as a UNIX timestamp (UTC) (in seconds)
            If mtime is None, use the current time (time.time())
        """
        raise NotImplementedError()

    def walk(self, path):
        """
        Directory tree generator, see os.walk

        :param str path:
        :return: iterator[(str,list[str],list[str])]
        """
        raise NotImplementedError()

    def write(self, path):
        """
        Writes to a file

        Example usage:
            with client.write('mypath/myfile.txt') as writer:
                txt = writer.write()  # reader is a file-like object

        :param str path:
        :return: contextlib.GeneratorContextManager
        """
        raise NotImplementedError()


class TempFile(object):
    def __init__(self, driver, prefix='tmp', suffix='', dir=None, destination=None, create=False):
        """
        Temporary file

        The file is automatically deleted when this object is garbage collected.
        If possible, delete the file manually by calling remove().

        Example usage:
            ```
            mytmp = TempFile(driver)
            with driver.write(mytmp.path) as writer:
                writer.write('hello world')

            # Do whatever your want with the file (like uploading it somewhere)

            mytmp.remove()
            ```

        :param BaseDriver driver:
        :param str prefix:
        :param str suffix:
        :param str dir: Where to create the temporary file
        :param str destination: The optional destination of the content of this folder
        """

        self.destination = destination
        self.driver = driver

        self.path = self.driver.abspath(tempfile.mktemp(suffix=suffix, prefix=prefix, dir=dir))
        """
        Randomly generated path
        :type: str
        """

        # Create an empty file
        if create:
            with self.driver.write(self.path) as reader:
                pass

    def move_to_destination(self):
        """
        Move the temporary file to its destination.
        Also works when `self.path` is a directory (hard sync).

        If the destination already exist, it will be deleted before the move.
        """
        if self.path is None:
            raise ValueError('Temporary path has already been moved to destination "{}"'.format(self.destination))

        if self.driver.exists(self.destination):
            self.driver.remove(self.destination, recursive=True)

        if self.driver.exists(self.path):
            # No need to do anything if the path doesn't exist.
            self.driver.renames(self.path, self.destination)
            self.remove()

    def remove(self):
        """
        Remove the temporary file/directory

        Once called, self.path is set to None
        """
        if self.path is not None and self.driver.exists(self.path):
            self.driver.remove(self.path, recursive=True)

        self.path = None

    def __del__(self):
        self.remove()


class TempDir(TempFile):
    def __init__(self, driver, prefix='tmp', suffix='', dir=None, destination=None, create=False):
        """
        Temporary directory

        The directory is automatically deleted when this object is garbage collected.
        If possible, delete the directory manually by calling remove().

        Example usage:
            ```
            mytmp = TempDir(driver, destination='final/dest', create=True)
            with driver.write(driver.join(mytmp.path, 'file.txt')) as writer:
                writer.write('hello world')
            mytmp.move_content_to_destination()

            # final/dest now contains a file named file.txt
            ```

        :param BaseDriver driver:
        :param str prefix:
        :param str suffix:
        :param str dir: Where to create the temporary file
        :param str destination: The optional destination of the content of this folder
        """

        # Initiate as a TempFile, without creating the file
        super(self.__class__, self).__init__(
            driver=driver,
            prefix=prefix,
            suffix=suffix,
            dir=dir,
            destination=destination,
            create=False)

        if create:
            self.driver.makedirs(self.path)

    def move_content_to_destination(self):
        """
        Move the content of the temporary directory to its destination (soft sync).

        The content already existing in the destination will be deleted before the move.
        """
        # If destination already exists, copy each file contained in self.path to self.destination
        #  Else, move the whole directory self.path
        if self.driver.exists(self.destination):
            if self.driver.exists(self.path):
                files = self.driver.listdir(self.path)
            else:
                files = []

            for f in files:
                src_path = self.driver.join(self.path, f)
                dst_path = self.driver.join(self.destination, f)

                if self.driver.exists(dst_path):
                    self.driver.remove(dst_path, recursive=True)
                self.driver.renames(src_path, dst_path)

            self.remove()
        else:
            self.move_to_destination()


class FileStatus(object):
    def __init__(self, absolute_path, relative_path, is_directory,
                 mtime, size, children_num):
        """
        Provide information about a file or a directory
        """

        self.absolute_path = absolute_path
        """
        Absolute path. Example: /opt/test/file.txt
        :type: str
        """

        self.relative_path = relative_path
        """
        Relative path (see BaseDriver.list_statuses()). Example: test/file.txt
        :type: str
        """

        self.is_directory = is_directory
        """
        True if this is a directory
        :type: bool
        """

        self._mtime = mtime
        """
        Modification time as a unix timestamp. Example: 1513258119.5487583
        :type: float
        """

        self.size = size
        """
        File size in bytes (0 if directory)
        :type: int
        """

        self.children_num = children_num
        """
        Number of children
        0 if file, len(listdir) otherwise
        :type: int
        """

    def __repr__(self):
        return '<FileInfo({})>'.format(self.absolute_path)

    @property
    def mtime(self):
        """
        Modification time as a unix timestamp. Example: 1513258119.5487583
        :rtype: float
        """
        return self._mtime

