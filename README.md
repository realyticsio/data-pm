Data Project Management (data_pm)
==================================

Presentation
-------------

This package is a data manager designed for data science projects.

It consists in a framework to organize and process data,
and a web based dashboard to watch the status of the processes.
The data stream process is divided into studies,
that are atomic pieces of work that get input data, and produce output data.
A study's input data is usually another study's output data, thus forming a dependency tree.

Multiple projects can be handled in the same work space:
each project follows the dependency tree determined by the studies, but the data does not mix.
Mixing data between projects is made possible with shared studies.


Features:

- Python framework
- Split data stream into atomic studies
- Studies dependence as in a Makefile
- Studies parameters
- Inter-projects studies
- Live debugging
- Web based dashboard
- Execute tasks asynchroneously on distributed cluster
- Versionning data


Install
-------

`pip>=1.1` is required to perform the installation ::

    $ sudo -E pip install git+https://anon@bitbucket.org/realyticsio/data-pm.git

You should now be able to import `data_pm` in a python shell and run ``pm`` command in your command shell.


Create a datalab
----------------

Navigate to the directory in which you want to create the datalab.

Run the following command ::
    
    $ pm init datalab

This will create all necessary directories to start running commands with pm.


Configure drivers
-----------------

Create a file named <path-to-datalab>/settings/settings.yml.

Redifined, if necessary, with yaml syntax the following settings:
- data_pm.driver.file.workspace: ~/.local/data-pm/data
- data_pm.driver.file.tmpdir: /tmp

To configure hdfs and S3 drivers: 
- data_pm.driver.hdfs.url: http://<hostname>:50070
- data_pm.driver.hdfs.user: <username>
- data_pm.driver.hdfs.workspace: my_data_dir

If data-pm writes outputs to hdfs:
- data_pm.drivers: [file, hdfs]

For now, the S3 driver is used only to be used from inside studies.
Do not write your outputs on S3.


Getting help
-------------

Run ``pm help`` in your shell to launch the firefox web browser directly to the help page.
You can specify a path to a different web browser with `--browser` option.

Head to the command page to get an insight of the tool!


Running tests
-------------

Extract the source code tarball somewhere, then run from the root directory ::
    
    python -m unittest discover


Compiling documentation
-----------------------

Extract the source code tarball somewhere, then run from the root directory ::
    
    python setup.py build_sphinx
