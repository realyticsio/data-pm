# Global imports
from __future__ import print_function

import data_pm
from data_pm.core import paths

import flask_restplus
from flask_restplus import reqparse
from werkzeug.datastructures import FileStorage


ns = flask_restplus.Namespace('data_view')


parser = reqparse.RequestParser()\
    .add_argument('html_file', type=FileStorage, location='files', required=True)\
    .add_argument('branch', required=True)\
    .add_argument('study', required=True)\
    .add_argument('project', required=False)


@ns.route('/')
class Index(flask_restplus.Resource):
    @ns.expect(parser)
    def put(self):
        args = parser.parse_args()

        branch_name = args['branch']
        study_name = args['study']
        project_name = args.get('project')

        file_storage = args['html_file']

        # Path to the views
        dir_path = paths.path_to_views(
            driver=data_pm.file,
            branch_name=branch_name,
            study_name=study_name,
            project_name=project_name
        )

        data_pm.file.makedirs(dir_path)
        file_path = data_pm.file.join(dir_path, 'view.html')

        file_storage.save(file_path)
