# Global imports
import traceback
import os
import importlib
import sys

# Local imports
from data_pm import settings


def is_shared(study_name):
    return study_name in get_shared_study_names()


def get_all_study_names():
    study_names = get_study_names()
    shared_study_names = get_shared_study_names()

    # Check for duplicates
    doubles = set(study_names) & set(shared_study_names)
    if len(doubles) > 0:
        raise ValueError('Studies can not exist both as a study and as a shared study: "{}"'.format(doubles))

    return study_names + shared_study_names


def get_study_names():
    return get_study_names_in_path(settings['data_pm.studies.dir'])


def get_shared_study_names():
    return get_study_names_in_path(settings['data_pm.shared_studies.dir'])


# Path based way
def get_study_names_in_path(path, prefix=''):
    """
    Return the list of studies that live in `path`

    :param string path:
    :param string prefix:
    :rtype: List of strings
    """
    study_names = []

    for dirpath, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if filename.endswith('.py') and filename != '__init__.py':
                relative_dir_path = os.path.relpath(dirpath, path)
                study_name = prefix + relative_dir_path.replace('/', '.') + '.' + filename.replace('.py', '')
                study_name = study_name.strip('.')
                study_names.append(study_name)

    return study_names


def load_study(study_name):
    """
    Load a study

    :param str study_name:
    :rtype: data_pm.core.study.BaseStudy
    """
    # Try load as a normal study
    module = try_load('DEV.STUDIES', study_name)

    # If it failed, load as a shared study
    if module is None:
        module = try_load('DEV.SHARED_STUDIES', study_name)

    # If it failed, raise an error
    if module is None:
        raise RuntimeError('Could not find study "{}"'.format(study_name))

    # Otherwise, return the study
    return module.Study()


def load_study_module(study_name):
    """
    Load a study

    :param str study_name:
    :rtype: data_pm.core.study.BaseStudy
    """
    # Try load as a normal study
    module = try_load('DEV.STUDIES', study_name)

    # If it failed, load as a shared study
    if module is None:
        module = try_load('DEV.SHARED_STUDIES', study_name)

    # If it failed, raise an error
    if module is None:
        raise RuntimeError('Could not find study "{}"'.format(study_name))

    # Otherwise, return the study
    return module


def try_load(prefix, study_name):
    """
    Attempt to load a module

    :return: the module or None if the module could not be loaded
    """

    try:
        importlib.import_module(prefix + '.' + study_name)

        if sys.version_info >= (3, 4):
            return importlib.reload(importlib.import_module(prefix + '.' + study_name))
        else:
            return reload(importlib.import_module(prefix + '.' + study_name))
    except ImportError as e:
        if not is_just_import_error(e, prefix, study_name):
            raise ImportError(
                traceback.format_exc(),
                'Could not load "{}"'.format(study_name),
            )


def is_just_import_error(e, prefix, study_name):
    if len(e.args) == 0:
        return type(e) == ImportError

    if 'No module named ' in e.args[0]:
        if sys.version_info < (3, ):
            if e.args[0].replace('No module named ', '') in study_name:
                return True
        else:
            if e.args[0].startswith("No module named '{}.".format(prefix)):
                return True

    return False


def parse_study_full_name(study_full_name):
    if study_full_name.startswith('DEV.STUDIES.'):
        return study_full_name.replace('DEV.STUDIES.', ''), False
    elif study_full_name.startswith('DEV.SHARED_STUDIES.'):
        return study_full_name.replace('DEV.SHARED_STUDIES.', ''), True
    else:
        raise ValueError('Could not determine whether the study was shared: "{}"'.format(study_full_name))
