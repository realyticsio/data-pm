# Global imports
import socket
import re
import os
import glob
from datetime import datetime

# Local imports
from data_pm import settings
from data_pm import log


ISO_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'


class StudyLogWriter:
    def __init__(self, logger):
        self.logger = logger
        self._handler = None
        self.file_path = None

    @property
    def handler(self):
        if self._handler is None:
            raise ValueError('StudyLogWriter is not activated')
        return self._handler

    def open(self, branch_name, project_name, study_name):
        self._handler = log.create_handler(
            handler_name='requests',
            branch_name=branch_name,
            project_name=project_name,
            study_name=study_name
        )
        self.logger.addHandler(self.handler)
        return self

    def flush(self):
        pass

    def set_start(self):
        date = datetime.utcnow().strftime(ISO_DATE_FORMAT)
        message = 'StudyLog.start_date={}'.format(date)
        self.handler.emit(message, format=False)

    def set_end(self):
        date = datetime.utcnow().strftime(ISO_DATE_FORMAT)
        message = 'StudyLog.end_date={}'.format(date)
        self.handler.emit(message, format=False)

    def set_status(self, status):
        message = 'StudyLog.status={}'.format(status)
        self.handler.emit(message, format=False)

    def set_hostname(self):
        message = 'StudyLog.hostname={}'.format(socket.gethostname())
        self.handler.emit(message, format=False)

    def set_origin(self, origin):
        message = 'StudyLog.origin={}'.format(origin)
        self.handler.emit(message, format=False)

    def set_activation_time(self, duration):
        message = 'StudyLog.activation_time={}'.format(duration)
        self.handler.emit(message, format=False)

    def set_deactivation_time(self, duration):
        message = 'StudyLog.deactivation_time={}'.format(duration)
        self.handler.emit(message, format=False)

    def set_last_update(self):
        date = datetime.utcnow().strftime(ISO_DATE_FORMAT)
        message = 'LastUpdate.datetime={}'.format(date)
        self.handler.emit(message, format=False)

    def close(self):
        self.flush()
        self.logger.removeHandler(self.handler)
        self._handler = None


class StudyLogReader:
    def __init__(self, file_path):
        self.start_date = None
        self.end_date = None
        self.status = None
        self.hostname = None
        self.origin = None
        self.activation_time = None
        self.deactivation_time = None

        self.lines = []
        self.name = os.path.basename(file_path)

        self.load(file_path)

    def load(self, file_path):
        with open(file_path, 'r') as f:
            lines = f.readlines()

        for line in lines:
            d_meta = parse_meta_log(line)
            if d_meta:
                self.set_value(**d_meta)
            else:
                self.lines.append(line)

    def set_value(self, keyword, value):
        setattr(self, keyword, value)

    @property
    def duration(self):
        if self.start_date and self.end_date:
            timedelta = self.end_date - self.start_date
            return int(timedelta.total_seconds())

    @property
    def text(self):
        return '\n'.join(self.lines)


def get_log_number(name):
    assert name.startswith(settings['data_pm.log.filename']),\
        'Log name "{}" does not look like "{}"'.format(name, settings['data_pm.log.filename'])

    if name == settings['data_pm.log.filename']:
        return 0
    else:
        _, ext = os.path.splitext(name)
        return int(ext.strip('.'))


def load_logs(path):
    log_files = glob.glob(os.path.join(path, '{}*'.format(settings['data_pm.log.filename'])))

    logs = []
    for log_file in sorted(log_files, key=lambda f: get_log_number(os.path.basename(f))):
        logs.append(StudyLogReader(log_file))

    return logs


def parse_meta_log(message):
    hooks = [
        '^StudyLog.(?P<keyword>\w+)=(?P<value>.*)$',
        '^LastUpdate.(?P<keyword>\w+)=(?P<value>.*)$'
    ]

    for pattern in hooks:
        m = re.match(pattern, message)

        if m is None:
            continue

        return process(m.groupdict())

    return dict()


def process(groupdict):
    date_keys = [
        'start_date',
        'end_date',
        'datetime'
    ]
    if groupdict['keyword'] in date_keys:
        date_str = groupdict['value']
        date_obj = datetime.strptime(date_str, ISO_DATE_FORMAT)
        groupdict.update(value=date_obj)
    return groupdict
