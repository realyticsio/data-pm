# Local imports
from data_pm.core.factory import base_factory
from data_pm.core.study_components.study_control import StudyControl
from data_pm.core.branch import BranchConfiguration


class TestFactoryProject(base_factory.BaseFactory):
    """
        A
        | \
        |  B
        |  |
        |  C
        | /
        D
    """

    d_studies = {
        'A': {
            'is_shared': False,
            'd_meta': {
                'description': '',
                'inputs': [],
                'outputs': [
                    {'name': 'output1'},
                    {'name': 'output2'},
                ],
                'parameters': []
            }
        },
        'B': {
            'is_shared': False,
            'd_meta': {
                'description': '',
                'inputs': [
                    {'name': 'output1', 'parent': 'A'}
                ],
                'outputs': [
                    {'name': 'output1'}
                ],
                'parameters': []
            }
        },
        'C': {
            'is_shared': False,
            'd_meta': {
                'description': '',
                'inputs': [
                    {'name': 'output1', 'parent': 'B'}
                ],
                'outputs': [
                    {'name': 'output1'}
                ],
                'parameters': []
            }
        },
        'D': {
            'is_shared': False,
            'd_meta': {
                'description': '',
                'inputs': [
                    {'name': 'output1', 'parent': 'C'},
                    {'name': 'output2', 'parent': 'A'}
                ],
                'outputs': [
                    {'name': 'output1'}
                ],
                'parameters': []
            }
        },
    }

    testing_config = BranchConfiguration(
        d_conf={
            'master': {
                'parent': None,
                'recompute_from': None
            },
            'dev1': {
                'parent': 'master',
                'recompute_from': ['C']
            },
        }
    )

    def create_study_control(self, study_name, project_name=None, configure_branch=False):
        is_shared = self.d_studies[study_name]['is_shared']
        d_meta = self.d_studies[study_name]['d_meta']

        d_args = StudyControl.parse_meta(**d_meta)

        study_ctrl = StudyControl(name=study_name, is_shared=is_shared, **d_args)

        if not study_ctrl.is_shared:
            study_ctrl.set_project_name(project_name)

        return study_ctrl

    def create_branch_configuration(self):
        return self.testing_config

    def get_project_names(self):
        return ['alpha', 'beta', 'gamma']


class TestFactoryShared(base_factory.BaseFactory):
    """
            A
           / \
          /   \
      D  C     B____
       \ |     |    |
        \|     |    |
         E*    F*   |
         |     |    |
         \     G    /
          \  /     /
            H_____/
    """

    d_studies = {
        'A': {
            'is_shared': True,
            'd_meta': {
                'description': '',
                'inputs': [],
                'outputs': [
                    {'name': 'output_A'},
                ],
                'parameters': []
            }
        },
        'B': {
            'is_shared': True,
            'd_meta': {
                'description': '',
                'inputs': [
                    {'name': 'output_A', 'parent': 'A'}
                ],
                'outputs': [
                    {'name': 'output_B'}
                ],
                'parameters': []
            }
        },
        'C': {
            'is_shared': True,
            'd_meta': {
                'description': '',
                'inputs': [
                    {'name': 'output_A', 'parent': 'A'}
                ],
                'outputs': [
                    {'name': 'output_C'}
                ],
                'parameters': []
            }
        },
        'D': {
            'is_shared': True,
            'd_meta': {
                'description': '',
                'inputs': [],
                'outputs': [
                    {'name': 'output_D'}
                ],
                'parameters': []
            }
        },
        'E': {
            'is_shared': False,
            'd_meta': {
                'description': '',
                'inputs': [
                    {'name': 'output_C', 'parent': 'C'},
                    {'name': 'output_D', 'parent': 'D'}
                ],
                'outputs': [
                    {'name': 'output_E'}
                ],
                'parameters': []
            }
        },
        'F': {
            'is_shared': False,
            'd_meta': {
                'description': '',
                'inputs': [
                    {'name': 'output_B', 'parent': 'B'}
                ],
                'outputs': [
                    {'name': 'output_F'}
                ],
                'parameters': []
            }
        },
        'G': {
            'is_shared': True,
            'd_meta': {
                'description': '',
                'inputs': [
                    {'name': 'output_F', 'parent': 'F', 'projects': ['alpha', 'beta', 'gamma']},
                ],
                'outputs': [
                    {'name': 'output_G'}
                ],
                'parameters': []
            }
        },
        'H': {
            'is_shared': True,
            'd_meta': {
                'description': '',
                'inputs': [
                    {'name': 'output_E', 'parent': 'E', 'projects': ['alpha', 'beta', 'gamma']},
                    {'name': 'output_B', 'parent': 'B'},
                    {'name': 'output_G', 'parent': 'G'}
                ],
                'outputs': [
                    {'name': 'output_H'}
                ],
                'parameters': []
            }
        },
    }

    testing_config = BranchConfiguration(
        d_conf={
            'master': {
                'parent': None,
                'recompute_from': None
            },
            'dev1': {
                'parent': 'master',
                'recompute_from': ['C']
            },
        }
    )

    def create_study_control(self, study_name, project_name=None, configure_branch=False):
        is_shared = self.d_studies[study_name]['is_shared']
        d_meta = self.d_studies[study_name]['d_meta']

        d_args = StudyControl.parse_meta(**d_meta)

        study_ctrl = StudyControl(name=study_name, is_shared=is_shared, **d_args)

        if not study_ctrl.is_shared:
            study_ctrl.set_project_name(project_name)

        return study_ctrl

    def create_branch_configuration(self):
        return self.testing_config

    def get_project_names(self):
        return ['alpha', 'beta', 'gamma']
