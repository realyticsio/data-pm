#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Compute the html view if Study::view is defined.
"""
# Global imports
from __future__ import print_function
import os
import sys

# Local imports
import data_pm
from data_pm import client
from data_pm.core.analysis_tool import AnalysisTool
from data_pm.core.factory import get_factory
from data_pm.deployment.environments import WorkingDirHelper
from data_pm.tools.cli_handler import BaseCommand


__maintainer__ = "Nicolas Richard"


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('--study', required=True)
        parser.add_argument('--project', nargs='?')
        parser.add_argument('-p', '--params', dest='params', nargs='*', type=str,
                            help='Parameters in the form myparam=myvalue')

    def handle(self, namespace):
        init_path = os.getcwd()
        working_dir = WorkingDirHelper()
        working_dir.snapshot_from_directory('.')

        try:
            os.chdir(working_dir.path)
            data_pm.logger.info('Computing view in local')
            make_view(namespace.study, namespace.project, namespace.params)
            data_pm.logger.info('** VIEW UPDATED SUCCESSFULLY')
        finally:
            os.chdir(init_path)
            working_dir.clear()


def make_view(study_name, project_name=None, params=None):
    study_ctrl = get_factory().create_study_control(
        study_name=study_name,
        project_name=project_name,
        configure_branch=True
    )

    study = get_factory().create_study(study_ctrl)

    if not hasattr(study, 'view'):
        raise RuntimeError('Study "{}" does not implement method "view"'.format(study.name))

    if params:
        d_params = AnalysisTool.parse_params(params)
        study.set_parameters(d_params)

    view_inputs = [i for i in study.inputs.values() if i.view]
    view_outputs = [o for o in study.outputs.values() if o.view]

    for o in view_outputs:
        o.use_tmp_dir = False  # Hack to force download hdfs->file
    for x in view_inputs + view_outputs:
        x.activate()

    html_string = study.view()

    for x in view_inputs + view_outputs:
        x.deactivate()

    size = sys.getsizeof(html_string) // 1024
    if size > 1000:
        raise Exception('Upload aborted: your html view is too big ({} KB)'.format(size))

    tmp_file = data_pm.file.TempFile()

    with open(tmp_file.path, 'w') as fd:
        fd.write(html_string)

    try:
        with open(tmp_file.path, 'rb') as fd:
            r = client.put_view(
                dictionary={
                    'branch': study.branch_out,
                    'study': study.name,
                    'project': study.project_name
                },
                fd=fd
            )
    finally:
        tmp_file.remove()

    if r.status_code != 200:
        raise RuntimeError('Failed to send view. Status={}. {}'.format(r.status_code, r.content))

    print('{} KB uploaded'.format(size))
