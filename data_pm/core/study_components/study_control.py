# Global imports
import io
import os
import yaml
import data_pm
from yaml.parser import ParserError

# Local imports
from data_pm.core import project_mngr
from data_pm.core.factory import get_factory
from data_pm.core.study_components import shortcut, input


class StudyControl(object):
    def __init__(self, name, is_shared, description, d_inputs, d_outputs, d_parameters):
        """
        Characterizes a study: inputs, outputs and parameters

        :param str name: The name of the study as a string
        :param bool is_shared: True if the study is shared
        :param str description:
        :param dict[str, data_pm.core.study_components.Input] d_inputs:
        :param dict[str, data_pm.core.study_components.Output] d_outputs:
        :param dict[str, data_pm.core.study_components.Parameter] d_parameters:
        """

        # Copy args into class variables
        self.name = name
        self.description = description
        self.inputs = d_inputs
        self.outputs = d_outputs
        self.parameters = d_parameters

        self._exploded = False

        # Set study_control for each child class
        for i in d_inputs.values():
            i.study_control = self

        for o in d_outputs.values():
            o.study_control = self

        for p in d_parameters.values():
            p.study_control = self

        # Create shortcuts
        self.i = shortcut.Shortcut(d_inputs, 'path')
        """Shortcut: `self.i.my_input_name` points to `self.inputs['my_input_name'].path`"""

        self.o = shortcut.Shortcut(d_outputs, 'path')
        """Shortcut: `self.o.my_output_name` points to `self.outputs['my_output_name'].path`"""

        self.p = shortcut.Shortcut(d_parameters, 'value')
        """Shortcut: `self.p.my_param_name` points to `self.parameters['my_param_name'].value`"""

        self.is_shared = is_shared
        """True if the study is shared, False otherwise"""

        self.project_name = None
        """The name of the project as a string (if the study is not shared)"""

        self.branches_in = None
        """Dict of branches for each input"""

        self.branch_out = None
        """The name of the branch on which the study must write"""

        self.full_name = 'DEV.SHARED_STUDIES.' + name if is_shared else 'DEV.STUDIES.' + name
        """Study full name. Example: DEV.STUDIES.sessions.computing"""

    def __hash__(self):
        if self.project_name is None:
            return hash(self.name)
        else:
            return hash(self.name + self.project_name)

    def __eq__(self, other):
        return self.name == other.name and self.project_name == other.project_name

    def __ne__(self, other):
        return self.name != other.name or self.project_name != other.project_name

    def __repr__(self):
        if self.is_shared:
            return '<StudyControl {} on {}>'.format(self.name, self.branch_out)
        else:
            return '<StudyControl {} for {} on {}>'.format(self.name, self.project_name, self.branch_out)

    def mtimes_up_to_date(self):
        input_mtimes = [i.get_mtime() for i in self.get_inputs().values()]
        output_mtimes = [o.get_mtime() for o in self.get_outputs().values()]

        inputs_missing = len([d_i for d_i in input_mtimes if d_i is None]) > 0
        output_missing = len([d_o for d_o in output_mtimes if d_o is None]) > 0

        # Case 1: The study produces nothing -> always up to date
        if len(output_mtimes) == 0:
            return True

        # Case 2: The study takes no input -> up to date if all outputs exist
        if len(input_mtimes) == 0:
            if output_missing:
                return False
            else:
                return True

        # Case 3: The study takes inputs and produces outputs
        if output_missing:
            # Outputs are missing -> not up to date
            return False
        elif inputs_missing:
            # Inputs are missing -> certainly not up date
            return False
        elif max(input_mtimes) > min(output_mtimes):
            # Inputs are more recent than outputs -> not up to date
            return False
        else:
            # Up to date
            return True

    def status_up_to_date(self, last_updates):
        """
        :param dict[str, Any] last_updates: min(output mtimes) for each study
        """

        def get_mtime(branch, study, project=None):
            d = last_updates.get(branch, {}).get(study, {})
            if isinstance(d, dict):
                if len(d) == 0:
                    return None
                else:
                    return d.get(project)
            else:
                return d

        mtime = get_mtime(self.branch_out, self.name, self.project_name)

        if mtime is None:
            return False

        i_mtimes = [
            get_mtime(i.branch_name, i.parent_study_name, i.parent_study.project_name)
            for i in self.inputs.values()
        ]

        if len(i_mtimes) == 0:
            return True
        elif None in i_mtimes:
            return False
        else:
            return mtime >= max(i_mtimes)

    def explode_inputs(self):
        for input_name, i in list(self.inputs.items()):
            try:
                # Try to load parent study, add some information if error.
                parent_is_shared = i.parent_study.is_shared
            except Exception as e:
                msg = e.args[0]
                if msg.startswith('Could not find study'):
                    raise ValueError(msg + ' from "{}" for input "{}"'.format(self.name, input_name))
                raise

            if self.is_shared and not parent_is_shared:
                # If this study is shared, and the input is not, we must create new inputs and set projects for them
                all_projects = get_factory().get_project_names()
                l_projects = i.projects or all_projects
                unknown = [p for p in l_projects if p not in all_projects]

                if unknown:
                    raise ValueError('[study={}] [input={}] Unknown projects: {}'
                                     .format(self.name, input_name, unknown))

                for project_name in l_projects:
                    i_project = input.Input(
                        name=i.name,
                        parent=i.parent_study_name,
                        alias=i.alias,
                        branch=i.branch_name,
                        description=i.description
                    )
                    i_project.study_control = self
                    i_project.parent_study.set_project_name(project_name)
                    self.inputs['{}__{}'.format(input_name, project_name)] = i_project
                del self.inputs[input_name]

    def get_inputs(self):
        if not self._exploded:
            self.explode_inputs()
            self._exploded = True
        return self.inputs

    def get_outputs(self):
        return self.outputs

    def set_project_name(self, project_name):
        assert not self.is_shared, 'Can not set project name on a shared study'
        assert self.project_name is None, 'Can not reset a project name on a study, you have to instantiate another one'

        self.project_name = project_name

    def set_branches_in(self, branches_in):
        self.branches_in = branches_in
        for i in self.inputs.values():
            if i.parent_study_name not in branches_in:
                raise RuntimeError(
                    'Make sure that study "{}" exists on current branch'
                    .format(i.parent_study_name)
                )
            branch_in = branches_in[i.parent_study_name]
            i.set_branch_name(branch_in)

    def set_branch_out(self, branch_out):
        assert self.branch_out is None, 'Can not reset branch out'
        self.branch_out = branch_out

    def synchro_branch_out(self, branch_out):
        """Must be called by Input class only"""
        self.branch_out = branch_out

    @staticmethod
    def parse_meta(description='', inputs=list(), outputs=list(), parameters=list()):
        """
        :param description:
        :param list[dict] inputs:
        :param outputs:
        :param parameters:
        :rtype:
        """
        from data_pm.core.study_components import input
        from data_pm.core.study_components import output
        from data_pm.core.study_components import parameter

        def instantiate(cls, d):
            import inspect

            arg_spec = inspect.getargspec(cls.__init__)
            required_args = arg_spec.args[:-len(arg_spec.defaults)]
            missing_args = set(required_args) - set(d.keys()) - {'self'}

            if missing_args:
                raise ValueError('{} is missing properties: {}'.format(cls.__name__, ', '.join(missing_args)))

            return cls(**d)

        try:
            l_inputs = [instantiate(input.Input, d_input) for d_input in inputs]
            l_outputs = [instantiate(output.Output, d_output) for d_output in outputs]
            l_parameters = [instantiate(parameter.Parameter, d_parameter) for d_parameter in parameters]
        except TypeError as e:
            raise e

        def to_dict(l):
            return {e.alias: e for e in l}

        return {
            'description': description,
            'd_inputs': to_dict(l_inputs),
            'd_outputs': to_dict(l_outputs),
            'd_parameters': to_dict(l_parameters)
        }

    @staticmethod
    def from_name(study_name):
        """
        :param str study_name:
        :rtype: StudyControl
        """
        d = cache.get_metas_from_name(study_name)
        d_meta = d['d_meta']
        is_shared = d['is_shared']

        try:
            d_args = StudyControl.parse_meta(**d_meta)
        except:
            data_pm.logger.error('Could not parse study metadata for "{}"'.format(study_name))
            raise

        return StudyControl(name=study_name, is_shared=is_shared, **d_args)


def path_to_study(study_name):
    """
    Find the relative path to a study

    :param str study_name:
    :return: (relative_path, is_shared)
    :rtype: (str, bool)
    """

    relative_path = os.path.join(data_pm.settings['data_pm.studies.dir'], study_name.replace('.', os.path.sep) + '.py')
    if os.path.exists(relative_path):
        return relative_path, False

    relative_path = os.path.join(data_pm.settings['data_pm.shared_studies.dir'], study_name.replace('.', os.path.sep) + '.py')
    if os.path.exists(relative_path):
        return relative_path, True

    raise ValueError('Could not find study "{}"'.format(study_name))


def get_docstring_from_study_name(study_name):
    path, is_shared = path_to_study(study_name)

    with open(path, mode='r', encoding='utf-8') as fd:
        code = fd.read()

    idx_start = code.find('"""', 0)
    idx_stop = code.find('"""', idx_start + 3)
    if idx_start == -1 or idx_stop == -1:
        raise ValueError('No module docstring found for {}'.format(path))
    docstring = code[idx_start + 3:idx_stop]

    return docstring, is_shared


def parse_docstring(docstring):
    """
    Parse a yaml docstring
    :param str docstring:
    :rtype: dict
    """
    if isinstance(docstring, str):
        docstring = docstring.encode('utf-8')
    try:
        loader = yaml.CLoader if hasattr(yaml, 'CLoader') else yaml.Loader
        d_meta = yaml.load(io.BytesIO(docstring), Loader=loader)
    except yaml.parser.ParserError as e:
        e.context = 'Could not parse docstring'
        raise e
    return d_meta or {}


class Cache(object):
    def __init__(self):
        self.d_parsed = {}
        """dict[study_name,d_meta]"""

    def get_metas_from_name(self, study_name):
        """
        :param study_name:
        :rtype: dict[str, Any]
        :return: dict[study_name,Any]
        """
        if study_name not in self.d_parsed:
            docstring, is_shared = get_docstring_from_study_name(study_name)
            self.d_parsed[study_name] = {}
            try:
                self.d_parsed[study_name]['d_meta'] = parse_docstring(docstring)
                self.d_parsed[study_name]['is_shared'] = is_shared
            except Exception:
                data_pm.logger.error('Could not parse docstring for study "{}"'.format(study_name))
                raise

        return self.d_parsed[study_name]

    def clear(self):
        """
        Completely clear the cache
        """
        self.d_parsed = {}


cache = Cache()
