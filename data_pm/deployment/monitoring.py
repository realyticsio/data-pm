#!/usr/bin/python
# -*- coding: utf-8 -*-

# Global imports
from __future__ import print_function
import sys
import time
import signal
import os
import subprocess

from data_pm.deployment.environments import WorkingDirHelper

from data_pm.core.factory import get_factory
from requests.exceptions import Timeout, ConnectionError
from redis.exceptions import TimeoutError

# Local imports
import data_pm
from data_pm import client
from data_pm.deployment import tasks


def activate_debug(async_result, namespace):
    """
    Attach to the worker with a jupyter console to debug the code
    """
    task_id = async_result.id
    # Wait for the API to return the configuration
    while True:
        try:
            r = client.get_debug(task_id)
            if r.status_code == 404:
                time.sleep(1)
                if async_result.failed():
                    raise tasks.SubmitError(
                        'Could not start task:\n{}'.format(async_result.info),
                        async_result.id)
                continue
            elif r.status_code == 200:
                break
            else:
                raise ValueError('Unexpected status code {}'.format(r.status_code))
        except KeyboardInterrupt:
            break

    # Start a console connected to the worker
    jupyter_dir = data_pm.file.TempDir(prefix='jupyter-', create=True)
    path = os.path.join(jupyter_dir.path, 'config.json')
    with open(path, mode='w', encoding='utf-8') as fd:
        data = r.content
        if isinstance(data, bytes):
            data = data.decode('utf-8')
        fd.write(data)
    try:
        subprocess.check_call(
            ['/usr/local/bin/jupyter', 'console', '--existing',
             os.path.join(jupyter_dir.path, 'config.json')],
            env={'JUPYTER_RUNTIME_DIR': jupyter_dir.path}
        )
    except Exception as e:
        print(e.args)
        revoke_debug(task_id, namespace)
        print('Task revoked')
        return 1
    finally:
        jupyter_dir.remove()

    print('Waiting for task to finish...')
    revoke_debug(task_id, namespace)

    t_start = time.time()

    while not async_result.ready() and time.time() - t_start <= 60:
        try:
            time.sleep(0.5)
        except KeyboardInterrupt:
            break

    # Revoke task if still active
    if not async_result.ready():
        print('Task was not finished, revoking')
        revoke_debug(task_id, namespace)
        print('Task revoked')
        return 1


def revoke_debug(task_id, namespace):
    study_name = namespace.studies[0]
    project_name = namespace.projects[0] if namespace.projects else None
    study_ctrl = get_factory().create_study_control(study_name, project_name, True)
    study = get_factory().create_study(study_ctrl)
    study._log_writer.open(
        branch_name=study.branch_out,
        project_name=study.project_name,
        study_name=study.name
    )
    study._log_writer.set_status('unknown')
    tasks.revoke(task_id)


def activate_monitoring(async_result, stdout=sys.stdout, stderr=sys.stderr):
    """
    Attach to the worker log and display notifications until the task is failed or completed
    """
    wsp_out = WorkerStreamPrinter(stdout, lambda pos: client.get_stream_stdout(async_result.id, pos))
    wsp_err = WorkerStreamPrinter(stderr, lambda pos: client.get_stream_stderr(async_result.id, pos))

    while not async_result.ready():
        try:
            wsp_out.flush()
            wsp_err.flush()
            time.sleep(1)
        except KeyboardInterrupt:
            raise tasks.SubmitError('KeyboardInterrupt', async_result.id)
        except Timeout:
            raise tasks.SubmitError('Log flush failed because API did not respond, '
                                    'request timed out. Task revoked.', async_result.id)
        except TimeoutError:
            raise tasks.SubmitError('Result scaning failed because redis did not respond, '
                                    'request timed out. Task revoked.', async_result.id)

    wsp_out.flush()
    wsp_err.flush()

    tasks.clear_streams(async_result.id)
    WorkingDirHelper().clear(task_id_to_delete=async_result.id)

    if not async_result.ready():
        tasks.revoke(async_result.id)
        return 1, 'Task interrupted'
    elif async_result.failed():
        return 1, 'Task failed'
    elif async_result.successful():
        return 0, 'Task finished successfully'
    else:
        return 1, 'Task finished abnormally: {}'.format(async_result.status)


class WorkerStreamPrinter(object):
    def __init__(self, stream, get_stream_func):
        """
        Fetch a log stream from the API and write it to a local stream.

        Used to display worker logs locally.

        :param file stream: Example: sys.stdout
        :param func get_stream_func:
        """
        self.stream = stream
        self.get_stream_func = get_stream_func
        self.pos = 0

    def flush(self):
        while True:
            data = self._request()
            if len(data) > 0:
                self.stream.write(data)
            if len(data) != 16 * 1024:
                break

    def _request(self):
        try:
            r = self.get_stream_func(self.pos)
        except ConnectionError:
            print('[Warning]: ConnectionError, API did not respond', file=sys.stderr)
            return ''

        if r.status_code != 200:
            print('[Warning]: Could not get log from worker: {}'.format(r.text), file=sys.stderr)
            return ''
        else:
            data = r.text
            if isinstance(data, bytes):
                data = data.decode('utf-8')
            self.pos += len(data)
            return r.text
