Commands
========

General usage
*************
Commands can be launched using ``pm [command] [command_args ...]``.

Launching ``pm`` alone, or ``pm --help``, or ``pm [command] --help`` will display help.


Data PM commands
****************

========================== =============
command                    description
========================== =============
`create_project`           Create the directory tree for a project
`dashboard`                Display the dashboard
`debug`                    Debug a study
`help`                     Open the Data PM documentation
`launch`                   Launch one or multiple study
`list_projects`            List projects
`list_studies`             List studies
`plot`                     Plot a dependency graph of studies
`purge`                    Purge the output of studies
========================== =============


User defined commands
*********************
You can implement new commands or re-implement existing commands by creating a package ``commands``
in the workspace. Do not forget the ``__init__.py`` file.

For a command named "synchronize", the directory tree should look like::

    DEV/
    PROJECTS/
    settings/
    commands/
        __init__.py
        synchronize.py

Here is the canvas for ``commands/synchronize.py``::

    from data_pm.tools.cli_handler import BaseCommand

    class Command(BaseCommand):
        help = "Command help"

        def add_arguments(self, parser):
            parser.add_argument('--mode', default=10, help='Mode number')

        def handle(self, namespace):
            # Run your command here
            # Namespace is the result of argparse command parsing
            my_synchronization(namespace.mode)

