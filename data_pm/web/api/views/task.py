# Global imports
from __future__ import print_function
import flask
import flask_restplus
import argparse
from flask_restplus.fields import String, Integer, List, Boolean

# Local imports
from data_pm.deployment import tasks, environments
from data_pm.deployment.tasks import app


ns = flask_restplus.Namespace('task')

task_status_mdl = ns.model('TaskStatus', {
    'id': String(required=True),
    'status': String(),
    'info': String(),
    'ready': Boolean(),
    'failed': Boolean(),
})

task_submit_mdl = ns.model('TaskSubmit', {
    'branch': String(required=False, default='master'),
    'max_depth_check': Integer(required=False, default=1000),
    'min_depth_force': Integer(required=False, default=0),
    'origin': String(required=False, default='flask'),
    'params': List(String(example='myparam=10'), required=False),
    'projects': List(String(example='myproject')),
    'queue': String(required=False, default='default'),
    'studies': List(String(example='sessions.computing'), required=True),
})


@ns.route('/')
class Index(flask_restplus.Resource):
    @ns.expect(task_submit_mdl, validate=True)
    @ns.marshal_with(task_status_mdl)
    def post(self):
        """
        Submit a task

        Similar to `pm submit --detach`
        """

        request = flask.request.json
        request['branch'] = request.get('branch', 'master')
        request['debug'] = request.get('debug', False)
        request['max_depth_check'] = request.get('max_depth_check', 1000)
        request['min_depth_force'] = request.get('min_depth_force', 0)
        request['origin'] = request.get('origin', 'flask')
        request['params'] = request.get('params', [])
        request['projects'] = request.get('projects', [])
        request['queue'] = request.get('queue', 'default')
        request['slack'] = request.get('slack', None)

        namespace = argparse.Namespace(**request)

        working_dir = environments.WorkingDirHelper()
        working_dir.snapshot_from_branch(namespace.branch)

        # Send task to celery
        task = tasks.launch_studies.apply_async(
            args=[namespace], queue=namespace.queue, serializer='pickle')

        # Upload code snapshot
        try:
            working_dir.upload(task.id)
        except Exception:
            tasks.revoke(task.id)
            raise
        finally:
            working_dir.clear()

        return get_task_status(task.id)


@ns.route('/<task_id>')
class Index(flask_restplus.Resource):
    @ns.marshal_with(task_status_mdl)
    def get(self, task_id):
        """
        Get a task status
        """

        return get_task_status(task_id)

    def delete(self, task_id):
        """
        Revoke task
        """
        tasks.revoke(task_id)

        return {'message': 'ok'}


def get_task_status(task_id):
    t = app.AsyncResult(task_id)
    ready = t.ready()
    failed = t.failed()

    if isinstance(t.info, Exception):
        info = repr(t.info)
    else:
        info = str(t.info)

    out = {'id': task_id, 'ready': ready, 'failed': failed, 'info': info, 'status': t.status}

    return out
