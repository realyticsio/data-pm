===========
Quick Start
===========

``pip>=1.1`` is required to perform the installation. On ubuntu ::

    sudo -E pip install git+https://anon@bitbucket.org/realyticsio/data-pm.git


Create a new workspace (this will create a directory named "my_workspace" in the current path)::

      pm init my_workspace


Create a project::

    pm create_project my_first_project


Create a study: open a new file under the path ``DEV/STUDIES/my_first_study.py``, fill it with this template::

    from __future__ import print_function
    import data_pm

    class Study(data_pm.BaseStudy):
        inputs = {
        }
        outputs = {
        }

        parameters = {
        }

        def run(self):
            print('Hello world')


Install redis::

    sudo apt install redis-server

Start services::

    # Start the API and the dashboard
    pm development server

    # Start the celery worker
    pm development worker


You can now launch this study for your project::

    pm launch my_first_project my_first_study

Congratulations, you just launched your first hello world study!


What to read next
*****************

- :doc:`List of pm commands<commands>`
- :doc:`Launching studies<launch_study>`
