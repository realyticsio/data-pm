import slackclient
from data_pm import settings


def send_start(study):
    if study.is_shared:
        text = 'Shared study {} has just started'.format(study.name)
    else:
        text = 'Study {} has just started for project {}'.format(study.name, study.project_name)
    _send(text)


def send_end(study):
    if study.is_shared:
        text = 'Shared study {} has just finished'.format(study.name)
    else:
        text = 'Study {} has just finished for project {}'.format(study.name, study.project_name)
    _send(text)


def send_error(study, args):
    if study.is_shared:
        text = 'Shared study {} failed\n'.format(study.name)
    else:
        text = 'Study {} failed for project {}\n'.format(study.name, study.project_name)
    text += str(args)
    _send(text)


def _send(text):
    token = settings.get('data_pm.slack_token')
    channel = settings.get('data_pm.slack_channel')
    try:
        client = slackclient.SlackClient(token)
        client.api_call('chat.postMessage', channel=channel, text=text, as_user=True)
    except Exception:
        pass
