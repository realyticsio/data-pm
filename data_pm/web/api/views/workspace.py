import flask_restplus
import flask
import time
import data_pm
import os
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

ns = flask_restplus.Namespace('workspace')


file_upload = flask_restplus.reqparse.RequestParser()
# From file uploads
file_upload.add_argument('file', type=FileStorage, location='files', required=True)


@ns.route('/<task_id>')
class Index(flask_restplus.Resource):
    @ns.expect(file_upload, validate=True)
    def put(self, task_id):
        """
        Push the code from datalab as a tar.gz file
        """
        args = file_upload.parse_args()

        task_id = secure_filename(task_id)
        filename = '{}.tar.gz'.format(task_id)
        path = os.path.join('/tmp', filename)

        if os.path.exists(path):
            os.remove(path)

        tmp = data_pm.file.TempFile()
        with open(tmp.path, 'wb') as fd:
            args['file'].save(fd)
            fd.flush()
            os.fsync(fd.fileno())
        data_pm.file.renames(tmp.path, path)
        tmp.remove()

        return {'message': "ok"}

    def get(self, task_id):
        """
        Get the code as a tar.gz file
        """
        task_id = secure_filename(task_id)
        filename = '{}.tar.gz'.format(task_id)
        path = os.path.join('/tmp', filename)

        # Waiting for file
        t = time.time()
        while not os.path.exists(path) and time.time() - t < 120:
            time.sleep(1)

        if not os.path.exists(path):
            return {'error': '{} does not exist'.format(filename)}, 404

        return flask.send_file(path, mimetype='application/octet-stream',
                               as_attachment=True, attachment_filename=filename)

    def delete(self, task_id):
        task_id = secure_filename(task_id)

        filename = '{}.tar.gz'.format(task_id)
        path = os.path.join('/tmp', filename)

        if os.path.exists(path):
            os.remove(path)

        return {'message': 'ok'}
