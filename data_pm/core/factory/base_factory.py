# Local imports
from data_pm.core.dependencies.plotting_dag import PlottingDAG
from data_pm.core.dependencies.scheduling_dag import SchedulingDAG


class BaseFactory(object):

    def create_studies_from_name(self, study_names, project_names=None):
        studies = [self.create_study_control(x) for x in study_names]

        if not project_names:
            return studies

        studies2 = []
        for study_ctrl in studies:
            if not study_ctrl.is_shared:
                for project_name in project_names:
                    study_ctrl2 = self.create_study_control(study_ctrl.name)
                    study_ctrl2.set_project_name(project_name)
                    studies2.append(study_ctrl2)

        return studies2

    def create_plotting_dag(self, studies=None, study_names=None):
        studies = studies or self.create_studies_from_name(study_names)
        return PlottingDAG.create(studies)

    def create_scheduling_dag(self, working_branch, studies=None, study_names=None,
                              project_names=None, max_depth_check=float('inf'),
                              min_depth_force=0, ignore_parent=False):

        studies = studies or self.create_studies_from_name(study_names, project_names)

        return SchedulingDAG.create(
            working_branch=working_branch,
            cli_studies=studies,
            max_depth_check=max_depth_check,
            min_depth_force=min_depth_force,
            ignore_parent=ignore_parent
        )

    def create_study(self, study_ctrl):
        raise NotImplementedError()

    def create_study_control(self, study_name, project_name=None, configure_branch=False):
        raise NotImplementedError()

    def create_branch_configuration(self):
        raise NotImplementedError()

    def get_project_names(self):
        raise NotImplementedError()
