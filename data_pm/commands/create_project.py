#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Create the directory tree for a project.
"""

# Global imports
from __future__ import print_function
import sys

# Local imports
from data_pm import settings
from data_pm.core import paths, drivers
from data_pm.tools.cli_handler import BaseCommand


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('project')
        parser.add_argument('-y', '--yes', action='store_true', help='Do not ask any confirmation')

    def handle(self, namespace):
        project_name = namespace.project
        yes = namespace.yes

        driver_name = settings['data_pm.redirection.driver']

        print('You are going to create a project named "{}" on the central file system ({})'
              .format(project_name, driver_name))

        if not yes:
            print('Confirm? [Y/n] ')
            confirm_text = sys.stdin.readline().strip()
            if not (len(confirm_text) == 0 or confirm_text in ['y', 'Y']):
                print('Aborting')
                return

        driver = drivers.get_driver(driver_name)
        create_project(project_name, driver)


def create_project(project_name, driver):
    path = paths.path_to_project(driver, project_name)
    if driver.exists(path):
        print('Project already exists')
    else:
        print('Creating directory: "{}" on {}'.format(path, driver.name))
        driver.makedirs(path)
