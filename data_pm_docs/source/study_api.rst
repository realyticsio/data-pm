Study API
=========


data_pm.BaseStudy
*****************
.. autoclass:: data_pm.core.study_components.study.BaseStudy
    :members:


data_pm.Input
*************
.. autoclass:: data_pm.core.study_components.input.Input
    :members:

data_pm.Output
**************
.. autoclass:: data_pm.core.study_components.output.Output
    :members:

data_pm.Parameter
*****************
.. autoclass:: data_pm.core.study_components.parameter.Parameter
    :members:


Shortcut
********
.. autoclass:: data_pm.core.study_components.shortcut.Shortcut
    :members:
