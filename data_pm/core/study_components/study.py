# Global imports
import sys
import signal
import logging

# Local imports
import data_pm
from data_pm.tools import slack
from data_pm.core.study_components import study_control, study_log
from data_pm.core import redirector


def require_configuration(func):
    def wrapper(self, *args, **kwargs):
        assert self.branch_out is not None, 'The study branch name was not set'

        if self.is_shared:
            assert self.project_name is None, \
                'The project name was set whereas "{}" is a shared study'.format(self.name)
        else:
            assert self.project_name is not None, \
                'The project name was NOT set for study "{}"'.format(self.name)

        return func(self, *args, **kwargs)
    return wrapper


class BaseStudy(study_control.StudyControl):
    """
    Base study every study should inherit from

    :attr:`inputs`, :attr:`outputs`, :attr:`parameters` and :func:`run` should be reimplemented.
    The other attributes can be used as read-only in :func:`run`.
    """

    def __init__(self):
        # Parse module docstring
        if self.__module__ not in sys.modules:
            raise RuntimeError('Module {} is not loaded while its study is being instantiated'.format(self.__module__))
        docstring = sys.modules[self.__module__].__doc__

        try:
            d_meta = study_control.parse_docstring(docstring)
        except Exception:
            data_pm.logger.error('Could not parse docstring for study "{}"'.format(self.__module__))
            raise

        # Determine study short name and whether the study is shared
        if self.__module__.startswith('DEV.STUDIES.'):
            study_name = self.__module__.replace('DEV.STUDIES.', '')
            is_shared = False
        elif self.__module__.startswith('DEV.SHARED_STUDIES.'):
            study_name = self.__module__.replace('DEV.SHARED_STUDIES.', '')
            is_shared = True
        else:
            raise ValueError('Could not determine whether study "{}" is shared'.format(self.__module__))

        # Parse metadata
        try:
            d_args = study_control.StudyControl.parse_meta(**d_meta)
        except:
            data_pm.logger.error('Could not parse study metadata for "{}"'.format(study_name))
            raise

        # Initialize study metadata
        study_control.StudyControl.__init__(self, name=study_name, is_shared=is_shared, **d_args)

        self.logger = logging.getLogger('data-pm')
        """A logger from the logging module. Can be used with `self.logger.info('message')` for example."""

        self._log_writer = study_log.StudyLogWriter(self.logger)

        self._redirector = redirector.Redirector(self, self._log_writer)
        """Redirector: prepare inputs/outputs before and after study execution"""

        self.origin = None
        """:type: str, user's name which launched the study"""

        self.warning_activated = False
        """:type bool, send status "warning" to API if True"""

    def __repr__(self):
        if self.is_shared:
            return '<Study {} on {}>'.format(self.name, self.branch_out)
        else:
            return '<Study {} for {} on {}>'.format(self.name, self.project_name, self.branch_out)

    def set_parameters(self, d_params):
        """
        Set parameters values

        :param dict d_params: dict[param_name,value]
        """
        for k, v in d_params.items():
            if k in self.parameters:
                self.parameters[k].set_value(v)

    def warn(self, message):
        self.logger.warning(message)
        self.warning_activated = True

    @require_configuration
    def prepare_launch(self, d_params=None):
        """
        # Configure inputs
        for i in self.inputs.values():
            i.set_configuration(self.name, self.project_name)

        # Configure outputs
        for o in self.outputs.values():
            o.set_configuration(self.name, self.project_name)

        # Configure parameters
        for param_name, p in self.parameters.ritems():
            p.configure(param_name, self.name, self.project_name)
        """

        # Open log writer
        self._log_writer.open(
            branch_name=self.branch_out,
            project_name=self.project_name,
            study_name=self.name
        )
        self._log_writer.set_start()
        self._log_writer.set_hostname()
        self._log_writer.set_origin(self.origin)
        self._log_writer.set_status('unknown')

        slack.send_start(self)

        # Set compatible parameters from d_params argument
        if d_params is not None:
            self.set_parameters(d_params)

    @require_configuration
    def launch(self, d_params=None):
        try:
            signal.signal(signal.SIGTERM, self._sigterm_handler)

            self.prepare_launch(d_params)

            self._log_writer.set_status('running')
            self._log_writer.flush()

            self._redirector.activate()

            drivers_used = []
            # Create the output directory on every necessary filesystem
            for o in self.outputs.values():
                if o.driver_name not in drivers_used:
                    try:
                        o.driver.makedirs(o.driver.dirname(o.path))
                    except NotImplementedError:
                        pass
                    drivers_used.append(o.driver_name)

            # Run the study
            self.run()

            self._redirector.deactivate()

            if self.mtimes_up_to_date():
                if self.warning_activated:
                    self._log_writer.set_status('warning')
                else:
                    self._log_writer.set_status('success')
                self._log_writer.set_last_update()
            else:
                self.logger.warning('The study did not update all its outputs')
                self._log_writer.set_status('warning')

            slack.send_end(self)

        except Exception as e:
            self._log_writer.set_status('failed')
            self.logger.exception('Uncaught exception')
            slack.send_error(self, e.args)
            raise StudyError(e.args, study_name=self.name, project_name=self.project_name)

        except KeyboardInterrupt:
            self._log_writer.set_status('stopped')
            raise KeyboardInterrupt

        finally:
            self._log_writer.set_end()
            self._log_writer.close()
            self._redirector.clear()

    def _sigterm_handler(self, signal, frame):
        self._log_writer.set_status('stopped')
        self._redirector.clear()
        sys.exit(0)

    def run(self):
        """
        Main function containing the actual code of the study

        This function should create all the outputs declared in :attr:`.outputs`.
        Parameters can be accessed in this function with `self.p.name`.
        Input data can be accessed in this function with `self.i.name`.
        """
        self.logger.warning('The launch method was not reimplemented in the inherited class')

    def validate(self):
        self.logger.warning('The validate method was not reimplemented in the inherited class')


class StudyError(BaseException):
    def __init__(self, message, project_name, study_name, *args, **kwargs):
        super(StudyError, self).__init__(message, *args, **kwargs)
        self.message = message
        self.project_name = project_name
        self.study_name = study_name

    def print_error(self):
        logger = logging.getLogger('data-pm')
        if self.project_name is None:
            logger.critical('Shared study {} failed'.format(self.study_name))
        else:
            logger.critical('Study {} failed for project {}'.format(self.study_name, self.project_name))
