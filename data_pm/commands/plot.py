#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Plot and display a graph of the relations between studies.

As it may be hard to read, study names may be specified to plot their dependency tree.

"""

# Global imports
import subprocess
import sys

# Local imports
import data_pm
from data_pm.core import study_mngr, project_mngr
from data_pm.core.factory import get_factory, set_factory, test_factory
from data_pm.tools import git_helper
from data_pm.tools.cli_handler import BaseCommand, refactor


colors = ['black', 'orange', 'royalblue3', 'green3', 'red3', 'purple3']
dot_file_path = '/tmp/dot.dot'
png_file_path = '/tmp/dot.png'


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('projects_or_studies', nargs='+', metavar='PROJECT OR STUDY')
        # parser.add_argument('studies', nargs='*', metavar='STUDY')

        parser.add_argument('--min-depth-force', type=int, default=0,
                            help='Force compute studies deeper than this value. '
                                 'By default, only force compute the provided studies.')
        parser.add_argument('--max-depth-check', type=int, default=1000,
                            help='Do not check the status of studies deeper than this value. '
                                 'By default, check as deep as possible (1000).')
        parser.add_argument('--ignore-parent', action='store_true',
                            help='Do not check the status of studies of parent branch.')
        parser.add_argument('--dry-run', action='store_true',
                            help='List studies to run')
        parser.add_argument('--dry-plot', action='store_true',
                            help='Plot studies to run. Red node if study is scheduled, green otherwise')

        parser.add_argument('--viewer', default='/usr/bin/eog', help='Path to viewer executable')

    def handle(self, namespace):
        # set_factory(test_factory.TestFactoryShared())
        factory = get_factory()

        repo = git_helper.get_repo('.')
        branch_name = git_helper.get_current(repo)

        # Preparing colors for branches
        branch_config = factory.create_branch_configuration()
        branches = branch_config.get_hierarchy().keys()
        d_colors = {b: colors[i] for i, b in enumerate(reversed(branches))}

        if namespace.dry_run or namespace.dry_plot:
            # --------------------------------- #
            # DRY - Look for up-to-date studies #
            # --------------------------------- #
            all_projects = set(project_mngr.get_project_names())
            refactor(namespace, all_projects)

            dag = factory.create_scheduling_dag(
                working_branch=branch_name,
                study_names=namespace.studies,
                project_names=namespace.projects,
                max_depth_check=namespace.max_depth_check,
                min_depth_force=namespace.min_depth_force,
                ignore_parent=namespace.ignore_parent
            )
            studies_to_run = list(dag.studies_to_run())

            for study_ctrl, data in studies_to_run:
                print(study_ctrl)

            print('Number of studies to run: {}'.format(len(studies_to_run)))

            dot = dag.to_dot(d_colors)
        else:
            # ------------------------ #
            # PLOT - Plot dependencies #
            # ------------------------ #
            namespace.studies = namespace.projects_or_studies
            # assert all([x in study_mngr.get_all_study_names() for x in namespace.studies]), 'Study unknown'

            dag = factory.create_plotting_dag(study_names=namespace.studies)

            dot = dag.to_dot(d_colors)

        if not namespace.dry_run:
            print('Colors legend: {}'.format(d_colors))

            with open(dot_file_path, 'w+') as dot_file:
                dot_file.write(dot)

            subprocess.call('dot -Tpng {} > {}'.format(dot_file_path, png_file_path), shell=True)
            subprocess.call('{} {}'.format(namespace.viewer, png_file_path), shell=True)
