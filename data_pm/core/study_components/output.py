# Global imports
import time

# Local imports
from data_pm import settings as default_settings
from data_pm.core import drivers, paths
from data_pm.core.study_components import study_configuration
from data_pm.tools import sync


class Output(object):
    def __init__(self, name, alias=None, driver='file', use_tmp_dir=True, purge=True,
                 settings=None, description=None, view=False):
        self.alias = alias if alias is not None else name
        self.name = name
        self.driver_name = driver
        self.use_tmp_dir = use_tmp_dir
        self.purge = purge
        self.description = description
        self.view = view
        self._tmpdest = None
        self._destination = None
        self._resource = None
        self._driver = None
        self._activated = False

        self.ext = '.' + self.name.split('.')[-1] if '.' in self.name else ''
        """File extension. Example: ".csv" for "myfile.csv", "" for "myfile" """

        self._settings = settings or default_settings

        self.study_control = None
        """:type: data_pm.core.study_control.StudyControl"""

    @property
    def driver(self):
        if self._driver is None:
            self._driver = drivers.get_driver(self.driver_name, self._settings)

        return self._driver

    @property
    @study_configuration.require_output_branch_set
    @study_configuration.require_project_set
    def resource(self):
        """
        :rtype: Resource
        """
        if self._resource is None:
            driver_name = drivers.redirect(self.driver_name).name
            self._resource = Resource(self, driver_name, default_settings)

        return self._resource

    def get_mtime(self):
        """
        Update the modification time of the resource (not the destination)

        :return mtime: seconds since 1st january 1970
        """

        return self.resource.get_mtime()

    @study_configuration.require_output_branch_set
    @study_configuration.require_project_set
    def activate(self):
        if self._activated:
            raise ValueError('output must be deactivated when trying to activate it')

        try:
            self._destination = Destination(self, self._settings)
            self._destination.activate()
        except Exception as e:
            raise Exception('Could not load output "{}". "{}"'
                            .format(self.name, e.args))

        if self.use_tmp_dir:
            self._tmpdest = TmpDest(self, self._settings)
            self._tmpdest.activate()

        self._activated = True

    def deactivate(self):
        if not self._activated:
            raise ValueError('destination must be activated when trying to deactivate it')

        try:
            if self._tmpdest:
                self._tmpdest.deactivate()
                self._tmpdest = None
        except Exception as e:
            raise Exception('Could not move temporary output "{}" to its destination. "{}"'
                            .format(self.name, e.args))

        try:
            self._destination.deactivate()
            self._destination = None
        except Exception as e:
            raise Exception('Could not move destination "{}" to its storage location. "{}"'
                            .format(self.name, e.args))

        self._activated = False

    def clear(self):
        if self._tmpdest is not None:
            self._tmpdest.clear()
        if self._destination is not None:
            self._destination.clear()

    def remove(self):
        self.resource.remove()

    @property
    def path(self):
        if not self._activated:
            raise ValueError('output must be activated to access its path')

        if self._tmpdest is not None:
            return self._tmpdest.path

        return self._destination.path

    @property
    def destination(self):
        if self._destination is None:
            raise ValueError('output must be activated to access its destination')

        return self._destination.path

    def force_update(self, timestamp=None):
        """
        Update the modification time of the destination (not the resource)

        :param timestamp: seconds since 1st january 1970
        """

        if not self._activated:
            raise ValueError('output must be activated to call force_update')

        self.driver.setmtime(self.destination, timestamp or time.time())

    def configuration_changed(self):
        if self._tmpdest is not None:
            raise RuntimeError('Configuration changed while tmp was activated')


class Resource:
    def __init__(self, output, driver_name, _settings=None):
        """

        :param Output output:
        :param str driver_name:
        :param dict _settings:
        """
        self.driver_name = driver_name
        self.driver = drivers.get_driver(driver_name, _settings)
        self._output = output

    def get_mtime(self):
        return self.driver.getmtime(self.path)

    def set_mtime(self, mtime):
        self.driver.setmtime(self.path, mtime)

    def exists(self):
        return self.driver.exists(self.path)

    def remove(self):
        if self.exists():
            self.driver.remove(self.path, recursive=True)

    @property
    def path(self):
        p = paths.path_to_output(driver=self.driver, branch_name=self._output.study_control.branch_out,
                                 study_name=self._output.study_control.name, output_name=self._output.name,
                                 project_name=self._output.study_control.project_name)
        return self.driver.abspath(p)


class Destination:
    def __init__(self, output, _settings=None):
        self.driver_name = output.driver_name
        self.driver = drivers.get_driver(output.driver_name, _settings)
        self._output = output
        self._resource = output.resource
        self._tmp = None
        self._activated = False

    def activate(self):
        if self._activated:
            raise ValueError('destination must be deactivated when trying to activate it')

        if drivers.need_redirection(self.driver_name):
            self._tmp = self.driver.TempDir(create=False, suffix=self._output.ext)

            if self._resource.exists() and not self._output.use_tmp_dir:
                # Synchronize resource and destination, preserves modification times for dependencies
                sync.synchronize(self._resource.path, self._resource.driver, self._tmp.path, self.driver,
                                 preserve_mtimes=True)

        self._activated = True

    def deactivate(self):
        if not self._activated:
            raise ValueError('destination must be activated when trying to deactivate it')

        if drivers.need_redirection(self.driver_name) and self.driver.exists(self.path):
            tmp_storage = self._resource.driver.TempDir(destination=self._resource.path)

            # Synchronize destination and resource, preserves modification times for dependencies
            sync.synchronize(self.path, self.driver, tmp_storage.path, self._resource.driver, preserve_mtimes=True)
            tmp_storage.move_to_destination()

            self.clear()

        self._activated = False

    def clear(self):
        if self._tmp is not None:
            self._tmp.remove()
            self._tmp = None

    @property
    def path(self):
        if not self._activated:
            raise ValueError('destination must be activated when trying to access its path')

        if self._tmp is not None:
            return self._tmp.path

        return self._resource.path


class TmpDest:
    def __init__(self, output, _settings=None):
        self.driver_name = output.driver_name
        self.driver = drivers.get_driver(output.driver_name, _settings)
        self._output = output
        self._tmp = None

    def activate(self):
        if self._tmp is not None:
            raise ValueError('tmp path must be deactivated when trying to activate it')

        self._tmp = self.driver.TempDir(destination=self._output.destination, suffix=self._output.ext)

    def deactivate(self):
        if self._tmp is None:
            raise ValueError('tmp path must be activated when trying to deactivate it')

        if self.driver.exists(self._tmp.path):
            if self._output.purge:
                self._tmp.move_to_destination()
            else:
                self._tmp.move_content_to_destination()
                self._output.force_update()  # In case no new content was added

        self.clear()

    def clear(self):
        if self._tmp is not None:
            self._tmp.remove()
            self._tmp = None

    @property
    def path(self):
        if self._tmp is None:
            raise ValueError('tmp path must be activated when trying to access its path')

        return self._tmp.path
