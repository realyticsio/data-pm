from __future__ import print_function

# Global imports
import sys
import os
import queue
import signal
import multiprocessing
from multiprocessing import cpu_count


class Bunch(object):
    def __init__(self):
        self.lists_of_args = []

    def add(self, args):
        self.lists_of_args.append(args)

    def get(self):
        return self.lists_of_args

    def size(self):
        return len(self.lists_of_args)


class BunchSequencer(object):
    def __init__(self, processes=cpu_count(), dry=False, critical=False):
        self.processes = processes
        """Number of processes in parallel"""

        self.dry = dry
        """Dry mode. When true, only print commands"""

        self.critical = critical
        """When True, stop execution at the first failure"""

        self.bunches = [Bunch()]
        """list[Bunch] Bunches will run sequentially, inside tasks will run in parallel"""

        self.routine = None
        """Function to apply on arguments, must return tuple(code status, message) (0 if process terminates well)"""

        self._checkpoint = False
        """If True, create a new bunch before adding a task"""

    def add(self, *args):
        """
        :param iterable[str] args: arguments
        """

        if self._checkpoint:
            self.bunches.append(Bunch())
            self._checkpoint = False

        current_bunch = self.bunches[-1]
        current_bunch.add(list(args))

        return self

    def add_all(self, ll_args):
        """
        :param list[list[str]] ll_args: several lists of arguments
        """

        for args in ll_args:
            self.add(*args)

        return self

    def checkpoint(self):
        """
        Close current bunch and create a new empty bunch
        """
        self._checkpoint = True
        return self

    @staticmethod
    def _consumer(func, job_queue, result_queue, lock):
        signal.signal(signal.SIGINT, signal.SIG_IGN)
        signal.signal(signal.SIGTERM, get_terminate())
        while not job_queue.empty():
            try:
                args = job_queue.get(block=False)
                signal.signal(signal.SIGTERM, get_terminate(*args))
                try:
                    rv, msg = func(lock, *args)
                except Exception as e:
                    rv, msg = 1, str(e)
                result_queue.put((rv, msg))
            except queue.Empty:
                pass

    def run_bunch(self, bunch):
        func = routine_dry if self.dry else self.routine

        lock = multiprocessing.Lock()
        job_queue = multiprocessing.Queue()
        result_queue = multiprocessing.Queue()

        for args in bunch.get():
            job_queue.put(args)

        workers = []
        for i in range(self.processes):
            process = multiprocessing.Process(
                target=self._consumer,
                args=(func, job_queue, result_queue, lock)
            )
            process.start()
            workers.append(process)

        try:
            for i in range(bunch.size()):
                rv, msg = result_queue.get(block=True)
                if rv == 0:
                    continue
                if not self.critical:
                    print('[Warning] Failure: {}'.format(msg))
                else:
                    raise RuntimeError(
                        'Process exit with status code {}, message: {}'.format(rv, msg)
                    )
            for worker in workers:
                worker.join()
        except KeyboardInterrupt:
            for worker in workers:
                worker.terminate()
                worker.join()
            raise
        except Exception:
            for worker in workers:
                worker.terminate()
                worker.join()
            raise

    def run(self):
        for bunch in self.bunches:
            self.run_bunch(bunch)
            print('Checkpoint')
        print('Quitting normally')

    def set_routine(self, func):
        self.routine = func
        return self


def routine_dry(lock, *args):
    lock.acquire()
    print('[pid={}] args={}'.format(os.getpid(), args))
    lock.release()
    return 0, 'success'


def get_terminate(*args):
    def terminate(sig, frame):
        print('[pid={}] args={} is terminating'.format(os.getpid(), args))
        sys.exit(0)
    return terminate
