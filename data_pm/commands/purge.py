#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Purge the outputs of studies.

Remove PROJECTS/[project_name]/DATA/[branch_name]/[study_name]
and/or SHARED/DATA/[branch_name]/[study_name].
Use with caution, there is no backup.
"""

# Global imports
from __future__ import print_function
import sys

# Local imports
from data_pm import settings, client
from data_pm.core import study_mngr, project_mngr, paths
from data_pm.core.drivers import get_driver
from data_pm.tools.cli_handler import BaseCommand


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def __init__(self, parsers, parser):
        super(Command, self).__init__(parsers, parser)
        self.all_regular_study_names = study_mngr.get_study_names()
        self.all_shared_study_names = study_mngr.get_shared_study_names()
        self.all_project_names = project_mngr.get_project_names()

    def add_arguments(self, parser):
        parser.add_argument('projects_or_studies', nargs='+', metavar='PROJECT OR STUDY',
                            choices=self.all_regular_study_names + self.all_shared_study_names + self.all_project_names)
        parser.add_argument('--branch', type=str, required=True,
                            help='Delete data from this branch')

    def handle(self, namespace):
        # Filter inputs
        regular_study_names = set(namespace.projects_or_studies) & set(self.all_regular_study_names)
        shared_study_names = set(namespace.projects_or_studies) & set(self.all_shared_study_names)
        project_names = set(namespace.projects_or_studies) & set(self.all_project_names)

        if regular_study_names and shared_study_names:
            raise ValueError('Do not mix shared and regular studies, purge cancelled.')
        elif shared_study_names and project_names:
            raise ValueError('Do not mix shared studies and projects, purge cancelled.')
        elif regular_study_names and not project_names:
            project_names = self.all_project_names
        elif not regular_study_names and project_names:
            regular_study_names = self.all_regular_study_names

        study_names = shared_study_names or regular_study_names

        paths_to_delete = dict()

        for driver_name in settings['data_pm.drivers']:
            driver = get_driver(driver_name)
            for study_name in study_names:
                if project_names:
                    for project_name in project_names:
                        t = (namespace.branch,
                             study_name,
                             project_name)
                        path = paths.path_to_study(driver, *t)
                        if driver.exists(path):
                            paths_to_delete[t] = (driver, path)
                else:
                    t = (namespace.branch,
                         study_name,
                         None)
                    path = paths.path_to_study(driver, *t)
                    if driver.exists(path):
                        paths_to_delete[t] = (driver, path)

        size = len(paths_to_delete.keys())

        if size == 0:
            exit_('Nothing to delete.')

        print(
            '@@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@\n'
            'WARNING: You are going to remove {} outputs '
            'on branch \033[41m\033[1m\033[7m{}\033[0m (here is the first ones):\n'
            '\n  {}\n\n'
            '@@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@ WARNING @@@@@@\n'
            .format(size, namespace.branch, '\n  '.join([p for d, p in paths_to_delete.values()][:20]))
        )

        confirm_or_exit()

        s = '{0:0' + str(len(str(size))) + 'd}'

        # Delete outputs, irreversible
        for i, (t, (driver, path)) in enumerate(paths_to_delete.items()):
            print('{}/{} [delete] {}'.format(s.format(i + 1), size, path))
            driver.remove(path, recursive=True)
            client.post_last_update_nullify(dict(
                branch=t[0],
                study=t[1],
                project=t[2]
            ))


def confirm_or_exit():
    try:
        sys.stdout.write('\nPlease confirm by writing "yesremoveallthis[ENTER]": ')
        confirm_text = sys.stdin.readline().strip()

        if confirm_text != 'yesremoveallthis':
            exit_('Purge cancelled.')

    except KeyboardInterrupt:
        exit_('Purge cancelled.')


def exit_(msg):
    print(msg)
    sys.exit(0)
