# Global imports
import flask_restplus
from flask_restplus import reqparse
from datetime import datetime, timedelta
from sqlalchemy.sql import func

# Local imports
from data_pm.web.api.models import StudyLog
from data_pm.core import study_mngr
from data_pm.web.api.backend import get_study_logs

ns = flask_restplus.Namespace('status')

parser = reqparse.RequestParser()
parser.add_argument('branch', type=str, required=True)
parser.add_argument('studies', type=str, required=True)
parser.add_argument('project', type=str)
parser.add_argument('today', type=str)


def fetch_statuses(studies, branch, project, today=False):
    """
    :param set[str] studies:
    :param str branch:
    :param str project: if None, status is fetched for all projects
    :param bool today: if True, only returns statuses for studies which start today
    :return: list[(study_name, project_name, status, date)]
    """

    from data_pm.web.api.backend import session

    query = session.query(StudyLog.name, StudyLog.project, StudyLog.status, func.date(StudyLog.start_date)) \
        .filter_by(branch=branch) \
        .filter(StudyLog.name.in_(studies))

    if project is not None:
        query = query.filter_by(project=project)

    if today:
        query = query.filter(func.date(StudyLog.start_date) == datetime.now().date())

    return query.all()


def get_filtered_study_logs(branch_name=None, study_name=None, project_name=None,
                            status=None, shared_only=False, project_only=False, today=False):
    """Read logs from database and filter non-existing studies"""

    study_names = study_mngr.get_all_study_names()

    rows = get_study_logs(
        branch_name=branch_name,
        study_name=study_name,
        project_name=project_name,
        status=status,
        shared_only=shared_only,
        project_only=project_only,
        today=today
    )

    filtered_row = [r for r in rows if r.name in study_names]

    return filtered_row


@ns.route('/')
class Index(flask_restplus.Resource):
    def get(self):
        """
        Get current statuses for a list of studies and a given branch
        """

        args = parser.parse_args(strict=True)
        branch = args['branch']
        studies = args['studies'].split(',')
        project = args['project']
        today = args['today'] == 'True'

        flatten_statuses = fetch_statuses(studies, branch, project, today=today)
        """list[(study_name, project_name, status, date)]"""

        nested_statuses = {
            study: {
                p: status for s, p, status, date in flatten_statuses if s == study
            } for study in studies
        }
        """dict[study_name, dict[project_name, [status,date]]"""

        return nested_statuses


@ns.route('/dates')
class Index(flask_restplus.Resource):
    def get(self):
        """
        Get current statuses for a list of studies and a given branch
        """

        args = parser.parse_args(strict=True)
        branch = args['branch']
        studies = args['studies'].split(',')
        project = args['project']
        today = args['today'] == 'True'

        flatten_statuses = fetch_statuses(studies, branch, project, today=today)
        """list[(study_name, project_name, status, date)]"""

        nested_statuses = {
            study: {
                p: [status, date] for s, p, status, date in flatten_statuses if s == study
            } for study in studies
        }
        """dict[study_name, dict[project_name, [status,date]]"""

        return nested_statuses


@ns.route('/durations')
class Index(flask_restplus.Resource):
    def get(self):
        """
        Get start date and ending date of studies
        """

        args = parser.parse_args(strict=True)
        branch = args['branch']
        studies = args['studies']
        project = args['project']
        today = args['today'] == 'True'

        duration_logs_flattened = get_filtered_study_logs(
            branch_name=branch,
            study_name=studies,
            project_name=project,
            status=None,
            shared_only=False,
            project_only=False,
            today=today,
        )
        """list[Studylogs(), Studylogs(), ..., Studylogs()]"""
        nested_duration_logs = [
            {
                log.project: [log.name, int(round(log.start_date.timestamp())), int(round(log.end_date.timestamp()))]
            } for log in duration_logs_flattened
        ]
        return nested_duration_logs
