# Global imports
import json
import networkx as nx
from itertools import groupby

# Local imports
from data_pm import client
from data_pm.core.dependencies import base_dag


class SchedulingDAG(base_dag.BaseDAG):
    """
    Graph for scheduling, a node is like <study_control.StudyControl>

    Important: Project name should be configured for studies in ``self.studies``.
    """

    def __init__(self, working_branch, cli_studies, max_depth_check,
                 min_depth_force, ignore_parent):
        super(SchedulingDAG, self).__init__(
            cli_studies=cli_studies,
            max_depth_check=max_depth_check,
            min_depth_force=min_depth_force,
            ignore_parent=ignore_parent
        )
        self.working_branch = working_branch
        self.saved_last_updates = None

    @staticmethod
    def create(working_branch, cli_studies, max_depth_check,
               min_depth_force, ignore_parent):

        dag = SchedulingDAG(
            working_branch=working_branch,
            cli_studies=cli_studies,
            max_depth_check=max_depth_check,
            min_depth_force=min_depth_force,
            ignore_parent=ignore_parent
        )

        for study in cli_studies:
            dag.populate(study)

        dag.G = dag.compress()

        dag.compute_distances()
        dag.spread_branches()

        # Configure studies
        for n, d in dag.G.nodes.data():
            datanode = d['datanode']
            for s in datanode.studies:
                if s.branches_in is None:
                    s.set_branches_in(datanode.branches_in)
                if s.branch_out is None:
                    s.set_branch_out(datanode.branch_out)

        # Fetch last updates before the cut off
        last_updates = get_last_updates(dag)
        dag.saved_last_updates = last_updates

        dag.cut_off()

        dag.spread_cli()
        dag.spread_out_of_date(last_updates)

        return dag

    def compress(self):
        g = nx.DiGraph()

        def add_node(study):
            if study.name in g:
                g.nodes[study.name]['datanode'].add_study(study)
            else:
                datanode = base_dag.DataNode(study.name, study.is_shared).add_study(study)
                g.add_node(study.name, datanode=datanode)

        for n in self.G.nodes():
            add_node(n)

        for s1, s2 in self.G.edges():
            g.add_edge(s1.name, s2.name)

        return g

    def compute_distances(self):
        self.G = compute_distances(self.G)

    def groups_to_run(self):
        d_groups = {}
        for study, data in self.studies_to_run():
            key = (
                data['datanode'].branch_out,
                data['datanode'].max_depth
            )
            if key not in d_groups:
                d_groups[key] = []
            d_groups[key].append((study, data))

        groups = sorted(d_groups.items(), key=lambda k_gr: -k_gr[0][1])
        groups = [k_gr[1] for k_gr in groups]

        return groups

    def populate(self, study):
        n_current = study
        self.G.add_node(n_current)

        for i in study.get_inputs().values():
            n_parent = i.parent_study

            if n_parent not in self.G.nodes():
                # Explore node only if it has never been seen
                self.populate(n_parent)

            self.G.add_edge(n_parent, n_current)

    def cut_off(self):
        """
        Apply max-depth-check and ignore-parent parameters
        """
        nodes = []
        for n, d in self.G.nodes.data():
            if d['datanode'].max_depth > self.max_depth_check:
                nodes.append(n)
            elif self.ignore_parent and d['datanode'].branch_out != self.working_branch:
                nodes.append(n)
        self.G.remove_nodes_from(nodes)

    def spread_cli(self):
        for s in self.cli_studies:
            try:
                datanode = self.G.nodes[s.name]['datanode']
                datanode.add_forced_by_cli(s.project_name)
            except KeyError as e:
                e.args = ('"{}" not in the graph. Try increasing max-depth-check.'.format(s.name),)
                raise e

        sources = [s.name for s in self.cli_studies]
        edges = list(nx.edge_dfs(self.G, sources))

        for u, v in edges:
            data_u = self.G.nodes[u]['datanode']
            data_v = self.G.nodes[v]['datanode']
            if data_u.is_shared or data_v.is_shared:
                data_v.all_forced_by_cli()
            else:
                data_v.forced_by_cli |= data_u.forced_by_cli

        # min-depth-force
        for n, d in self.G.nodes.data():
            datanode = d['datanode']
            if datanode.max_depth <= self.min_depth_force:
                datanode.all_forced_by_cli()

    def spread_out_of_date(self, last_updates):
        for n, d in self.G.nodes.data():
            datanode = d['datanode']
            for s in datanode.studies:
                if not s.status_up_to_date(last_updates):
                    datanode.add_out_of_date(s.project_name)

        sources = [n for n, d in self.G.nodes.data() if len(d['datanode'].out_of_date) > 0]
        edges = list(nx.edge_dfs(self.G, sources))

        for u, v in edges:
            data_u = self.G.nodes[u]['datanode']
            data_v = self.G.nodes[v]['datanode']

            if len(data_u.out_of_date) == 0:
                continue

            if data_u.is_shared or data_v.is_shared:
                data_v.all_out_of_date()
            else:
                data_v.out_of_date |= data_u.out_of_date

    def studies_to_run(self):
        # Order studies by (branch index, -max_depth, not is_shared)
        # first recompute the furthest ones
        l_branches = list(reversed(self.d_branches.keys()))

        nodes = sorted(
            self.G.nodes.data(),
            key=lambda n_d: (
                l_branches.index(n_d[1]['datanode'].branch_out),
                -n_d[1]['datanode'].max_depth,
                not n_d[1]['datanode'].is_shared
            )
        )

        for node, data in nodes:
            datanode = data['datanode']
            for study_ctrl in datanode.studies:
                if study_ctrl.project_name in datanode.forced_by_cli:
                    yield study_ctrl, data
                elif study_ctrl.project_name in datanode.out_of_date:
                    yield study_ctrl, data

    @staticmethod
    def nodes_to_dot(graph, d_colors):
        """Generates nodes to dot format"""

        for node, data in graph.nodes.data():
            datanode = data['datanode']

            shape = 'box' if datanode.is_shared else 'ellipse'
            fillcolor = 'pink' if datanode.forced_by_cli or datanode.out_of_date else 'palegreen2'

            dot_node = '"{}" [color={}] [fillcolor={}] [style=filled] [shape={}]' \
                .format(node, d_colors[datanode.branch_out], fillcolor, shape)

            yield dot_node


def compute_distances(g):
    """
    Add max_depth properties to each node
    max_depth: length of the longest path to the source
    """

    g.add_edges_from(g.edges, weight=-1)

    # sources = base_dag.get_sources_list(g)
    # longuest_path(g, sources, 'scheduling_number')

    g_rev = g.reverse(copy=False)

    leafs = base_dag.get_sources_list(g_rev)
    longuest_path(g_rev, leafs)

    for d in g.edges.values():
        d.pop('weight', None)

    return g


def longuest_path(g, sources):
    """Needs parameter weight=-1 on edges"""
    # Compute the longest path from single source
    # Works because the graph is oriented and acyclic
    for source in sources:
        _, longest = nx.bellman_ford_predecessor_and_distance(g, source)

        for n in longest.keys():
            max_depth = g.nodes[n]['datanode'].max_depth
            new_depth = -longest[n]

            if max_depth is None or new_depth > max_depth:
                g.nodes[n]['datanode'].set_max_depth(new_depth)


def get_last_updates(dag):
    """
    Generate a dictionary request and send it to the API.
    """
    d_query = {}

    for study, data in dag.G.nodes.data():
        datanode = data['datanode']
        branch = datanode.branch_out
        for s in datanode.studies:

            if datanode.is_shared:
                if branch not in d_query:
                    d_query[branch] = {}

                if study not in d_query[branch]:
                    d_query[branch][study] = None

            else:
                project = s.project_name

                if branch not in d_query:
                    d_query[branch] = {}

                if study not in d_query[branch]:
                    d_query[branch][study] = {}

                if project not in d_query[branch][study]:
                    d_query[branch][study][project] = None

    r = client.post_last_update_query(d_query)

    if r.status_code != 200:
        raise Exception(
            'Error when getting last updates: {}, {}'
            .format(r.status_code, r.reason)
        )

    data = r.content
    if isinstance(data, bytes):
        data = data.decode('utf-8')

    last_updates = json.loads(data)

    return last_updates
