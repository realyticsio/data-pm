#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Debug a study.

Enter an IPython shell as if it was the beginning of the study :func:`run`.
Specify either a project and a study, or a shared study.
"""

# Global imports
from __future__ import print_function
import os
import sys
import json
import data_pm
import argparse
import ipykernel
from ipykernel.kernelapp import IPKernelApp
from jupyter_console.app import ZMQTerminalIPythonApp

# Local imports
from data_pm import client
from data_pm.tools.cli_handler import BaseCommand
from data_pm.core import study_mngr, project_mngr
from data_pm.core.factory import get_factory


class MyAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        all_projects = set(project_mngr.get_project_names())
        all_studies = set(study_mngr.get_all_study_names())
        setattr(namespace, 'projects', [v for v in values if v in all_projects])
        setattr(namespace, 'studies', [v for v in values if v in all_studies])


class Command(BaseCommand):
    help = sys.modules[__module__].__doc__

    def add_arguments(self, parser):
        parser.add_argument('projects_or_studies', nargs='+',
                            choices=project_mngr.get_project_names() + study_mngr.get_all_study_names(),
                            action=MyAction, metavar='PROJECT OR STUDY')
        parser.add_argument('-p', '--params', dest='params', nargs='*', type=str,
                            help='Parameters in the form myparam=myvalue')
        parser.add_argument('--task-id', dest='task_id', type=str,
                            help='Task id (if launched by a worker)')
        parser.add_argument('--origin', dest='origin', type=str, default=None,
                            help='The user which issues this task')

    def handle(self, namespace):
        if len(namespace.studies) > 1:
            print('Please specify one study name')
            sys.exit(1)

        study_name = namespace.studies[0]
        d_params = dict([p.split('=') for p in namespace.params or []])

        if study_mngr.is_shared(study_name):
            if len(namespace.projects) > 0:
                print('No project is needed for a shared study')
                sys.exit(1)
            study_ctrl = get_factory().create_study_control(study_name)
            lines = [
                'from data_pm.core.factory import get_factory',
                'study_ctrl = get_factory().create_study_control("{}", None, True)'.format(study_name),
            ]
        else:
            if len(namespace.projects) != 1:
                print('Please specify one project name for your study')
                sys.exit(1)
            project_name = namespace.projects[0]
            study_ctrl = get_factory().create_study_control(study_name, project_name)
            lines = [
                'from data_pm.core.factory import get_factory',
                'study_ctrl = get_factory().create_study_control("{}", "{}", True)'.format(study_name, project_name),
            ]

        more_lines = [
            'from {} import *'.format(study_ctrl.full_name),
            'self = get_factory().create_study(study_ctrl)',
            'self.origin = "{}"'.format(namespace.origin),
            'self.prepare_launch({})'.format(d_params),
            'self._log_writer.set_status("debugging")',
            'self._log_writer.flush()',
            'self._redirector.activate()',
        ]

        jupyter_dir = data_pm.file.TempDir(prefix='jupyter-', create=True)
        os.environ['JUPYTER_RUNTIME_DIR'] = jupyter_dir.path
        if namespace.task_id:
            # Create a kernel (remote mode)
            app = IPKernelApp(
                kernel_class=ipykernel.ipkernel.Kernel,
                ip=data_pm.settings['data_pm.worker.ip'],
                autorestart=False
            )
        else:
            # Create a kernel and a console attached to it (local mode)
            # app = ZMQTerminalIPythonApp(exec_lines=exec_lines + more_exec_lines)
            app = ZMQTerminalIPythonApp()

        app.initialize([])

        exec_lines(app, lines + more_lines)

        if namespace.task_id:
            # Retrieve the connection configuration to send to the emitter of the task
            with open(app.abs_connection_file) as fd:
                params = json.load(fd)

            client.post_debug(
                task_id=namespace.task_id,
                dictionary=params
            )

        app.start()
        jupyter_dir.remove()


def exec_lines(app, lines):
    for line in lines:
        try:
            app.log.info("Running code in user namespace: %s" % line)
            app.shell.run_cell(line, store_history=False)
        except:
            app.log.warning("Error in executing line in user namespace: %s" % line)
            app.shell.showtraceback()
