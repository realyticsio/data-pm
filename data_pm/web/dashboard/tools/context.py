# Global imports
import os
import socket

# Local imports
from data_pm import settings


def load_context(request):
    repo_names = os.listdir(settings.local('data_pm.repo.api'))
    context = {
        'hostname': socket.gethostname(),
        'branches': sorted(repo_names)
    }
    return context
